package com.centroinformacion.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Sala;
import com.centroinformacion.service.SalaService;
import com.centroinformacion.util.AppSettings;

@RestController
@RequestMapping("/url/crudSala")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class CrudSalaController {
	
	@Autowired
	private SalaService salaservice;
	
	@GetMapping("/listaSalaporNumeroLike/{numero}")
	@ResponseBody
	public ResponseEntity<List<Sala>> listaSalaporNumeroLike(@PathVariable("numero") String numero){
		List<Sala> lista=null;
		try {
			if(numero.equals("todos")) {
				lista = salaservice.listaSalaxNumeroLike("%");
			}else {
				lista = salaservice.listaSalaxNumeroLike("%"+ numero + "%");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(lista);
	}
	
	@PostMapping("/registrarSala")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> registrarSala(@RequestBody Sala obj){
		Map<String, Object> salida = new HashMap<>();
		try {
			obj.setFechaActualizacion(new Date());
			obj.setFechaRegistro(new Date());
			obj.setEstado(AppSettings.ACTIVO);
			
			List<Sala> lstNumero = salaservice.listaNumeroIgualRegistrar(obj.getNumero());
			if(!lstNumero.isEmpty()) {
				salida.put("mensaje", "El numero de sala" + obj.getNumero() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
			Sala objSalida = salaservice.insertaActualizaSala(obj);
			if (objSalida == null) {
				salida.put("mensaje","Error en el registro");
			}else {
				salida.put("mensaje","Se registró la Sala con el ID ==> " + objSalida.getIdSala());
			}
		}catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", "No se ha registrado");
		}
		return ResponseEntity.ok(salida);
	}
	
	@PutMapping("/actualizaSala")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> actualizaSala(@RequestBody Sala obj){
		Map<String, Object> salida = new HashMap<>();
		List<Sala> lstNumero = salaservice.listaNumeroIgualActualizar(obj.getNumero(), obj.getIdSala());
		if(!lstNumero.isEmpty()) {
			salida.put("mensaje", "El número de sala" + obj.getNumero() + " ya existe");
			return ResponseEntity.ok(salida);
		}
		try {
			Sala objSalida = salaservice.insertaActualizaSala(obj);
			if (objSalida == null) {
				salida.put("mensaje","Error al actualizar");
			}else {
				salida.put("mensaje","Se actualizó la Sala con el ID ==> " + objSalida.getIdSala());
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", "No se ha actualizado");
		}
		return ResponseEntity.ok(salida);
	}
	
	@DeleteMapping("/eliminaSala/{idSala}")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> eliminaSala(@PathVariable("idSala") int idSala){
		Map<String, Object> salida = new HashMap<>();
		try {
			salaservice.eliminarSala(idSala);
			salida.put("mensaje", "Se ha eliminado la sala " + idSala);
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", "No se ha eliminado");
		}
		return ResponseEntity.ok(salida);
	}
}
