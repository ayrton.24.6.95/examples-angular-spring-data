package com.centroinformacion.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Libro;
import com.centroinformacion.service.LibroService;
import com.centroinformacion.util.AppSettings;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.apachecommons.CommonsLog;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@RestController
@RequestMapping("/url/consultaLibro")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
@CommonsLog
public class LibroConsultaController {
	
	@Autowired
	private LibroService libroService;


	
	@ResponseBody
	@GetMapping("/consultaLibroPorParametros")
	public List<Libro> listaConsultaLibro(
			@RequestParam(name = "titulo",required = false , defaultValue = " ")String titulo,
			@RequestParam(name = "anio",required = false , defaultValue = "0")int anio,
			@RequestParam(name = "serie",required = false , defaultValue = " ")String serie,
			@RequestParam(name = "estado",required = false , defaultValue = "1")int estado,
			@RequestParam(name = "categoriaLibro",required = false , defaultValue = "-1")int categoriaLibro,
			@RequestParam(name = "tipoLibro",required = false , defaultValue = "-1")int tipoLibro
		){
			
		List<Libro> lstSalida = libroService.listaConsulta("%"+titulo+"%",anio,serie,estado,categoriaLibro,tipoLibro);
		return lstSalida;
	}
	
	
	
	//reporte
	@PostMapping("/reporteLibroPdf")
	public void exportarPDF( 
			@RequestParam(name = "titulo",required = false , defaultValue = "")String titulo,
			@RequestParam(name = "anio",required = false , defaultValue = "0" )int anio,
			@RequestParam(name = "serie",required = false , defaultValue = " ")String serie,
			@RequestParam(name = "estado",required = false , defaultValue = "1")int estado,
			@RequestParam(name = "categoriaLibro",required = false , defaultValue = "-1")int categoriaLibro,
			@RequestParam(name = "tipoLibro",required = false , defaultValue = "-1")int tipoLibro,
			HttpServletRequest request,
			HttpServletResponse response) {
		    
		try {
		   //fuente de datos
		    List<Libro> lstSalida = libroService.listaConsulta("%"+titulo+"%", anio, serie, estado, categoriaLibro, tipoLibro);
		    JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(lstSalida); 
		    
		    //diseño reporte
		    String fileReporte = request.getServletContext().getRealPath("/WEB-INF/reportes/reporteLibro.jasper");
		    log.info(">>> filereportee>>>>" + fileReporte);
		    
		    //parametros adicionales
		    String fileLogo = request.getServletContext().getRealPath("/WEB-INF/img/logo.jpg");
		    log.info(">>> filelogo>>>>" + fileLogo);
		    
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("RUTA_LOGO", fileLogo);
		    
		    //se junta la data
		    JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new FileInputStream(new File(fileReporte)));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			
			//
			response.setContentType("application/pdf");
			response.addHeader("Content-disposition", "attachment; filename=ReporteLibro.pdf");
			
			//
			OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);	
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
	}
	

}
