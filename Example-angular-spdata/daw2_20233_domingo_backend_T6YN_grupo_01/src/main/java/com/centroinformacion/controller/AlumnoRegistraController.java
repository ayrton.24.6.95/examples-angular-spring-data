package com.centroinformacion.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Alumno;
import com.centroinformacion.service.AlumnoService;
import com.centroinformacion.util.AppSettings;

@RestController
@RequestMapping("/url/alumno")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class AlumnoRegistraController {
	
	@Autowired
	private AlumnoService alumnoService;
	
	@GetMapping
	@ResponseBody
	public ResponseEntity<List<Alumno>> listaAlumno(){
		List<Alumno> lista = alumnoService.listaTodos();
		return ResponseEntity.ok(lista);
	}
	
	@PostMapping
	@ResponseBody
	public  ResponseEntity<?> insertaAlumno( @RequestBody Alumno obj, Errors errors){
		Map<String, Object> salida = new HashMap <>();
		List<String> lstMensajes = new ArrayList <>();
		salida.put("errores", lstMensajes);
		
		obj.setFechaRegistro(new Date());
		obj.setFechaActualizacion(new Date());
		obj.setEstado(AppSettings.ACTIVO);

		//validacion DNI unique Resgistra
		List<Alumno> lstAlumnoDniUnique = alumnoService.listaPorDNIIgualRegistra(obj.getDni());
		if (!lstAlumnoDniUnique.isEmpty()) {
			salida.put("mensaje", "El DNI " + obj.getDni() + " ya existe");
			return ResponseEntity.ok(salida); 
		}
		
		//validacion Correo unique Registra
		List<Alumno> lstAlumnoCorreoUnique = alumnoService.listaPorCorreoIgualRegistra(obj.getCorreo());
		if (!lstAlumnoCorreoUnique.isEmpty()) {
			salida.put("mensaje", "El Correo " + obj.getCorreo() + " ya se encuentra resgistrado");
			return ResponseEntity.ok(salida);
		}


		Alumno objSalida = alumnoService.insertaAlumno(obj);
		if (objSalida == null) {
			lstMensajes.add("Error en el registro");
		}else {
			lstMensajes.add("Se registró la modalidad de ID ==> " + objSalida.getIdAlumno());
		}
		
		return ResponseEntity.ok(salida);
	}


}
