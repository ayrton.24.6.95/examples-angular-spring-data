package com.centroinformacion.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Editorial;
import com.centroinformacion.service.EditorialService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;


@RestController
@RequestMapping("/url/consultaEditorial")
@CrossOrigin(origins = "http://localhost:4200")
public class EditorialConsultaController {
	
	@Autowired
	private EditorialService editorialService;
	
	@ResponseBody
	@GetMapping("/consultaEditorialPorParametros")
	public ResponseEntity<?> consultarPorParametros(
			@RequestParam(name = "razon", required = false, defaultValue = "") String razon,
			@RequestParam(name = "direccion", required = false, defaultValue = "") String direccion,
			@RequestParam(name = "ruc", required = false, defaultValue = "") String ruc,
			@RequestParam(name = "estado", required = false, defaultValue = "1") int estado,
			@RequestParam(name = "idPais", required = false, defaultValue = "-1") int idPais) {
		
		List<Editorial> lsEditorial = editorialService.buscarPorParametros("%" + razon + "%", "%" + direccion + "%",
				"%" + ruc + "%", estado, idPais);
		return ResponseEntity.ok(lsEditorial);
	}
	
	@PostMapping("/reporteEditorialPdf")
	public void  exportaPDF(
			@RequestParam(name = "razon", required = false, defaultValue = "") String razon,
			@RequestParam(name = "direccion", required = false, defaultValue = "") String direccion,
			@RequestParam(name = "ruc", required = false, defaultValue = "") String ruc,
			@RequestParam(name = "estado", required = false, defaultValue = "1") int estado,
			@RequestParam(name = "idPais", required = false, defaultValue = "-1") int idPais,
			HttpServletRequest request,
			HttpServletResponse response) {
		
		try {
			
			//PASO 1 Fuente de datos
			List<Editorial> lsEditorial = editorialService.buscarPorParametros("%" + razon + "%", "%" + direccion + "%",
					"%" + ruc + "%", estado, idPais);
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(lsEditorial);
			
			//PASO 2 Diseño de reporte
			String fileReporte  = request.getServletContext().getRealPath("/WEB-INF/reportes/reporteEditorial.jasper");
			Log.info(">>> fileReporte >> " + fileReporte);
			
			//PASO3 parámetros adicionales
			String fileLogo = request.getServletContext().getRealPath("/WEB-INF/img/logo.jpg");
			Log.info(">>> filelogo >> "+ fileLogo);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("RUTA_LOGO", fileLogo);
			
			//PASO4 Se juntas la data, diseño y parámetros
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new FileInputStream(new File(fileReporte)));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
	       
			//PASO 5 parametros en el Header del mensajes HTTP
    		response.setContentType("application/pdf");
    	    response.addHeader("Content-disposition", "attachment; filename=ReporteAutor.pdf");
		    
			//PASO 6 Se envia el pdf
			OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


}
