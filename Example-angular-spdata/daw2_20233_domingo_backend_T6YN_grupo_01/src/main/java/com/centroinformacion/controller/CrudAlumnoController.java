package com.centroinformacion.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Alumno;
import com.centroinformacion.service.AlumnoService;
import com.centroinformacion.util.AppSettings;
import com.centroinformacion.util.Constantes;

@RestController
@RequestMapping("/url/alumno")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class CrudAlumnoController {
	
	@Autowired
	private AlumnoService alumnoService;
	
	@GetMapping("/listaAlumnoPorNombreLike/{nom}")
	@ResponseBody
	public ResponseEntity<List<Alumno>> listaAlumnoPorNombreLike(@PathVariable("nom") String nom){
		List<Alumno> lista = null;
		try {
			if (nom.equals("todos")) {
				lista = alumnoService.listaAlumnoPorNombreLike("%");
			} else {
				lista = alumnoService.listaAlumnoPorNombreLike("%" + nom + "%");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(lista);
	}
	
	
	@PostMapping("/registraAlumno")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> insertaAlumno(@RequestBody Alumno obj) {
		Map<String, Object> salida = new HashMap<>();
		try {
			// valores por defecto
			obj.setIdAlumno(0);
			obj.setFechaRegistro(new Date());
			obj.setFechaActualizacion(new Date());
			obj.setEstado(AppSettings.ACTIVO);
			
			//validacion DNI unique Resgistra
			List<Alumno> lstAlumnoDniUnique = alumnoService.listaPorDNIIgualRegistra(obj.getDni());
			if (!lstAlumnoDniUnique.isEmpty()) {
				salida.put("mensaje", "El DNI " + obj.getDni() + " ya existe");
				return ResponseEntity.ok(salida); 
			}
			
			//validacion Correo unique Resgistra
			List<Alumno> lstAlumnoCorreoUnique = alumnoService.listaPorCorreoIgualRegistra(obj.getCorreo());
			if (!lstAlumnoCorreoUnique.isEmpty()) {
				salida.put("mensaje", "El Correo " + obj.getCorreo() + " ya se encuentra resgistrado");
				return ResponseEntity.ok(salida);
			}
			
			
			Alumno objSalida = alumnoService.insertaActualizaAlumno(obj);
			if (objSalida == null) {
				salida.put("mensaje", Constantes.MENSAJE_REG_ERROR);
			} else {
				salida.put("mensaje", Constantes.MENSAJE_REG_EXITOSO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", Constantes.MENSAJE_REG_ERROR);
		}
		return ResponseEntity.ok(salida);
	}
	
	
	@PutMapping("/actualizaAlumno")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> actualizaAlumno(@RequestBody Alumno obj) {
		Map<String, Object> salida = new HashMap<>();
		
		//validacion DNI unique Actualiza
		List<Alumno> lstAlumnoDniUnique = alumnoService.listaPorDNIIgualActualiza(obj.getDni(), obj.getIdAlumno());
		if (!lstAlumnoDniUnique.isEmpty()) {
			salida.put("mensaje", "El DNI " + obj.getDni() + " ya existe");
			return ResponseEntity.ok(salida); 
		}
		
		//validacion Correo unique Actualiza
		List<Alumno> lstAlumnoCorreoUnique = alumnoService.listaPorCorreoIgualActualiza(obj.getCorreo(), obj.getIdAlumno());
		if (!lstAlumnoCorreoUnique.isEmpty()) {
			salida.put("mensaje", "El Corrego " + obj.getCorreo() + " ya existe");
			return ResponseEntity.ok(salida); 
		}

		obj.setFechaActualizacion(new Date());		
		
		try {
			Alumno objSalida = alumnoService.insertaActualizaAlumno(obj);
			if (objSalida == null) {
				salida.put("mensaje", Constantes.MENSAJE_ACT_ERROR);
			} else {
				salida.put("mensaje", Constantes.MENSAJE_ACT_EXITOSO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", Constantes.MENSAJE_ACT_ERROR);
		}
		return ResponseEntity.ok(salida);
	}
	
	@DeleteMapping("/eliminaAlumno/{id}")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> eliminaAlumno(@PathVariable("id") int idAlumno) {
		Map<String, Object> salida = new HashMap<>();
		try {
			alumnoService.eliminaAlumno(idAlumno);
			salida.put("mensaje", Constantes.MENSAJE_ELI_EXITOSO);
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", Constantes.MENSAJE_ELI_ERROR);
		}
		return ResponseEntity.ok(salida);
	}
	



}
