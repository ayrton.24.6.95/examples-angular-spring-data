package com.centroinformacion.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Proveedor;
import com.centroinformacion.service.ProveedorService;
import com.centroinformacion.util.AppSettings;

@RestController
@RequestMapping("/url/proveedor")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class ProveedorRegistraController {

	@Autowired
	private ProveedorService proveedorService;
	
	@GetMapping
	@ResponseBody
	public ResponseEntity<List<Proveedor>> listaproveedor(){
		List<Proveedor> lstSalida = proveedorService.listaProveedor();
		return ResponseEntity.ok(lstSalida);		
	}
		
	@PostMapping
	@ResponseBody
	public ResponseEntity<?> insertaProveedor(@RequestBody Proveedor obj){
		HashMap<String, Object> salida = new HashMap<>();
		
		obj.setFechaRegistro(new Date());
		obj.setFechaActualizacion(new Date());
		obj.setEstado(AppSettings.ACTIVO);
		
		//validando que razón social no se repita
		List<Proveedor> listaProveedorNombre = proveedorService.listaPorRazonSocialIgualRegistra(obj.getRazonsocial());
		if (!listaProveedorNombre.isEmpty()) {
			salida.put("mensaje", "El Proveedor (" + obj.getRazonsocial() + ") ya existe");
			return ResponseEntity.ok(salida);
		}
		//validando que el ruc no se repita
		List<Proveedor> listaProveedorRuc = proveedorService.listaPorRucIgualRegistra(obj.getRuc());
		if (!listaProveedorRuc.isEmpty()) {
			salida.put("mensaje", "El RUC (" + obj.getRuc() + ") ya existe");
			return ResponseEntity.ok(salida);
		}
		//validando que el celular no se repita
		List<Proveedor> listaProveedorCelular = proveedorService.listaPorCelularIgualRegistra(obj.getCelular());
		if (!listaProveedorCelular.isEmpty()) {
			salida.put("mensaje", "El Celular (" + obj.getCelular() + ") ya existe");
			return ResponseEntity.ok(salida);
		}
		
		Proveedor objSalida = proveedorService.instertaActualizaProveedor(obj);
		if (objSalida == null) {
			salida.put("mensaje", "Error en el registro");
		}else {
			salida.put("mensaje", "Registro realizado con el ID ==> " + objSalida.getIdProveedor());
		}
		return ResponseEntity.ok(salida);				
	}
	
}
