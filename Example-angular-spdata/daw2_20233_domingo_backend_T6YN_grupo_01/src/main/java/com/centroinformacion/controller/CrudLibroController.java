package com.centroinformacion.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.DataCatalogo;
import com.centroinformacion.entity.Libro;
import com.centroinformacion.service.LibroService;
import com.centroinformacion.util.AppSettings;

@RestController
@RequestMapping("/url/crudLibro")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class CrudLibroController {
	
	@Autowired
	private LibroService service;
	
	@GetMapping("/listaLibroPorTituloLike/{tit}")
	@ResponseBody
	public ResponseEntity<List<Libro>>listaLibroPorTituloLike(@PathVariable("tit") String tit){
		List<Libro> lista=null;
		try {
			if(tit.equals("todos")) {
				lista = service.listaLibroPorTituloLike("%");
			}else {
				lista = service.listaLibroPorTituloLike("%" + tit + "%");
			}	
			}catch (Exception e) {
				e.printStackTrace();
			}
		return ResponseEntity.ok(lista);
		}
	
	
	@PostMapping("/registrarLibro")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> insertaLibro(@RequestBody Libro obj){
		Map<String, Object> salida = new HashMap<>();
		try {

			obj.setFechaActualizacion(new Date());
			obj.setFechaRegistro(new Date());
			obj.setEstado(AppSettings.ACTIVO);
			
			DataCatalogo objData = new DataCatalogo();
			objData.setIdDataCatalogo(27);
			obj.setEstadoPrestamo(objData);
			
			List<Libro> lstLibroSerie = service.listaPorSerieIgualRegistrar(obj.getSerie());
			if(!lstLibroSerie.isEmpty()) {
				salida.put("mensaje", "El Libro "+ obj.getSerie() + " ya existe");
				return ResponseEntity.ok(salida);
			}

			
			Libro objSalida = service.insertaActualizaLibro(obj);
			if(objSalida == null) {
				salida.put("mensaje", "No se registró, consulte con el administrador.");
			}else {
				salida.put("mensaje", "Se registró correctamente.");
			}
		}catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje","No se registró, consulte con el administrador." );
		}
		return ResponseEntity.ok(salida);
	}
	
	
	@PutMapping("/actualizaLibro")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> actualizaLibro(@RequestBody Libro obj){
		Map<String, Object> salida = new HashMap<>();
		//validaciones
		List<Libro> lstLibroSerie = service.listaPorSerieIgualActualizar(obj.getSerie(), obj.getIdLibro());
		if(!lstLibroSerie.isEmpty()) {
			salida.put("mensaje","El Libro "+ obj.getSerie() + " ya existe");
			return ResponseEntity.ok(salida);
		}
		
		try {
			Libro objSalida = service.insertaActualizaLibro(obj);
			if(objSalida == null) {
				salida.put("mensaje", "No se actualizó, consulte con el administrador.");
			}else {
				salida.put("mensaje", "Se actualizó correctamente.");
			}
		}catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje","No se actualizó, consulte con el administrador." );
		}
		return ResponseEntity.ok(salida);
	}
	
	
	@DeleteMapping("/eliminaLibro/{id}")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> eliminaLibro(@PathVariable("id") int  idLibro){
		Map<String, Object> salida = new HashMap<>();
		try {
			 service.eliminaLibro(idLibro);
			 salida.put("mensaje", "Se eliminó correctamente." );
		}catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje","No se eliminó, ya que el registro esta relacionado." );
		}
		return ResponseEntity.ok(salida);
	}
	

}
