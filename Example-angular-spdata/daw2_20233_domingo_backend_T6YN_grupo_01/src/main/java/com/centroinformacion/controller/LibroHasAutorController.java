package com.centroinformacion.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.LibroHasAutor;
import com.centroinformacion.service.LibroHasAutorService;
import com.centroinformacion.util.AppSettings;

@RestController
@RequestMapping("/url/libro-has-autor")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class LibroHasAutorController {

	@Autowired
	private LibroHasAutorService service;

	@GetMapping
	@ResponseBody
	public ResponseEntity<List<LibroHasAutor>> listarEditorial() {
		List<LibroHasAutor> lista = service.listarLibroHasAutors();
		return ResponseEntity.ok(lista);
	}

	@ResponseBody
	@GetMapping("/{idLibro}/{idAutor}") // Se añaden parámetros a la URL
	public ResponseEntity<LibroHasAutor> consultarPorParametros(
			@PathVariable("idLibro") int idLibro,
			@PathVariable("idAutor") int idAutor) {
		try {
			LibroHasAutor libroHasAutor = service.consulLibroHasAutor(idLibro, idAutor);
			return ResponseEntity.ok(libroHasAutor);
		} catch (Exception e) {
			// Manejar la excepción adecuadamente
			return ResponseEntity.status(404).body(null);
		}
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity<LibroHasAutor> registrarLibroHasAutor(@RequestBody LibroHasAutor obj) {
		var resp = service.registrarLibroHasAutor(obj);
		return ResponseEntity.ok(resp);
	}

	@DeleteMapping("/{idLibro}/{idAutor}") // Se añaden parámetros a la URL
	@ResponseBody
	public ResponseEntity<HashMap<String, Object>> eliminar(@PathVariable("idLibro") int idLibro,
			@PathVariable("idAutor") int idAutor) {

		try {
			service.eliminarLibroHasAutor(idLibro, idAutor);
			HashMap<String, Object> salida = new HashMap<>();
			salida.put("mensaje", "Se eliminó correctamente");
			return ResponseEntity.ok(salida);
		} catch (Exception e) {
			// Manejar la excepción adecuadamente
			HashMap<String, Object> salida = new HashMap<>();
			salida.put("error", "No se pudo eliminar el registro");
			return ResponseEntity.status(500).body(salida);
		}
	}

}
