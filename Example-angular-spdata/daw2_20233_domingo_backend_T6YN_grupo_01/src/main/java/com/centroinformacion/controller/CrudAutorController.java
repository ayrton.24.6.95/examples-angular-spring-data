package com.centroinformacion.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Autor;
import com.centroinformacion.service.AutorService;
import com.centroinformacion.util.AppSettings;

@RestController
@RequestMapping("/url/autor")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class CrudAutorController {
	
	@Autowired
	private AutorService autorService;
	
	@GetMapping("/listaAutorPorNombreLike/{nom}")
	@ResponseBody
	public ResponseEntity<List<Autor>> listaAutorPorNombreLike(@PathVariable("nom") String nom) {
		List<Autor> lista = null;		
		try {
			if (nom.equals("todos")) {
				lista = autorService.listaAutorPorNombreLike("%");
			}else {
				lista = autorService.listaAutorPorNombreLike("%" + nom + "%");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(lista);
		
	} // fin de listaAutorPorNombreLike
	
	@PostMapping("/registraAutor")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> insertaAutor(@RequestBody Autor obj){
		Map<String, Object> salida = new HashMap<>();
		try {
			//valores por defecto
			obj.setIdAutor(0);
			obj.setFechaRegistro(new Date());
			obj.setFechaActualizacion(new Date());		
			obj.setEstado(AppSettings.ACTIVO);	
			
			//Validación de Nombre unique
			List<Autor> lstAutorNombre = autorService.listaPorNombreIgualRegistra(obj.getNombres(), obj.getApellidos());
			if (!lstAutorNombre.isEmpty()) {
				salida.put("mensaje", "El autor (" + obj.getNombres() + " " +obj.getApellidos() + ") ya existe");
				return ResponseEntity.ok(salida);
			}
			
			//Validación de Telefono unique
			List<Autor> lstAutorTelefono = autorService.listaPorTelefonoIgualRegistra(obj.getTelefono());
			if (!lstAutorTelefono.isEmpty()) {
				salida.put("mensaje", "El Telefono (" + obj.getTelefono() + ") ya existe");
				return ResponseEntity.ok(salida);
			}
						
			Autor objSalida = autorService.insertaActualizaAutor(obj);
			if (objSalida == null) {
				salida.put("mensaje","Error en el registro");
			}else {			
				salida.put("mensaje","Se registró el Autor con el ID ==> " + objSalida.getIdAutor());
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje","Error en el registro");
		}
		
		return ResponseEntity.ok(salida);
		
	} // fin de insertaAutor
	
	@PutMapping("/actualizaAutor")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> actualizaAutor(@RequestBody Autor obj){
		Map<String, Object> salida = new HashMap<>();
		//valor por defecto
		obj.setFechaActualizacion(new Date());	
		
		//Validación de Nombre unique
		List<Autor> lstAutorNombre = autorService.listaPorNombreIgualActualizar(obj.getNombres(), obj.getApellidos() ,obj.getIdAutor());
		if (!lstAutorNombre.isEmpty()) {
			salida.put("mensaje", "El Nombre (" + obj.getNombres() + " " + obj.getApellidos() + ") ya existe");
			return ResponseEntity.ok(salida);
		}
		
		//Validación de Telefono unique
		List<Autor> lstAutorTelefono = autorService.listaPorTelefonoIgualActualizar(obj.getTelefono(), obj.getIdAutor());
		if (!lstAutorTelefono.isEmpty()) {
			salida.put("mensaje", "El Telefono (" + obj.getTelefono() + ") ya existe");
			return ResponseEntity.ok(salida);
		}
		
		try {				
			Autor objSalida = autorService.insertaActualizaAutor(obj);
			if (objSalida == null) {
				salida.put("mensaje","Error en la actualizacion");
			}else {			
				salida.put("mensaje","Se actualizo el ID ==> " + objSalida.getIdAutor());
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje","Error en la actualizacion");
		}
			
		return ResponseEntity.ok(salida);
		
	} // fin de actualizaAutor
	
	
	@DeleteMapping("/eliminaAutor/{id}")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> eliminaAutor(@PathVariable("id") int idAutor){
		Map<String, Object> salida = new HashMap<>();
		try {
			autorService.eliminaAutor(idAutor);
			salida.put("mensaje","Se eliminó correctamente");			
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje","No se eliminó, ya que el registro esta relacionado");
		}
		
		return ResponseEntity.ok(salida);
		
	} // fin eliminar
	
	
} // fin de class AutorRegistraController 
