package com.centroinformacion.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Proveedor;
import com.centroinformacion.service.ProveedorService;
import com.centroinformacion.util.AppSettings;


@RestController
@RequestMapping("/url/crudproveedor")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class CrudProveedorController {
	
	@Autowired
	private ProveedorService proveedorService;
	
	@GetMapping("/listaProveedorPorNombreLike/{nom}")
	@ResponseBody
	public ResponseEntity<List<Proveedor>> listaProveedorPorNombreLike(@PathVariable("nom") String nom){
		List<Proveedor> lista = null;
		try {
			if (nom.equals("todos")) {
				lista = proveedorService.listaProveedorPorNombreLike("%");
			}else {
				lista = proveedorService.listaProveedorPorNombreLike("%" + nom + "%");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(lista);
	}
	
	//registra en el crud
	@PostMapping("/registraProveedor")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> insertaProveedor(@RequestBody Proveedor obj){
		Map<String, Object> salida = new HashMap<>();
		try {
			//valores por defecto
			obj.setIdProveedor(0);
			obj.setFechaRegistro(new Date());
			obj.setFechaActualizacion(new Date());
			obj.setEstado(1);
			
			//validando Razón social unique
			List<Proveedor> listaProveedorRazonSocial = proveedorService.listaPorRazonSocialIgualRegistra(obj.getRazonsocial());
			if (!listaProveedorRazonSocial.isEmpty()) {
				salida.put("mensaje", "La Razón Social " + obj.getRazonsocial() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
			//validando RUC unique
			List<Proveedor> listaProveedorRuc = proveedorService.listaPorRucIgualRegistra(obj.getRuc());
			if (!listaProveedorRuc.isEmpty()) {
				salida.put("mensaje", "El RUC " + obj.getRuc() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
			//validando Celular unique
			List<Proveedor> listaProveedorCelular = proveedorService.listaPorCelularIgualRegistra(obj.getCelular());
			if (!listaProveedorCelular.isEmpty()) {
				salida.put("mensaje", "El Celular " + obj.getCelular() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
			Proveedor objSalida = proveedorService.instertaActualizaProveedor(obj);
			if (objSalida == null) {
				salida.put("mensaje", "Error en el registro");
			} else {
				salida.put("mensaje", "Registro realizado con el ID ==> " + objSalida.getIdProveedor());
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", "Error en el registro");
		}
		return ResponseEntity.ok(salida);		
	}
	
	//actualiza en el CRUD
	@PutMapping("/actualizaProveedor")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> actualizaProveedor(@RequestBody Proveedor obj){
		Map<String, Object> salida = new HashMap<>();
		
		obj.setFechaActualizacion(new Date());
			
			//validando Razón social unique
			List<Proveedor> listaProveedorRazonSocial = proveedorService.listaPorRazonSocialIgualActualiza(obj.getRazonsocial(), obj.getIdProveedor());
			if (!listaProveedorRazonSocial.isEmpty()) {
				salida.put("mensaje", "La Razón Social " + obj.getRazonsocial() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
			//validando RUC unique
			List<Proveedor> listaProveedorRuc = proveedorService.listaPorRucIgualActualiza(obj.getRuc(), obj.getIdProveedor());
			if (!listaProveedorRuc.isEmpty()) {
				salida.put("mensaje", "El RUC " + obj.getRuc() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
			//validando Celular unique
			List<Proveedor> listaProveedorCelular = proveedorService.listaPorCelularIgualActualiza(obj.getCelular(), obj.getIdProveedor());
			if (!listaProveedorCelular.isEmpty()) {
				salida.put("mensaje", "El Celular " + obj.getCelular() + " ya existe");
				return ResponseEntity.ok(salida);
			}
			
		try {
			Proveedor objSalida = proveedorService.instertaActualizaProveedor(obj);
			if (objSalida == null) {
				salida.put("mensaje", "NO se pudo Actualizar");
			} else {
				salida.put("mensaje", "Actualización realizada con éxito");
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", "Error en la actualización");
		}
		return ResponseEntity.ok(salida);		
	}
	
	
	@DeleteMapping("/eliminaProveedor/{id}")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> eliminaProveedor(@PathVariable("id") int idProveedor){
		Map<String, Object> salida = new HashMap<>();
		try {
			proveedorService.eliminaProveedor(idProveedor);
			salida.put("mensaje", "Proveedor Eliminado Correctamente");
		}catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", "Proveedor NO eliminado");
		}
		return ResponseEntity.ok(salida);
		
	}

}
