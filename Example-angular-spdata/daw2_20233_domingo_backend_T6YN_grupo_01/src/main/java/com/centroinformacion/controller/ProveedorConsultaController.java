package com.centroinformacion.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Proveedor;
import com.centroinformacion.service.ProveedorService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@RestController
@RequestMapping("/url/consultaProveedor")
@CrossOrigin(origins = "http://localhost:4200")
public class ProveedorConsultaController {
	
	@Autowired
	private ProveedorService proveedorService;
	
	@ResponseBody
	@GetMapping("/consultaProveedorPorParametros")
	public List<Proveedor> listaConsultaProveedor(
						@RequestParam(name = "razonsocial", required = false, defaultValue = "") String razonsocial,
						@RequestParam(name = "ruc", required = false, defaultValue = "") String ruc,
						@RequestParam(name = "estado", required = false, defaultValue = "1") int estado,
						@RequestParam(name = "idPais", required = false, defaultValue = "-1") int idPais,
						@RequestParam(name = "idTipoProveedor", required = false, defaultValue = "-1") int idDataCatalogo){
		
		List<Proveedor> lstSalida = proveedorService.listaConsulta("%"+razonsocial+"%", "%"+ruc+"%", estado, idPais, idDataCatalogo);
		return lstSalida;
	}
	
	@PostMapping("/reporteProveedorPdf")
	public void exportaPdf(
			@RequestParam(name = "razonsocial", required = false, defaultValue = "") String razonsocial,
			@RequestParam(name = "ruc", required = false, defaultValue = "") String ruc,
			@RequestParam(name = "estado", required = false, defaultValue = "1") int estado,
			@RequestParam(name = "idPais", required = false, defaultValue = "-1") int idPais,
			@RequestParam(name = "idTipoProveedor", required = false, defaultValue = "-1") int idDataCatalogo,
			HttpServletRequest request,
			HttpServletResponse response) {
		
		try {
			
			//PASO 1 -> Fuente de datos.
			List<Proveedor> lstSalida = proveedorService.listaConsulta("%"+razonsocial+"%", "%"+ruc+"%", estado, idPais, idDataCatalogo);
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(lstSalida);
			
			//PASO 2 -> Diseño de reporte.
			String fileReporte  = request.getServletContext().getRealPath("/WEB-INF/reportes/reporteProveedor.jasper");
			Log.info(">>> fileReporte >> " + fileReporte);
			
			//PASO 3 -> Parámetros Adicionales.
			String fileLogo = request.getServletContext().getRealPath("/WEB-INF/img/logo.jpg");
			Log.info(">>> filelogo >> "+ fileLogo);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("RUTA_LOGO", fileLogo);
			
			//PASO 4 -> Juntamos la data, diseño y parámetros.
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new FileInputStream(new File(fileReporte)));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
	       
			//PASO 5 -> Parámetros en el Header del mensajes HTTP.
    		response.setContentType("application/pdf");
    	    response.addHeader("Content-disposition", "attachment; filename=ReporteAutor.pdf");
		    
			//PASO 6 -> Se envía el PDF.
			OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}					
}
