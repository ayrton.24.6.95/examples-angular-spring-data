package com.centroinformacion.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.centroinformacion.entity.Revista;
import com.centroinformacion.service.RevistaService;
import com.centroinformacion.util.AppSettings;
import com.centroinformacion.util.Constantes;

@RestController
@RequestMapping("/url/revista")
@CrossOrigin(origins = AppSettings.URL_CROSS_ORIGIN)
public class CrudRevistaController {

	@Autowired
	private RevistaService service;

	@GetMapping("/listaRevistaPorNombreLike/{nom}")
	@ResponseBody
	public ResponseEntity<List<Revista>> listaRevistaPorNombreLike(@PathVariable("nom") String nom) {
		List<Revista> lista = null;
		try {
			if (nom.equals("todos")) {
				lista = service.listaAutorPorNombreLike("%");
			} else {
				lista = service.listaAutorPorNombreLike("%" + nom + "%");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(lista);

	}

	@PostMapping("/registraRevista")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> insertaRevista(@RequestBody Revista obj) {
		Map<String, Object> salida = new HashMap<>();
		try {
			// valores por defecto
			obj.setIdRevista(0);
			obj.setFechaRegistro(new Date());
			obj.setFechaActualizacion(new Date());
			obj.setEstado(AppSettings.ACTIVO);
			
			//Validación de Nombre unique
			List<Revista> lstRevista =  service.listaPorNombreIgualRegistra(obj.getNombre());
			if (!lstRevista.isEmpty()) {
				salida.put("mensaje", "La Revista: " + obj.getNombre() + ", ya existe");
				return ResponseEntity.ok(salida);
			} 
			
			//Validación de Frecuencia unique
			List<Revista> lstFrecuencia =  service.listaPorFrecuenciaIgualRegistra(obj.getFrecuencia());
			if (!lstFrecuencia.isEmpty()) {
				salida.put("mensaje", "La Frecuencia: " + obj.getFrecuencia() + ", ya existe");
				return ResponseEntity.ok(salida);
			} 


			Revista objSalida = service.insertaActualizaRevista(obj);
			if (objSalida == null) {
				salida.put("mensaje", Constantes.MENSAJE_REG_ERROR);
			} else {
				salida.put("mensaje", Constantes.MENSAJE_REG_EXITOSO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", Constantes.MENSAJE_REG_ERROR);
		}
		return ResponseEntity.ok(salida);
	}

	@PutMapping("/actualizaRevista")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> actualizaRevista(@RequestBody Revista obj) {
		Map<String, Object> salida = new HashMap<>();

		obj.setFechaActualizacion(new Date());
		
		//Validación de Nombre unique
		List<Revista> lstRevista =  service.listaPorNombreIgualActualizar(obj.getNombre(), obj.getIdRevista());
		if (!lstRevista.isEmpty()) {
			salida.put("mensaje", "La Revista: " + obj.getNombre() + ", ya existe");
			return ResponseEntity.ok(salida);
		}
		
		//Validación de Frecuencia unique
				List<Revista> lstFrecuencia =  service.listaPorfrecuenciaIgualActualizar(obj.getFrecuencia(), obj.getIdRevista());
				if (!lstFrecuencia.isEmpty()) {
					salida.put("mensaje", "La Frecuencia: " + obj.getFrecuencia() + ", ya existe");
					return ResponseEntity.ok(salida);
				}

		try {
			Revista objSalida = service.insertaActualizaRevista(obj);
			if (objSalida == null) {
				salida.put("mensaje", Constantes.MENSAJE_ACT_ERROR);
			} else {
				salida.put("mensaje", Constantes.MENSAJE_ACT_EXITOSO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", Constantes.MENSAJE_ACT_ERROR);
		}
		return ResponseEntity.ok(salida);
	}

	@DeleteMapping("/eliminaRevista/{id}")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> eliminaRevista(@PathVariable("id") int idRevista) {
		Map<String, Object> salida = new HashMap<>();
		try {
			service.eliminaRevista(idRevista);
			salida.put("mensaje", Constantes.MENSAJE_ELI_EXITOSO);
		} catch (Exception e) {
			e.printStackTrace();
			salida.put("mensaje", Constantes.MENSAJE_ELI_ERROR);
		}
		return ResponseEntity.ok(salida);
	}

}
