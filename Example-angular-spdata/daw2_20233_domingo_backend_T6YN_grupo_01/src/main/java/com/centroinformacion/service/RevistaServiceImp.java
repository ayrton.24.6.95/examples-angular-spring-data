package com.centroinformacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Revista;
import com.centroinformacion.repository.RevistaRepository;

@Service
public class RevistaServiceImp implements RevistaService{
	@Autowired
	 private RevistaRepository repository; 
	
	@Override
	public Revista insertaActualizaRevista(Revista obj) {		
		return repository.save(obj);
	}

	@Override
	public List<Revista> listaRevista() {
		return repository.findAll();
	}

	@Override
	public List<Revista> listaAutorPorNombreLike(String nombre) {
		return repository.listaPorNombreLike(nombre);
	}

	@Override
	public void eliminaRevista(int idRevista) {
		repository.deleteById(idRevista);
		
	}

	@Override
	public List<Revista> listaPorNombreIgualRegistra(String nombre) {		
		return repository.listaPorNombreIgualRegistra(nombre);
	}

	@Override
	public List<Revista> listaPorNombreIgualActualizar(String nombre, int idRevista) {
		return repository.listaPorNombreIgualActualizar(nombre, idRevista);
	}

	@Override
	public List<Revista> listaPorFrecuenciaIgualRegistra(String frecuencia) {
		return repository.listaPorFrecuenciaIgualRegistra(frecuencia);
	}

	@Override
	public List<Revista> listaPorfrecuenciaIgualActualizar(String frecuencia, int idRevista) {
		return repository.listaPorFreceunciaIgualActualizar(frecuencia, idRevista);
	}
	@Override
	public List<Revista> listaConsultaRevista(String nombre, String frecuencia, int estado, int pais,
			int tipoRevista) {
		return repository.listaConsultaRevista(nombre, frecuencia, estado, pais, tipoRevista);
	}
}
