package com.centroinformacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Libro;
import com.centroinformacion.repository.LibroRepository;

@Service
public class LibroServiceImp implements LibroService{
	
	@Autowired
	private LibroRepository repository;

	@Override
	public List<Libro> listaLibro() {
		
		return repository.findAll();
	}

	@Override
	public Libro insertarLibro(Libro obj) {
		
		return repository.save(obj);
	}

	
	//PC2 
	@Override
	public List<Libro> listaLibroPorTituloLike(String titulo) {
		
		return repository.listaLibroPorTituloLike(titulo);
	}

	@Override
	public Libro insertaActualizaLibro(Libro obj) {
		return repository.save(obj);
	}

	@Override
	public void eliminaLibro(int idLibro) {
		repository.deleteById(idLibro);
		
	}

	@Override
	public Optional<Libro> buscaEmpleado(int idLibro) {
		// TODO Auto-generated method stub
		return repository.findById(idLibro);
	}

	@Override
	public List<Libro> listaPorSerieIgualRegistrar(String serie) {
		// TODO Auto-generated method stub
		return repository.listaPorSerieIgualRegistrar(serie);
	}

	@Override
	public List<Libro> listaPorSerieIgualActualizar(String serie, int idLibro) {
		// TODO Auto-generated method stub
		return repository.listaPorSerieIgualActualizar(serie, idLibro);
	}
	
	//consulta
	@Override
	public List<Libro> listaConsulta(String titulo,int anio,String serie, int estado,int categoriaLibro,int tipoLibro) {
		return repository.listaConsulta(titulo, anio, serie, estado, categoriaLibro, tipoLibro);
	}

	
	

	
	
	
	

	
}
