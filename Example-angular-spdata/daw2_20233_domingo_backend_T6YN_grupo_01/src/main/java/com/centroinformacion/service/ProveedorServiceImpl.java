package com.centroinformacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Proveedor;
import com.centroinformacion.repository.ProveedorRepository;

@Service
public class ProveedorServiceImpl implements ProveedorService {
	
	@Autowired
	private ProveedorRepository repository;
	
	@Override
	public List<Proveedor> listaProveedor() {
		return repository.findAll();
	}

	@Override
	public Proveedor instertaActualizaProveedor(Proveedor obj) {
		return repository.save(obj);
	}

	@Override
	public List<Proveedor> listaProveedorPorNombreLike(String razonsocial) {
		return repository.listaPorNombreLike(razonsocial);
	}

	@Override
	public void eliminaProveedor(int idProveedor) {
		repository.deleteById(idProveedor);
		
	}

	@Override
	public List<Proveedor> listaPorRazonSocialIgualRegistra(String razonsocial) {
		return repository.listaPorRazonSocialIgualRegistra(razonsocial);
	}

	@Override
	public List<Proveedor> listaPorRucIgualRegistra(String ruc) {
		return repository.listaPorRucIgualRegistra(ruc);
	}

	@Override
	public List<Proveedor> listaPorCelularIgualRegistra(String celular) {
		return repository.listaPorCelularIgualRegistra(celular);
	}

	@Override
	public List<Proveedor> listaPorRazonSocialIgualActualiza(String razonsocial, int idProveedor) {
		return repository.listaPorRazonSocialIgualActualiza(razonsocial, idProveedor);
	}

	@Override
	public List<Proveedor> listaPorRucIgualActualiza(String ruc, int idProveedor) {
		return repository.listaPorRucIgualActualiza(ruc, idProveedor);
	}

	@Override
	public List<Proveedor> listaPorCelularIgualActualiza(String celular, int idProveedor) {
		return repository.listaPorCelularIgualActualiza(celular, idProveedor);
	}

	//consulta P03
	@Override
	public List<Proveedor> listaConsulta(String razonsocial, String ruc, int estado, int idPais, int idDataCatalogo) {
		return repository.listaConsulta(razonsocial, ruc, estado, idPais, idDataCatalogo);
	}
}
