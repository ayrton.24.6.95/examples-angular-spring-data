
package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.Revista;

public interface RevistaService {
	
	public abstract List<Revista> listaRevista();
	public abstract Revista insertaActualizaRevista (Revista obj);
	
	public abstract List<Revista> listaAutorPorNombreLike(String nombre);
	public abstract void eliminaRevista(int idRevista);
	
	public abstract List<Revista> listaPorNombreIgualRegistra(String nombre);
	public abstract List<Revista> listaPorFrecuenciaIgualRegistra(String frecuencia);
	
	public abstract List<Revista> listaPorNombreIgualActualizar(String nombre, int idRevista);	
	public abstract List<Revista> listaPorfrecuenciaIgualActualizar(String frecuencia, int idRevista);
	//consulta
	public abstract List<Revista> listaConsultaRevista(String nombre, String frecuencia , int estado, int pais, int tipoRevista);

	

}
