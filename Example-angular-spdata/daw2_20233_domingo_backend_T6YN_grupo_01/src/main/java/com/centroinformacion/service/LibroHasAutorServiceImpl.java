package com.centroinformacion.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.LibroHasAutor;
import com.centroinformacion.entity.LibroHasAutorPK;
import com.centroinformacion.repository.LibroAutorRepository;

@Service
public class LibroHasAutorServiceImpl implements LibroHasAutorService {

    @Autowired
    private LibroAutorRepository repository;

    @Override
    public LibroHasAutor registrarLibroHasAutor(LibroHasAutor libroHasAutor) {
        return repository.save(libroHasAutor);
    }

    @Override
    public List<LibroHasAutor> listarLibroHasAutors() {

        return repository.findAll();
    }

    @Override
    public LibroHasAutor consulLibroHasAutor(int idLibro, int idAutor) {
        try {
            Optional<LibroHasAutor> optionalLibroHasAutor = repository.findById(new LibroHasAutorPK(idLibro, idAutor));
            return optionalLibroHasAutor.orElseThrow(() -> new RuntimeException("LibroHasAutor no encontrado"));
            
        } catch (NoSuchElementException ex) {
            throw new RuntimeException("LibroHasAutor no encontrado", ex);
        }
    }

    @Override
    public void eliminarLibroHasAutor(int idLibro, int idAutor) {
        LibroHasAutor libroHasAutor = consulLibroHasAutor(idLibro, idAutor);
        repository.delete(libroHasAutor);
    }

}
