package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.Alumno;

public interface AlumnoService {

	public abstract List<Alumno> listaTodos();

	public abstract Alumno insertaAlumno(Alumno obj);

	public abstract Alumno insertaActualizaAlumno(Alumno obj);

	public abstract List<Alumno> listaAlumnoPorNombreLike(String nombres);

	public abstract void eliminaAlumno(int idAlumno);

	// Validaciones Registra
	public abstract List<Alumno> listaPorDNIIgualRegistra(String dni);

	public abstract List<Alumno> listaPorCorreoIgualRegistra(String correo);

	// Validaciones Actualiza
	public abstract List<Alumno> listaPorDNIIgualActualiza(String dni, int idAlumno);

	public abstract List<Alumno> listaPorCorreoIgualActualiza(String correo, int idAlumno);

	// Consulta service
	public abstract List<Alumno> listaConsultaAlumno(String nombres, String apellidos, String telefono, String dni,
			String correo, int estado, int idPais, int idModalidad);
}
