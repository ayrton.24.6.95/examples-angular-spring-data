package com.centroinformacion.service;

import java.util.List;
import java.util.Optional;

import com.centroinformacion.entity.Libro;


public interface LibroService {
	
	//registrar pc1
	public abstract List<Libro> listaLibro();
	public abstract Libro insertarLibro(Libro obj);
	
	//crud pc2
	public abstract List<Libro> listaLibroPorTituloLike(String titulo);
    public abstract Libro insertaActualizaLibro(Libro libro);
    public abstract void eliminaLibro(int idLibro);
    public abstract Optional<Libro> buscaEmpleado(int idLibro);
    
    //validaciones
    public abstract List<Libro> listaPorSerieIgualRegistrar(String serie);
    public abstract List<Libro> listaPorSerieIgualActualizar(String serie, int idLibro);
    
    //consultar
    public abstract List<Libro> listaConsulta(String titulo,int anio,String serie, int estado,int categoriaLibro,int tipoLibro);
    
}
