package com.centroinformacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Editorial;
import com.centroinformacion.repository.EditorialRepository;

@Service
public class EditorialServiceImpl implements EditorialService {

	@Autowired
	private EditorialRepository repository;

	@Override
	public Editorial registrarEditorial(Editorial editorial) {

		return repository.save(editorial);

	}

	@Override
	public List<Editorial> listarEditoriales() {
		return repository.findAll();
	}

	@Override
	public Editorial actualizarEditorial(Editorial editorial) {
		var objAct = repository.findById(editorial.getIdEditorial());

		objAct.get().setRazonSocial(editorial.getRazonSocial());
		objAct.get().setDireccion(editorial.getDireccion());
		objAct.get().setRuc(editorial.getRuc());
		objAct.get().setEstado(editorial.getEstado());
		objAct.get().setPais(editorial.getPais());
		objAct.get().setUsuarioRegistro(editorial.getUsuarioRegistro());

		return repository.save(objAct.get());
	}

	@Override
	public void eliminarEditorial(Editorial editorial) {
		repository.delete(editorial);

	}

	@Override
	public Editorial buscarPorId(Integer id) {
		return repository.findById(id).get();
	}
	
	@Override
	public List<Editorial> buscarPorParametros(String razon, String direccion, String ruc, int estado, int idUbigeo) {
		return repository.listaConsulta(razon, direccion, ruc, estado, idUbigeo);
	}

}
