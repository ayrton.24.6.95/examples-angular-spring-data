package com.centroinformacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Sala;
import com.centroinformacion.repository.SalaRepository;

@Service
public class SalaServiceImp implements SalaService {
	
	@Autowired
	private SalaRepository repository;

	@Override
	public Sala insertaActualizaSala(Sala obj) {
		return repository.save(obj);
	}

	@Override
	public List<Sala> listaSala() {
		return repository.findAll();
	}

	@Override
	public List<Sala> listaSalaxNumeroLike(String numero) {
		return repository.listaSalaxNumeroLike(numero);
	}

	@Override
	public List<Sala> listaNumeroIgualRegistrar(String numero) {
		return repository.listaNumeroIgualRegistrar(numero);
	}

	@Override
	public List<Sala> listaNumeroIgualActualizar(String numero, int idSala) {
		return repository.listaNumeroIgualActualizar(numero, idSala);
	}

	@Override
	public void eliminarSala(int idSala) {
		repository.deleteById(idSala);
	}

	@Override
	public List<Sala> buscarPorParametros(String numero, String recursos, int tipoSala, int idSede, int estado) {
		return repository.buscarPorParametros(numero, recursos, tipoSala, idSede, estado);
	}
	
}
