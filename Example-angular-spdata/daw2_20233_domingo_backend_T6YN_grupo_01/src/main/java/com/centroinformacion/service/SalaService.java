package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.Sala;

public interface SalaService {
	public abstract Sala insertaActualizaSala(Sala obj);
	public abstract List<Sala> listaSala();
	
	public abstract List<Sala> listaSalaxNumeroLike(String numero);
	public abstract List<Sala> listaNumeroIgualRegistrar(String numero);
	public abstract List<Sala> listaNumeroIgualActualizar(String numero, int idSala);
	public abstract void eliminarSala(int idSala);
	
	public abstract List<Sala> buscarPorParametros(String numero, String recursos, int tipoSala, int idSede, int estado);
}
