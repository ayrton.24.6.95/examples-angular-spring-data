package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.Proveedor;

public interface ProveedorService {
	
	public abstract List<Proveedor> listaProveedor();
	public abstract Proveedor instertaActualizaProveedor(Proveedor obj);
	
	//CRUD PROVEEDOR
	public abstract List<Proveedor> listaProveedorPorNombreLike(String razonsocial);
	public abstract void eliminaProveedor(int idProveedor);
	
	//validaciones CRUD data que no se repita en registrar y actualizar
	public abstract List<Proveedor> listaPorRazonSocialIgualRegistra(String razonsocial);
	public abstract List<Proveedor> listaPorRucIgualRegistra(String ruc);
	public abstract List<Proveedor> listaPorCelularIgualRegistra(String celular);
	
	public abstract List<Proveedor> listaPorRazonSocialIgualActualiza(String razonsocial, int idProveedor);
	public abstract List<Proveedor> listaPorRucIgualActualiza(String ruc, int idProveedor);
	public abstract List<Proveedor> listaPorCelularIgualActualiza(String celular, int idProveedor);
	
	//Consulta P03
	public abstract List<Proveedor> listaConsulta(String razonsocial, String ruc, int estado, int idPais, int idDataCatalogo);

}
