package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.Editorial;

public interface EditorialService {

	public Editorial registrarEditorial(Editorial editorial);
	
	public List<Editorial> listarEditoriales();

	public Editorial actualizarEditorial(Editorial editorial);

	public void eliminarEditorial(Editorial editorial);
	
	public Editorial buscarPorId(Integer id);
	
	public List<Editorial> buscarPorParametros(String razon, String direccion, String ruc, int estado, int idUbigeo);

}
