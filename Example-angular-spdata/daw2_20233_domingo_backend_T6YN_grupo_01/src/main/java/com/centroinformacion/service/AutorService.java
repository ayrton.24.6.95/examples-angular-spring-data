package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.Autor;

public interface AutorService {
	// para PC1
	public abstract List<Autor> listaAutor();

	// para PC1 y PC2 para el registrar
	public abstract Autor insertaActualizaAutor(Autor obj);

	// para PC2
	public abstract List<Autor> listaAutorPorNombreLike(String nombres);

	public abstract void eliminaAutor(int idAutor);

	// para PC2 validaciones unicas
	public abstract List<Autor> listaPorNombreIgualRegistra(String nombres, String apellidos);

	public abstract List<Autor> listaPorTelefonoIgualRegistra(String telefono);

	public abstract List<Autor> listaPorNombreIgualActualizar(String nombres, String apellidos, int idAutor);

	public abstract List<Autor> listaPorTelefonoIgualActualizar(String telefono, int idAutor);
	
	// para PC3 CONSULTA	
	public abstract List<Autor> listaConsultaAutor(String nombres, String apellidos, String telefono, int estado, int idPais, int idGrado);
	

} // fin de interface AutorService
