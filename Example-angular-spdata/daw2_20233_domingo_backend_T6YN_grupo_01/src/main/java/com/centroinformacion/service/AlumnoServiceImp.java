package com.centroinformacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Alumno;
import com.centroinformacion.repository.AlumnoRepository;

@Service
public class AlumnoServiceImp implements AlumnoService {

	@Autowired
	private AlumnoRepository repository;

	@Override
	public List<Alumno> listaTodos() {
		return repository.findByOrderByApellidosAsc();
	}

	@Override
	public void eliminaAlumno(int idAlumno) {
		repository.deleteById(idAlumno);
		
	}

	@Override
	public Alumno insertaActualizaAlumno(Alumno obj) {
		// TODO Auto-generated method stub
		return repository.save(obj);
	}

	@Override
	public List<Alumno> listaAlumnoPorNombreLike(String nombres) {
		// TODO Auto-generated method stub
		return repository.listaPorNombreLike(nombres);
	}

	@Override
	public Alumno insertaAlumno(Alumno obj) {
		// TODO Auto-generated method stub
		return repository.save(obj);
	}
	
	
	//Validaciones para Registrar
	@Override
	public List<Alumno> listaPorDNIIgualRegistra(String dni) {
		// TODO Auto-generated method stub
		return repository.listaPorDNIIgualRegistra(dni);
	}

	@Override
	public List<Alumno> listaPorCorreoIgualRegistra(String correo) {
		// TODO Auto-generated method stub
		return repository.listaPorCorreoIgualRegistra(correo);
	}
	
	//Validaciones para Actualizar
	@Override
	public List<Alumno> listaPorDNIIgualActualiza(String dni, int idAlumno) {
		// TODO Auto-generated method stub
		return repository.listaPorDNIIgualActualiza(dni, idAlumno);
	}

	@Override
	public List<Alumno> listaPorCorreoIgualActualiza(String correo, int idAlumno) {
		// TODO Auto-generated method stub
		return repository.listaPorCorreoIgualActualiza(correo, idAlumno);
	}
	
	//consultas serviceImple
	@Override
	public List<Alumno> listaConsultaAlumno(String nombres, String apellidos, String telefono, String dni,
			String correo, int estado, int idPais, int idModalidad) {
		// TODO Auto-generated method stub
		return repository.listaConsultaAlumno(nombres, apellidos, telefono, dni, correo, estado, idPais, idModalidad);
	}	

}
