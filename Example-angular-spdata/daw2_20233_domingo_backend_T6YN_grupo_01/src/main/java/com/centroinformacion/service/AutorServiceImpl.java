package com.centroinformacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centroinformacion.entity.Autor;
import com.centroinformacion.repository.AutorRepository;

@Service
public class AutorServiceImpl implements AutorService {

	@Autowired
	private AutorRepository repository;

	// PARA PC1 y PC2
	@Override
	public Autor insertaActualizaAutor(Autor obj) {
		return repository.save(obj);

	}

	// PARA PC1
	@Override
	public List<Autor> listaAutor() {
		return repository.findAll();
	}

	// PARA PC2
	@Override
	public List<Autor> listaAutorPorNombreLike(String nombres) {
		return repository.listaPorNombreLike(nombres);
	}

	// PARA PC2
	@Override
	public void eliminaAutor(int idAutor) {
		repository.deleteById(idAutor);
	}

	@Override
	public List<Autor> listaPorNombreIgualRegistra(String nombres, String apellidos) {
		return repository.listaPorNombreIgualRegistra(nombres, apellidos);
	}

	@Override
	public List<Autor> listaPorTelefonoIgualRegistra(String telefono) {
		return repository.listaPorTelefonoIgualRegistra(telefono);
	}

	@Override
	public List<Autor> listaPorNombreIgualActualizar(String nombres, String apellidos, int idAutor) {
		return repository.listaPorNombreIgualActualizar(nombres, apellidos, idAutor);
	}

	@Override
	public List<Autor> listaPorTelefonoIgualActualizar(String telefono, int idAutor) {
		return repository.listaPorTelefonoIgualActualizar(telefono, idAutor);
	}
	// pc3

	@Override
	public List<Autor> listaConsultaAutor(String nombres, String apellidos, String telefono, int estado, int idPais,
			int idGrado) {
		return repository.listaConsultaAutor(nombres, apellidos, telefono, estado, idPais, idGrado);
	}

}
