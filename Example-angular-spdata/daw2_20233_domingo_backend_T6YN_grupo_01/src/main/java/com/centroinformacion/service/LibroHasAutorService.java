package com.centroinformacion.service;

import java.util.List;

import com.centroinformacion.entity.LibroHasAutor;

public interface LibroHasAutorService {
    
    public LibroHasAutor registrarLibroHasAutor(LibroHasAutor libroHasAutor);
	
	public List<LibroHasAutor> listarLibroHasAutors();

    public LibroHasAutor consulLibroHasAutor(int idLibro, int idAutor);
    

    public void eliminarLibroHasAutor(int idLibro, int idAutor);
}
