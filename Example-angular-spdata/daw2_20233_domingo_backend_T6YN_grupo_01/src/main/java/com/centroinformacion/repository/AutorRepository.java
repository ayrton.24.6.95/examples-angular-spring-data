package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Autor;

public interface AutorRepository extends JpaRepository<Autor,Integer> {
	
	//para PC2 - el select (debe tener los mismos nombres de entidad y de los campos)
	@Query("select x from Autor x where x.nombres like ?1")
	public List<Autor> listaPorNombreLike(String nombres);
	
	//para PC2 Validaciones CRUD Registrar Nombres y apellidos para que se puedan
	//registrar personas con el mismo nombre pero diferente apellido
	@Query("select x from Autor x where x.nombres = ?1 and x.apellidos = ?2")
	public abstract List<Autor> listaPorNombreIgualRegistra(String nombres, String apellidos);
	
	//para PC2 Validaciones CRUD Registrar Telefono
	@Query("select x from Autor x where x.telefono = ?1")
	public abstract List<Autor> listaPorTelefonoIgualRegistra(String telefono);
	
	//para PC2 Validaciones CRUD Actualizar Nombres excluyendo el id y los apellidos para
	//que se puedan actualizar personas con el mismo nombre pero diferente apellido
	@Query("select x from Autor x where x.nombres = ?1 and x.apellidos = ?2 and x.idAutor != ?3")
	public abstract List<Autor> listaPorNombreIgualActualizar(String nombres,String apellidos, int idAutor);
	
	//para PC2 Validaciones CRUD Actualizar Telefono excluyendo el id
	@Query("select x from Autor x where x.telefono = ?1 and x.idAutor != ?2")
	public abstract List<Autor> listaPorTelefonoIgualActualizar(String telefono, int idAutor);
	
	// ****** PC3 CONSULTA******************
	@Query ("select x from Autor x where "
			+ "(x.nombres like ?1) and "
			+ "(x.apellidos like ?2) and "
			+ "(?3 = '' or x.telefono = ?3) and "
			+ "(x.estado = ?4) and "			
			+ "(?5 = -1 or x.pais.idPais = ?5) and "			
			+ "(?6 = -1 or x.grado.idDataCatalogo = ?6)")
	
	public abstract List<Autor> listaConsultaAutor(String nombres, String apellidos, String telefono, int estado, int idPais, int idGrado);

	
} // fin de public interface AutorRepository







