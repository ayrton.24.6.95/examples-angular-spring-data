package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Alumno;

public interface AlumnoRepository extends JpaRepository<Alumno, Integer>{
	
	@Query("select x from Alumno x where x.nombres like ?1")
	public List<Alumno> listaPorNombreLike(String nombres);
	
	public abstract List<Alumno> findByOrderByApellidosAsc();
	
	//Validacion Registro CRUD Registrar
		@Query("select x from Alumno x where x.dni = ?1")
		public abstract List<Alumno> listaPorDNIIgualRegistra(String dni);
		
		@Query("select x from Alumno x where x.correo = ?1")
		public abstract List<Alumno> listaPorCorreoIgualRegistra(String correo);
		
	//Validaciones CRUD Actualizar	
		@Query("select x from Alumno x where x.dni = ?1 and x.idAlumno != ?2")
		public abstract List<Alumno> listaPorDNIIgualActualiza(String dni, int idAlumno);
		
		@Query("select x from Alumno x where x.correo = ?1 and x.idAlumno != ?2")
		public abstract List<Alumno> listaPorCorreoIgualActualiza(String correo, int idAlumno);
		
		//Consultas
		@Query("select x from Alumno x where "
								+ "(x.nombres like ?1) and "
								+ "(x.apellidos like ?2) and "
								+ "(x.telefono like ?3) and "
								+ "(?4 = '' or x.dni = ?4) and "
								+ "(x.correo like ?5) and "
								//+ "(?6 = '' or x.fechaNacimiento = ?6) and "
								+ "(x.estado = ?6 ) and "	
								+ "(?7 = -1 or x.pais.idPais = ?7)and "
								+ "(?8 = -1 or x.modalidad.idDataCatalogo = ?8)")
								
		public abstract List<Alumno> listaConsultaAlumno(String nombres, String apellidos, String telefono, 
				String dni, String correo, int estado,  int idPais, int idModalidad);
		
		
}
