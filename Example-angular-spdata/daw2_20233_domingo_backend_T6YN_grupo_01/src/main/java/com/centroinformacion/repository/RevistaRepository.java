package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Revista;

public interface RevistaRepository extends JpaRepository<Revista, Integer> {
	
	
	@Query("select x from Revista x where x.nombre like ?1")
	public List<Revista> listaPorNombreLike(String nombre);
	
	//Validaciones CRUD Registrar nombre
	@Query("select x from Revista x where x.nombre = ?1")
	public abstract List<Revista> listaPorNombreIgualRegistra(String nombre);
	
	//Validaciones CRUD Registrar frecuencia
	@Query("select x from Revista x where x.frecuencia = ?1")
	public abstract List<Revista> listaPorFrecuenciaIgualRegistra(String frecuencia);
	
	
	
	//Validaciones CRUD Actualiza
	@Query("select x from Revista x where x.nombre = ?1 and x.idRevista != ?2")
	public abstract List<Revista> listaPorNombreIgualActualizar(String nombre, int idRevista);
	
	//Validaciones CRUD Actualiza
	@Query("select x from Revista x where x.frecuencia = ?1 and x.idRevista != ?2")
	public abstract List<Revista> listaPorFreceunciaIgualActualizar(String frecuencia, int idRevista);
	// consulta
	
	@Query ("select x from Revista x where "
			+ "(x.nombre like ?1) and "
			+ "(x.frecuencia like ?2) and "
			+ "(x.estado = ?3) and "			
			+ "(?4 = -1 or x.pais.idPais = ?4) and "			
			+ "(?5 = -1 or x.tipoRevista.idDataCatalogo = ?5)")
	public abstract List<Revista> listaConsultaRevista(String nombre, String frecuencia, int estado, int pais, int tipoRevista);

}

