package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Proveedor;

public interface ProveedorRepository extends JpaRepository<Proveedor, Integer> {
	
	//para el filtro
	@Query("select x from Proveedor x where x.razonsocial like ?1")
	public abstract List<Proveedor> listaPorNombreLike(String razonsocial);
	
	//validaciones CRUD
	@Query("select x from Proveedor x where x.razonsocial = ?1")
	public abstract List<Proveedor> listaPorRazonSocialIgualRegistra(String razonsocial);
	
	@Query("select x from Proveedor x where x.ruc = ?1")
	public abstract List<Proveedor> listaPorRucIgualRegistra(String ruc);
	
	@Query("select x from Proveedor x where x.celular = ?1")
	public abstract List<Proveedor> listaPorCelularIgualRegistra(String celular);
	
	@Query("select x from Proveedor x where x.razonsocial = ?1 and x.idProveedor != ?2")
	public abstract List<Proveedor> listaPorRazonSocialIgualActualiza(String razonsocial, int idProveedor);
	
	@Query("select x from Proveedor x where x.ruc = ?1 and x.idProveedor != ?2")
	public abstract List<Proveedor> listaPorRucIgualActualiza(String ruc, int idProveedor);
	
	@Query("select x from Proveedor x where x.celular = ?1 and x.idProveedor != ?2")
	public abstract List<Proveedor> listaPorCelularIgualActualiza(String celular, int idProveedor);
	
	//consulta P03
	@Query("SELECT pro FROM Proveedor pro WHERE "
	        + "(pro.razonsocial LIKE ?1) AND "
	        + "(?2 = '' OR pro.ruc LIKE ?2) AND "
	        + "(pro.estado = ?3) AND "
	        + "(?4 = -1 OR pro.pais.idPais = ?4) AND "
	        + "(?5 = -1 OR pro.tipoProveedor.idDataCatalogo = ?5)")
	public abstract List<Proveedor> listaConsulta(String razonsocial, String ruc, int estado, int idPais, int idDataCatalogo);

}
