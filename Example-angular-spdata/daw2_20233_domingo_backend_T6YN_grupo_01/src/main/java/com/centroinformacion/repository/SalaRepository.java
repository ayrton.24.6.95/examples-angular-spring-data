package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Sala;

public interface SalaRepository extends JpaRepository<Sala, Integer>{

	@Query("select x from Sala x where x.numero like ?1")
	public List<Sala> listaSalaxNumeroLike(String numero);
	
	@Query("select x from Sala x where x.numero = ?1")
	public List<Sala> listaNumeroIgualRegistrar(String numero);
	
	@Query("select x from Sala x where x.numero = ?1 and x.idSala != ?2")
	public abstract List<Sala> listaNumeroIgualActualizar(String numero, int idSala);
	
	@Query("select x from Sala x where "+
			 "(x.numero like ?1) and "+
		     "(x.recursos like ?2) and "+
			 "(?3 = -1 or x.tipoSala.idDataCatalogo = ?3) and "+
		     "(?4 = -1 or x.sede.idDataCatalogo = ?4) and "+
			 "(x.estado = ?5)")
	public abstract List<Sala> buscarPorParametros(String numero, String recursos, int tipoSala, int idSede, int estado);
	
}
