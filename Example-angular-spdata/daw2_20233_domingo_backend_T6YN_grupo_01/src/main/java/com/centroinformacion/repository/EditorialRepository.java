package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Editorial;

public interface EditorialRepository extends JpaRepository<Editorial, Integer> {

	// CONSULTAR POR PARAMETROS
	@Query("select x from Editorial x where " + "(x.razonSocial like ?1) and " + "(?2 = '' or x.direccion like ?2) and "
	        + "(?3 = '' or x.ruc like ?3) and "
	        + "(x.estado = ?4 ) and " + "(?5 = -1 or x.pais.idPais = ?5)")
	public abstract List<Editorial> listaConsulta(String razon, String direccion, String ruc, int estado, int idUbigeo);


}
