package com.centroinformacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.centroinformacion.entity.Libro;

public interface LibroRepository extends JpaRepository<Libro, Integer>{
	
	@Query("select x from Libro x where x .titulo like ?1")
	public List<Libro> listaLibroPorTituloLike(String titulo);
	
	//registrar
	@Query("select x from Libro x where x.serie = ?1")
	public List<Libro> listaPorSerieIgualRegistrar(String serie);
		
	//actualizar
	@Query("select x from Libro x where x.serie = ?1 and x .idLibro != ?2")
	public abstract List<Libro> listaPorSerieIgualActualizar(String serie, int idLibro);
	
	//consultar
	@Query("select x from Libro x where "
	        + "(x.titulo like ?1) and "
	        + "(?2 = 0 or x.anio = ?2) and "
	        + "(?3 = ' ' or x.serie like ?3) and "
	        + "(x.estado = ?4) and "
	        + "(?5 = -1 or x.categoriaLibro.idDataCatalogo = ?5) and "
	        + "(?6 = -1 or x.tipoLibro.idDataCatalogo = ?6)")
	public abstract List<Libro> listaConsulta(String titulo,int anio,String serie,int estado, int categoriaLibro,int tipoLibro);
	

}

