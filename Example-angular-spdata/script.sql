-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistema_biblioteca_2023_02_security
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumno` (
  `idAlumno` int NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `dni` char(8) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idPais` int NOT NULL,
  `idModalidad` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idAlumno`),
  KEY `fk_alumno_pais_idx` (`idPais`),
  KEY `fk_alumno_catalogo_idx` (`idModalidad`),
  KEY `fk_alumno_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_alumno_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_alumno_modalidad` FOREIGN KEY (`idModalidad`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_alumno_pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`),
  CONSTRAINT `fk_alumno_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_alumno_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (1,'Elba Carlos','Flores Arcos','912345678','74859685','elbafloreso@gmail.com','2000-10-10','2022-04-03 22:59:04','2023-06-25 23:12:00',1,2,9,2,2),(2,'Juan Jorge','Quispe Llanos','998574858','87474747','juanPerez@gmail.com','2010-10-10','2022-04-04 10:59:07','2022-04-04 10:59:07',1,4,9,2,2);
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autor`
--

DROP TABLE IF EXISTS `autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autor` (
  `idAutor` int NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idPais` int NOT NULL,
  `idGrado` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idAutor`),
  KEY `fk_autor_pais_idx` (`idPais`),
  KEY `fk_autor_grado_idx` (`idGrado`),
  KEY `fk_autor_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_autor_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_autor_grado` FOREIGN KEY (`idGrado`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_autor_pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`),
  CONSTRAINT `fk_autor_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_autor_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autor`
--

LOCK TABLES `autor` WRITE;
/*!40000 ALTER TABLE `autor` DISABLE KEYS */;
INSERT INTO `autor` VALUES (1,'Cesar ','Vallejo','2020-10-11','912345678','2022-04-04 11:59:07','2022-04-04 11:59:07',1,4,11,2,2),(2,'Vargas ','LLosa','1995-10-11','998874747','2022-04-04 10:57:07','2022-04-04 10:57:07',1,3,11,2,2),(3,'José María ','Arguedas','1995-10-11','998874747','2022-04-04 10:57:07','2022-04-04 10:57:07',1,4,11,2,2);
/*!40000 ALTER TABLE `autor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogo`
--

DROP TABLE IF EXISTS `catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `catalogo` (
  `idCatalogo` int NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`idCatalogo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo`
--

LOCK TABLES `catalogo` WRITE;
/*!40000 ALTER TABLE `catalogo` DISABLE KEYS */;
INSERT INTO `catalogo` VALUES (1,'Categoria de Libro'),(2,'Tipo de Proveedor'),(3,'Modalidad de Alumno'),(4,'Grado de Autor'),(5,'Tipo de Libro y Revista'),(6,'Tipo de Sala'),(7,'Sede'),(8,'Estado de Libro');
/*!40000 ALTER TABLE `catalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_catalogo`
--

DROP TABLE IF EXISTS `data_catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_catalogo` (
  `idDataCatalogo` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `idCatalogo` int NOT NULL,
  PRIMARY KEY (`idDataCatalogo`),
  KEY `fk_datacatalogo_catalogo_idx` (`idCatalogo`),
  CONSTRAINT `fk_datacatalogo_catalogo` FOREIGN KEY (`idCatalogo`) REFERENCES `catalogo` (`idCatalogo`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_catalogo`
--

LOCK TABLES `data_catalogo` WRITE;
/*!40000 ALTER TABLE `data_catalogo` DISABLE KEYS */;
INSERT INTO `data_catalogo` VALUES (1,'Novela',1),(2,'Cuento',1),(3,'Poesía',1),(4,'Enciclopedia',1),(5,'Local',2),(6,'Nacional',2),(7,'Internacional',2),(8,'Global',2),(9,'Virtual',3),(10,'Presencial',3),(11,'Técnico',4),(12,'Profesional',4),(13,'Magister',4),(14,'Doctor',4),(15,'Digital',5),(16,'Físico',5),(17,'Laboratorio',6),(18,'Lectura',6),(19,'Ayacucho',7),(20,'Centro',7),(21,'San Miguel',7),(22,'Miraflores',7),(23,'Independencia',7),(24,'Lima',7),(25,'Arequipa',7),(26,'Prestado',8),(27,'Disponible',8);
/*!40000 ALTER TABLE `data_catalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devolucion`
--

DROP TABLE IF EXISTS `devolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devolucion` (
  `idDevolucion` int NOT NULL AUTO_INCREMENT,
  `fechaPrestamo` date NOT NULL,
  `fechaDevolucion` date NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `idAlumno` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  PRIMARY KEY (`idDevolucion`),
  KEY `fk_devolucion_alumno_idx1` (`idAlumno`),
  KEY `fk_devolucion_usuario_idx` (`idUsuarioRegistro`),
  CONSTRAINT `fk_devolucion_alumno` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `fk_devolucion_usuario` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devolucion`
--

LOCK TABLES `devolucion` WRITE;
/*!40000 ALTER TABLE `devolucion` DISABLE KEYS */;
INSERT INTO `devolucion` VALUES (1,'2023-09-04','2023-09-06','2023-09-04 11:59:07',1,2);
/*!40000 ALTER TABLE `devolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devolucion_tiene_libro`
--

DROP TABLE IF EXISTS `devolucion_tiene_libro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devolucion_tiene_libro` (
  `idDevolucion` int NOT NULL,
  `idLibro` int NOT NULL,
  PRIMARY KEY (`idDevolucion`,`idLibro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devolucion_tiene_libro`
--

LOCK TABLES `devolucion_tiene_libro` WRITE;
/*!40000 ALTER TABLE `devolucion_tiene_libro` DISABLE KEYS */;
INSERT INTO `devolucion_tiene_libro` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `devolucion_tiene_libro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `editorial`
--

DROP TABLE IF EXISTS `editorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `editorial` (
  `idEditorial` int NOT NULL AUTO_INCREMENT,
  `razonSocial` varchar(45) NOT NULL,
  `direccion` text NOT NULL,
  `ruc` varchar(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idPais` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idEditorial`),
  KEY `fk_editorial_pais_idx` (`idPais`),
  KEY `fk_editorial_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_editorial_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_editorial_pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`),
  CONSTRAINT `fk_editorial_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_editorial_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `editorial`
--

LOCK TABLES `editorial` WRITE;
/*!40000 ALTER TABLE `editorial` DISABLE KEYS */;
INSERT INTO `editorial` VALUES (1,'Isai Lazo','Av Lima 123','74859621414','1998-10-08','2022-04-04 10:59:07','2022-04-04 10:59:07',1,2,2,2),(2,'Editorial Lima','Av Tacna 747','87474747747','1995-01-01','2022-04-04 10:59:12','2022-04-04 10:59:12',1,4,2,2),(4,'Crisol SAC','Av. Alfonso Ugarte 123','12345678911','2023-12-04','2023-12-05 21:51:22','2023-12-05 21:51:22',1,173,1,1),(5,'San Marcos','Av. Filipinas 753','85236974185','2023-12-05','2023-12-05 21:55:40','2023-12-05 21:55:40',1,13,1,1),(6,'Libreria SA','Av. Mexico 153','96325875312','2023-12-01','2023-12-05 21:59:38','2023-12-05 21:59:38',1,1,1,1);
/*!40000 ALTER TABLE `editorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ejemplo`
--

DROP TABLE IF EXISTS `ejemplo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ejemplo` (
  `idEjemplo` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` int NOT NULL,
  `idPais` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idEjemplo`),
  KEY `fk_prueba_pais_idx` (`idPais`),
  KEY `fk_prueba_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_prueba_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_prueba_pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`),
  CONSTRAINT `fk_prueba_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_prueba_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ejemplo`
--

LOCK TABLES `ejemplo` WRITE;
/*!40000 ALTER TABLE `ejemplo` DISABLE KEYS */;
INSERT INTO `ejemplo` VALUES (1,'Nuevo','2022-04-04 10:59:07','2022-04-04 10:59:07',1,4,2,2),(10,'Reclamo 22222','2023-09-13 11:47:10','2023-09-13 11:47:10',1,4,2,2),(11,'Reclamo 22222','2023-09-13 11:47:47','2023-09-13 11:47:47',1,4,2,2),(12,'Reclamo 22222','2023-09-13 11:52:47','2023-09-13 11:52:47',1,5,2,2),(13,'Ejemplo2','2023-09-13 16:38:03','2023-09-13 16:38:03',1,3,1,1),(14,'Reclamo 22222','2023-09-15 11:04:49','2023-09-15 11:04:49',1,3,1,1),(15,'Reclamo 22222','2023-09-15 11:05:31','2023-09-15 11:05:31',1,1,1,1),(16,'Reclamo 22222','2023-09-15 11:05:49','2023-09-15 11:05:49',1,1,1,1),(17,'AAAAA','2023-09-15 18:35:45','2023-09-15 18:35:45',1,3,1,1),(18,'AAAAA','2023-09-15 18:36:11','2023-09-15 18:36:11',1,4,2,2),(20,'AAA','2023-09-15 19:05:44','2023-09-15 19:05:44',1,1,1,1),(21,'AAAA','2023-10-15 01:49:33','2023-10-15 01:49:33',1,1,1,1),(22,'aaaa','2023-10-15 01:52:03','2023-10-15 01:52:03',1,3,1,1),(23,'AAA','2023-10-15 01:52:30','2023-10-15 01:52:30',1,3,1,1),(24,'AAA','2023-10-15 01:52:35','2023-10-15 01:52:35',1,3,1,1),(25,'AAA','2023-10-15 01:55:05','2023-10-15 01:55:05',1,3,1,1),(26,'AAAA','2023-10-15 02:01:34','2023-10-15 02:01:34',1,1,1,1),(27,'Prueba','2023-10-15 07:38:24','2023-10-15 07:38:24',1,1,1,1),(28,'Prueba','2023-10-15 07:39:30','2023-10-15 07:39:30',1,3,1,1),(29,'Prueba','2023-10-15 08:05:52','2023-10-15 08:05:52',1,14,1,1),(30,'AAAA','2023-10-15 11:05:29','2023-10-15 11:05:29',1,1,1,1);
/*!40000 ALTER TABLE `ejemplo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libro`
--

DROP TABLE IF EXISTS `libro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `libro` (
  `idLibro` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `anio` int NOT NULL,
  `serie` varchar(100) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idCategoriaLibro` int NOT NULL,
  `idTipoLibro` int NOT NULL,
  `idEstadoPrestamo` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idLibro`),
  KEY `fk_libro_tipo_idx` (`idCategoriaLibro`),
  KEY `fk_libro_modalidad_idx` (`idTipoLibro`),
  KEY `fk_libro_estado_idx` (`idEstadoPrestamo`),
  KEY `fk_libro_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_libro_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_libro_categoria` FOREIGN KEY (`idCategoriaLibro`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_libro_estado` FOREIGN KEY (`idEstadoPrestamo`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_libro_tipo` FOREIGN KEY (`idTipoLibro`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_libro_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_libro_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libro`
--

LOCK TABLES `libro` WRITE;
/*!40000 ALTER TABLE `libro` DISABLE KEYS */;
INSERT INTO `libro` VALUES (1,'Trilce',2020,'AX4714774747','2022-04-04 11:59:07','2022-04-04 11:59:07',1,1,15,27,2,2),(2,'Los Rios Profundos',2021,'XS7474499888','2022-04-04 11:59:07','2022-04-04 11:59:07',1,1,15,27,2,2),(3,'La Ciudad y los Perros',2000,'MN8747474854','2022-04-04 11:59:07','2022-04-04 11:59:07',1,1,15,27,2,2),(4,'La tía Julia',2000,'PQ9874747475','2022-04-04 11:59:07','2022-04-04 11:59:07',1,1,15,27,2,2);
/*!40000 ALTER TABLE `libro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libro_tiene_autor`
--

DROP TABLE IF EXISTS `libro_tiene_autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `libro_tiene_autor` (
  `idLibro` int NOT NULL,
  `idAutor` int NOT NULL,
  PRIMARY KEY (`idLibro`,`idAutor`),
  KEY `fk_libro_has_autor_autor1_idx` (`idAutor`),
  KEY `fk_libro_has_autor_libro_idx` (`idLibro`),
  CONSTRAINT `fk_libro_has_autor_autor1` FOREIGN KEY (`idAutor`) REFERENCES `autor` (`idAutor`),
  CONSTRAINT `fk_libro_has_autor_libro` FOREIGN KEY (`idLibro`) REFERENCES `libro` (`idLibro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libro_tiene_autor`
--

LOCK TABLES `libro_tiene_autor` WRITE;
/*!40000 ALTER TABLE `libro_tiene_autor` DISABLE KEYS */;
INSERT INTO `libro_tiene_autor` VALUES (2,1),(1,2),(1,3),(2,3),(3,3),(4,3);
/*!40000 ALTER TABLE `libro_tiene_autor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion`
--

DROP TABLE IF EXISTS `opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opcion` (
  `idOpcion` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `ruta` text,
  `tipo` smallint DEFAULT NULL,
  PRIMARY KEY (`idOpcion`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion`
--

LOCK TABLES `opcion` WRITE;
/*!40000 ALTER TABLE `opcion` DISABLE KEYS */;
INSERT INTO `opcion` VALUES (1,'Registro Alumno','1','verRegistroAlumno',1),(2,'Registro Libro','1','verRegistroLibro',1),(3,'Registro Tesis','1','verRegistroTesis',1),(4,'Registro Autor','1','verRegistroAutor',1),(5,'Registro Sala','1','verRegistroSala',1),(6,'Registro Proveedor','1','verRegistroProveedor',1),(7,'Registro Editorial','1','verRegistroEditorial',1),(8,'Registro Revista','1','verRegistroRevista',1),(9,'Registro Ejemplo','1','verRegistroEjemplo',1),(10,'Consulta y Reporte Alumno','1','verConsultaAlumno',2),(11,'Consulta y Reporte Libro','1','verConsultaLibro',2),(12,'Consulta y Reporte Tesis','1','verConsultaTesis',2),(13,'Consulta y Reporte Autor','1','verConsultaAutor',2),(14,'Consulta y Reporte Sala','1','verConsultaSala',2),(15,'Consulta y Reporte Proveedor','1','verConsultaProveedor',2),(16,'Consulta y Reporte Editorial','1','verConsultaEditorial',2),(17,'Consulta y Reporte Revista','1','verConsultaRevista',2),(18,'Crud Alumno','1','verCrudAlumno',3),(19,'Crud Libro','1','verCrudLibro',3),(20,'Crud Tesis','1','verCrudTesis',3),(21,'Crud Autor','1','verCrudAutor',3),(22,'Crud Sala','1','verCrudSala',3),(25,'Crud Proveedor','1','verCrudProveedor',3),(26,'Crud Editorial','1','verCrudEditorial',3),(27,'Crud Revista','1','verCrudRevista',3),(28,'Asignación de Rol a Usuario','1','VerAsignacionRol',4),(29,'Asignación de Opción a Rol','1','VerAsignacionOpcion',4),(30,'Asignación de Libro a Autor','1','VerAsignacionLibro',4),(31,'Préstamo de Libro','1','verPrestamoLibro',4),(32,'Devolución de Libro','1','verDevolucionLibro',4),(33,'Reserva de Sala','1','verReservaSala',4),(34,'Reporte de Libro','1','verReporteLibro',4),(35,'Reporte de Sala','1','verReporteSala',4);
/*!40000 ALTER TABLE `opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pais` (
  `idPais` int NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'AF','Afganistán'),(2,'AX','Islas Gland'),(3,'AL','Albania'),(4,'DE','Alemania'),(5,'AD','Andorra'),(6,'AO','Angola'),(7,'AI','Anguilla'),(8,'AQ','Antártida'),(9,'AG','Antigua y Barbuda'),(10,'AN','Antillas Holandesas'),(11,'SA','Arabia Saudí'),(12,'DZ','Argelia'),(13,'AR','Argentina'),(14,'AM','Armenia'),(15,'AW','Aruba'),(16,'AU','Australia'),(17,'AT','Austria'),(18,'AZ','Azerbaiyán'),(19,'BS','Bahamas'),(20,'BH','Bahréin'),(21,'BD','Bangladesh'),(22,'BB','Barbados'),(23,'BY','Bielorrusia'),(24,'BE','Bélgica'),(25,'BZ','Belice'),(26,'BJ','Benin'),(27,'BM','Bermudas'),(28,'BT','Bhután'),(29,'BO','Bolivia'),(30,'BA','Bosnia y Herzegovina'),(31,'BW','Botsuana'),(32,'BV','Isla Bouvet'),(33,'BR','Brasil'),(34,'BN','Brunéi'),(35,'BG','Bulgaria'),(36,'BF','Burkina Faso'),(37,'BI','Burundi'),(38,'CV','Cabo Verde'),(39,'KY','Islas Caimán'),(40,'KH','Camboya'),(41,'CM','Camerún'),(42,'CA','Canadá'),(43,'CF','República Centroafricana'),(44,'TD','Chad'),(45,'CZ','República Checa'),(46,'CL','Chile'),(47,'CN','China'),(48,'CY','Chipre'),(49,'CX','Isla de Navidad'),(50,'VA','Ciudad del Vaticano'),(51,'CC','Islas Cocos'),(52,'CO','Colombia'),(53,'KM','Comoras'),(54,'CD','República Democrática del Congo'),(55,'CG','Congo'),(56,'CK','Islas Cook'),(57,'KP','Corea del Norte'),(58,'KR','Corea del Sur'),(59,'CI','Costa de Marfil'),(60,'CR','Costa Rica'),(61,'HR','Croacia'),(62,'CU','Cuba'),(63,'DK','Dinamarca'),(64,'DM','Dominica'),(65,'DO','República Dominicana'),(66,'EC','Ecuador'),(67,'EG','Egipto'),(68,'SV','El Salvador'),(69,'AE','Emiratos Árabes Unidos'),(70,'ER','Eritrea'),(71,'SK','Eslovaquia'),(72,'SI','Eslovenia'),(73,'ES','España'),(74,'UM','Islas ultramarinas de Estados Unidos'),(75,'US','Estados Unidos'),(76,'EE','Estonia'),(77,'ET','Etiopía'),(78,'FO','Islas Feroe'),(79,'PH','Filipinas'),(80,'FI','Finlandia'),(81,'FJ','Fiyi'),(82,'FR','Francia'),(83,'GA','Gabón'),(84,'GM','Gambia'),(85,'GE','Georgia'),(86,'GS','Islas Georgias del Sur y Sandwich del Sur'),(87,'GH','Ghana'),(88,'GI','Gibraltar'),(89,'GD','Granada'),(90,'GR','Grecia'),(91,'GL','Groenlandia'),(92,'GP','Guadalupe'),(93,'GU','Guam'),(94,'GT','Guatemala'),(95,'GF','Guayana Francesa'),(96,'GN','Guinea'),(97,'GQ','Guinea Ecuatorial'),(98,'GW','Guinea-Bissau'),(99,'GY','Guyana'),(100,'HT','Haití'),(101,'HM','Islas Heard y McDonald'),(102,'HN','Honduras'),(103,'HK','Hong Kong'),(104,'HU','Hungría'),(105,'IN','India'),(106,'ID','Indonesia'),(107,'IR','Irán'),(108,'IQ','Iraq'),(109,'IE','Irlanda'),(110,'IS','Islandia'),(111,'IL','Israel'),(112,'IT','Italia'),(113,'JM','Jamaica'),(114,'JP','Japón'),(115,'JO','Jordania'),(116,'KZ','Kazajstán'),(117,'KE','Kenia'),(118,'KG','Kirguistán'),(119,'KI','Kiribati'),(120,'KW','Kuwait'),(121,'LA','Laos'),(122,'LS','Lesotho'),(123,'LV','Letonia'),(124,'LB','Líbano'),(125,'LR','Liberia'),(126,'LY','Libia'),(127,'LI','Liechtenstein'),(128,'LT','Lituania'),(129,'LU','Luxemburgo'),(130,'MO','Macao'),(131,'MK','ARY Macedonia'),(132,'MG','Madagascar'),(133,'MY','Malasia'),(134,'MW','Malawi'),(135,'MV','Maldivas'),(136,'ML','Malí'),(137,'MT','Malta'),(138,'FK','Islas Malvinas'),(139,'MP','Islas Marianas del Norte'),(140,'MA','Marruecos'),(141,'MH','Islas Marshall'),(142,'MQ','Martinica'),(143,'MU','Mauricio'),(144,'MR','Mauritania'),(145,'YT','Mayotte'),(146,'MX','México'),(147,'FM','Micronesia'),(148,'MD','Moldavia'),(149,'MC','Mónaco'),(150,'MN','Mongolia'),(151,'MS','Montserrat'),(152,'MZ','Mozambique'),(153,'MM','Myanmar'),(154,'NA','Namibia'),(155,'NR','Nauru'),(156,'NP','Nepal'),(157,'NI','Nicaragua'),(158,'NE','Níger'),(159,'NG','Nigeria'),(160,'NU','Niue'),(161,'NF','Isla Norfolk'),(162,'NO','Noruega'),(163,'NC','Nueva Caledonia'),(164,'NZ','Nueva Zelanda'),(165,'OM','Omán'),(166,'NL','Países Bajos'),(167,'PK','Pakistán'),(168,'PW','Palau'),(169,'PS','Palestina'),(170,'PA','Panamá'),(171,'PG','Papúa Nueva Guinea'),(172,'PY','Paraguay'),(173,'PE','Perú'),(174,'PN','Islas Pitcairn'),(175,'PF','Polinesia Francesa'),(176,'PL','Polonia'),(177,'PT','Portugal'),(178,'PR','Puerto Rico'),(179,'QA','Qatar'),(180,'GB','Reino Unido'),(181,'RE','Reunión'),(182,'RW','Ruanda'),(183,'RO','Rumania'),(184,'RU','Rusia'),(185,'EH','Sahara Occidental'),(186,'SB','Islas Salomón'),(187,'WS','Samoa'),(188,'AS','Samoa Americana'),(189,'KN','San Cristóbal y Nevis'),(190,'SM','San Marino'),(191,'PM','San Pedro y Miquelón'),(192,'VC','San Vicente y las Granadinas'),(193,'SH','Santa Helena'),(194,'LC','Santa Lucía'),(195,'ST','Santo Tomé y Príncipe'),(196,'SN','Senegal'),(197,'CS','Serbia y Montenegro'),(198,'SC','Seychelles'),(199,'SL','Sierra Leona'),(200,'SG','Singapur'),(201,'SY','Siria'),(202,'SO','Somalia'),(203,'LK','Sri Lanka'),(204,'SZ','Suazilandia'),(205,'ZA','Sudáfrica'),(206,'SD','Sudán'),(207,'SE','Suecia'),(208,'CH','Suiza'),(209,'SR','Surinam'),(210,'SJ','Svalbard y Jan Mayen'),(211,'TH','Tailandia'),(212,'TW','Taiwán'),(213,'TZ','Tanzania'),(214,'TJ','Tayikistán'),(215,'IO','Territorio Británico del Océano Índico'),(216,'TF','Territorios Australes Franceses'),(217,'TL','Timor Oriental'),(218,'TG','Togo'),(219,'TK','Tokelau'),(220,'TO','Tonga'),(221,'TT','Trinidad y Tobago'),(222,'TN','Túnez'),(223,'TC','Islas Turcas y Caicos'),(224,'TM','Turkmenistán'),(225,'TR','Turquía'),(226,'TV','Tuvalu'),(227,'UA','Ucrania'),(228,'UG','Uganda'),(229,'UY','Uruguay'),(230,'UZ','Uzbekistán'),(231,'VU','Vanuatu'),(232,'VE','Venezuela'),(233,'VN','Vietnam'),(234,'VG','Islas Vírgenes Británicas'),(235,'VI','Islas Vírgenes de los Estados Unidos'),(236,'WF','Wallis y Futuna'),(237,'YE','Yemen'),(238,'DJ','Yibuti'),(239,'ZM','Zambia'),(240,'ZW','Zimbabue');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestamo`
--

DROP TABLE IF EXISTS `prestamo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prestamo` (
  `idPrestamo` int NOT NULL AUTO_INCREMENT,
  `fechaPrestamo` date NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaDevolucion` date NOT NULL,
  `idAlumno` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  PRIMARY KEY (`idPrestamo`),
  KEY `fk_prestamo_alumno1_idx` (`idAlumno`),
  KEY `fk_prestamo_usuario1_idx` (`idUsuarioRegistro`),
  CONSTRAINT `fk_prestamo_alumno1` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `fk_prestamo_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestamo`
--

LOCK TABLES `prestamo` WRITE;
/*!40000 ALTER TABLE `prestamo` DISABLE KEYS */;
INSERT INTO `prestamo` VALUES (1,'2023-09-04','2023-09-04 22:59:04','2023-09-07',1,2);
/*!40000 ALTER TABLE `prestamo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestamo_tiene_libro`
--

DROP TABLE IF EXISTS `prestamo_tiene_libro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prestamo_tiene_libro` (
  `idPrestamo` int NOT NULL,
  `idLibro` int NOT NULL,
  PRIMARY KEY (`idPrestamo`,`idLibro`),
  KEY `fk_prestamo_has_libro_libro1_idx` (`idLibro`),
  KEY `fk_prestamo_has_libro_prestamo1_idx` (`idPrestamo`),
  CONSTRAINT `fk_prestamo_has_libro_libro1` FOREIGN KEY (`idLibro`) REFERENCES `libro` (`idLibro`),
  CONSTRAINT `fk_prestamo_has_libro_prestamo1` FOREIGN KEY (`idPrestamo`) REFERENCES `prestamo` (`idPrestamo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestamo_tiene_libro`
--

LOCK TABLES `prestamo_tiene_libro` WRITE;
/*!40000 ALTER TABLE `prestamo_tiene_libro` DISABLE KEYS */;
INSERT INTO `prestamo_tiene_libro` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `prestamo_tiene_libro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proveedor` (
  `idProveedor` int NOT NULL AUTO_INCREMENT,
  `razonsocial` varchar(100) NOT NULL,
  `ruc` char(11) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `celular` char(9) NOT NULL,
  `contacto` varchar(200) NOT NULL,
  `estado` smallint NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `idPais` int NOT NULL,
  `idTipoProveedor` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idProveedor`),
  KEY `fk_proveedor_pais_idx` (`idPais`),
  KEY `fk_proveedor_tipo_idx` (`idTipoProveedor`),
  KEY `fk_proveedor_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_proveedor_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_proveedor_pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`),
  CONSTRAINT `fk_proveedor_tipo` FOREIGN KEY (`idTipoProveedor`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_proveedor_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_proveedor_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'Lumbreras','74747478845','Av Lima 141','','Juan Vargas',1,'2022-04-04 10:59:05','2022-04-04 10:59:05',1,5,2,2),(2,'Libun','98747474325','Av Lima 874','','Luis Quispe',1,'2022-04-04 10:59:07','2022-04-04 10:59:07',2,6,2,2);
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserva_sala`
--

DROP TABLE IF EXISTS `reserva_sala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reserva_sala` (
  `idReservaSala` int NOT NULL AUTO_INCREMENT,
  `horaInicio` time DEFAULT NULL,
  `horaFin` time DEFAULT NULL,
  `fechaReserva` date DEFAULT NULL,
  `fechaRegistro` datetime DEFAULT NULL,
  `estado` smallint DEFAULT NULL,
  `idAlumno` int NOT NULL,
  `idSala` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  PRIMARY KEY (`idReservaSala`),
  KEY `fk_separacion_alumno1_idx` (`idAlumno`),
  KEY `fk_separacion_sala1_idx` (`idSala`),
  KEY `fk_separacion_usuario1_idx` (`idUsuarioRegistro`),
  CONSTRAINT `fk_separacion_alumno1` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `fk_separacion_sala1` FOREIGN KEY (`idSala`) REFERENCES `sala` (`idSala`),
  CONSTRAINT `fk_separacion_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva_sala`
--

LOCK TABLES `reserva_sala` WRITE;
/*!40000 ALTER TABLE `reserva_sala` DISABLE KEYS */;
INSERT INTO `reserva_sala` VALUES (1,'10:00:00','14:00:00','2023-09-13','2023-09-13 11:59:07',1,1,1,2);
/*!40000 ALTER TABLE `reserva_sala` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revista`
--

DROP TABLE IF EXISTS `revista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `revista` (
  `idRevista` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `frecuencia` varchar(200) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idPais` int NOT NULL,
  `idTipoRevista` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idRevista`),
  KEY `fk_revista_pais_idx` (`idPais`),
  KEY `fk_revista_tipo_idx` (`idTipoRevista`),
  KEY `fk_revista_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_revista_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_revista_pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`),
  CONSTRAINT `fk_revista_tipo` FOREIGN KEY (`idTipoRevista`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_revista_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_revista_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revista`
--

LOCK TABLES `revista` WRITE;
/*!40000 ALTER TABLE `revista` DISABLE KEYS */;
INSERT INTO `revista` VALUES (1,'Gente','Semanal Todos los miércoles','2021-05-06','2022-04-04 11:59:07','2022-04-04 11:59:07',1,3,15,2,2),(3,'Etiqueta Negra','Semanal Todos los lunes','2021-05-06','2022-04-04 11:59:07','2022-04-04 11:59:07',1,5,16,2,2);
/*!40000 ALTER TABLE `revista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `idRol` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Administrador General','1'),(2,'Administrador','1'),(3,'Bibliotecólogo','1');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_has_opcion`
--

DROP TABLE IF EXISTS `rol_has_opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol_has_opcion` (
  `idrol` int NOT NULL,
  `idopcion` int NOT NULL,
  PRIMARY KEY (`idrol`,`idopcion`),
  KEY `fk_rol_has_opcion_opcion1_idx` (`idopcion`),
  KEY `fk_rol_has_opcion_rol1_idx` (`idrol`),
  CONSTRAINT `fk_rol_has_opcion_opcion1` FOREIGN KEY (`idopcion`) REFERENCES `opcion` (`idOpcion`),
  CONSTRAINT `fk_rol_has_opcion_rol1` FOREIGN KEY (`idrol`) REFERENCES `rol` (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_has_opcion`
--

LOCK TABLES `rol_has_opcion` WRITE;
/*!40000 ALTER TABLE `rol_has_opcion` DISABLE KEYS */;
INSERT INTO `rol_has_opcion` VALUES (1,1),(2,1),(1,2),(2,2),(1,3),(2,3),(1,4),(2,4),(1,5),(2,5),(1,6),(2,6),(1,7),(2,7),(1,8),(2,8),(1,9),(2,9),(1,10),(2,10),(1,11),(2,11),(1,12),(2,12),(1,13),(2,13),(1,14),(2,14),(1,15),(2,15),(1,16),(2,16),(1,17),(2,17),(1,18),(2,18),(1,19),(2,19),(1,20),(2,20),(1,21),(2,21),(1,22),(2,22),(1,25),(2,25),(1,26),(2,26),(1,27),(2,27),(1,28),(3,28),(1,29),(3,29),(1,30),(3,30),(1,31),(3,31),(1,32),(3,32),(1,33),(3,33),(1,34),(3,34),(1,35),(3,35);
/*!40000 ALTER TABLE `rol_has_opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sala`
--

DROP TABLE IF EXISTS `sala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sala` (
  `idSala` int NOT NULL AUTO_INCREMENT,
  `numero` varchar(45) NOT NULL,
  `piso` int NOT NULL,
  `numAlumnos` int NOT NULL,
  `recursos` text NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idTipoSala` int NOT NULL,
  `idSede` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idSala`),
  KEY `fk_sala_tipo_idx` (`idTipoSala`),
  KEY `fk_sala_sede_idx` (`idSede`),
  KEY `fk_sala_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_sala_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_sala_sede` FOREIGN KEY (`idSede`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_sala_tipo` FOREIGN KEY (`idTipoSala`) REFERENCES `data_catalogo` (`idDataCatalogo`),
  CONSTRAINT `fk_sala_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_sala_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sala`
--

LOCK TABLES `sala` WRITE;
/*!40000 ALTER TABLE `sala` DISABLE KEYS */;
INSERT INTO `sala` VALUES (1,'A001',1,10,'tablet plumones','2023-04-04 11:59:07','2023-04-04 11:59:07',1,17,20,2,2),(2,'A002',1,3,'tablet plumones','2023-04-04 11:59:07','2023-04-04 11:59:07',1,17,21,2,2);
/*!40000 ALTER TABLE `sala` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tesis`
--

DROP TABLE IF EXISTS `tesis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tesis` (
  `idTesis` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) NOT NULL,
  `tema` varchar(200) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaActualizacion` datetime NOT NULL,
  `estado` smallint NOT NULL,
  `idAlumno` int NOT NULL,
  `idUsuarioRegistro` int NOT NULL,
  `idUsuarioActualiza` int NOT NULL,
  PRIMARY KEY (`idTesis`),
  KEY `fk_tesis_alumno_idx` (`idAlumno`),
  KEY `fk_tesis_usuario_idx` (`idUsuarioRegistro`),
  KEY `fk_tesis_usuario2_idx` (`idUsuarioActualiza`),
  CONSTRAINT `fk_tesis_alumno` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `fk_tesis_usuario1` FOREIGN KEY (`idUsuarioRegistro`) REFERENCES `usuario` (`idUsuario`),
  CONSTRAINT `fk_tesis_usuario2` FOREIGN KEY (`idUsuarioActualiza`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tesis`
--

LOCK TABLES `tesis` WRITE;
/*!40000 ALTER TABLE `tesis` DISABLE KEYS */;
INSERT INTO `tesis` VALUES (1,'Sistema de Redes neuronales aplicado a los colegios','Sistemas','2021-05-10','2022-04-04 10:59:04','2022-04-04 10:59:04',1,1,2,2),(2,'Sistema de inteligencia artificial aplicado al gobierno','Sistemas','2021-05-10','2022-04-04 10:59:04','2022-04-04 10:59:04',1,2,2,2);
/*!40000 ALTER TABLE `tesis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `idUsuario` int NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(200) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `direccion` text NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Luis Alberto','Sanchez Quispe ','74747474','luis','$2a$10$Z7/zwEFEV3L14ghsKapFj.tZKLiBvqCR84PwN9jG/bZA8ePDIUqru','luis@gmail.com','2022-04-04 10:59:07','2000-10-10','Av Lima 123'),(2,'Ana Elba','Flores Enero','87485965','ana','$2a$10$LKANug7Towq30DMm/GsCNOqJ6ybfWjqC/8FKZX78Fp3r4ZPvVfHSO','ana@gmail.com','2022-04-04 10:59:07','1997-10-10','Av Lince 787'),(3,'Juana','Arcos Gutierrez','85747478','juana','$2a$10$KNzO4mDKYpKnSmkvPTUzM.zos8iga4l5VTZX81R6tbru4j2aUajCu','juana@gmail.com','2022-04-04 10:59:07','1997-10-10','Av Mangomarca 987');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_has_rol`
--

DROP TABLE IF EXISTS `usuario_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario_has_rol` (
  `idUsuario` int NOT NULL,
  `idRol` int NOT NULL,
  PRIMARY KEY (`idUsuario`,`idRol`),
  KEY `fk_usuario_has_rol_rol1_idx` (`idRol`),
  KEY `fk_usuario_has_rol_usuario1_idx` (`idUsuario`),
  CONSTRAINT `fk_usuario_has_rol_rol1` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`),
  CONSTRAINT `fk_usuario_has_rol_usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_has_rol`
--

LOCK TABLES `usuario_has_rol` WRITE;
/*!40000 ALTER TABLE `usuario_has_rol` DISABLE KEYS */;
INSERT INTO `usuario_has_rol` VALUES (1,1),(2,2),(3,3);
/*!40000 ALTER TABLE `usuario_has_rol` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-14  0:21:54
