import { Pais } from "./pais.model";
import { Usuario } from "./usuario.model";

export class Editorial {

    idEditorial?: number;
    razonSocial?:string;
    direccion?:string;
    ruc?:string;
    estado?:number;
    fechaRegistro?:string;
    fechaCreacion?:string;
    pais?:Pais;
    usuarioRegistro?:Usuario;
    usuarioActualiza?:Usuario;

}
