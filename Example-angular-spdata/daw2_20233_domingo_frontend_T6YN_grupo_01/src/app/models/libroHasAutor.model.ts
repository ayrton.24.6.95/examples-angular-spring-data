import { Autor } from "./autor.model";
import { Libro } from "./libro.model";

export class LibroHasAutor {

  libroHasAutorPK: LibroHasAutorPK;

  autor?: Autor;
  libro?: Libro;

  constructor() {
    this.libroHasAutorPK = { idLibro: 0, idAutor: 0 }; // o proporciona valores iniciales adecuados
  }
}

export interface LibroHasAutorPK {
  idLibro: number;
  idAutor: number;
}
