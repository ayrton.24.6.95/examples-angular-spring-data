import { Injectable } from '@angular/core';
import { AppSettings } from '../app.settings';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import { Alumno } from '../models/alumno.model';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";


const baseUrlAlumno = AppSettings.API_ENDPOINT+ '/alumno';
const baseUrlConsulta = AppSettings.API_ENDPOINT+ '/consultaAlumno';

@Injectable({
  providedIn: 'root'
})

export class AlumnoService {

  constructor(private http:HttpClient) { }

  registraAlumno(obj:Alumno): Observable<any>{
    return this.http.post(baseUrlAlumno, obj);
  }

  consultaPorNombre(filtro:string):Observable<Alumno[]>{
    return  this.http.get<Alumno[]>(baseUrlAlumno +"/listaAlumnoPorNombreLike/"+filtro); 
}  

inserta(obj:Alumno):Observable<any>{
    return this.http.post(baseUrlAlumno +"/registraAlumno", obj);
}

actualiza(obj:Alumno):Observable<any>{
    return this.http.put(baseUrlAlumno + "/actualizaAlumno", obj);
}

elimina(idAlumno:number):Observable<any>{
    return this.http.delete(baseUrlAlumno + "/eliminaAlumno/"+ idAlumno);
}

//consulta
consulta(
  nombres:string,
  apellidos:string,
  telefono:string,
  dni:string,
  correo:string,
  estado:number,
  idPais:number,
  idModalidad:number
  ):Observable<Alumno[]>{

    const params = new HttpParams()
    .set("nombres", nombres)
    .set("apellidos", apellidos)
    .set("telefono", telefono)
    .set("dni", dni)
    .set("correo", correo)
    .set("estado", estado)
    .set("idPais", idPais)
    .set("idModalidad", idModalidad);
  return this.http.get<Alumno[]>(baseUrlConsulta + "/porParametros", {params});
}

generateDocumentReport(
  nombres:string,
  apellidos:string,
  telefono:string,
  dni:string,
  correo:string,
  estado:number,
  idPais:number,
  idModalidad:number
  ): Observable<any> {

  const params = new HttpParams()
    .set("nombres", nombres)
    .set("apellidos", apellidos)
    .set("telefono", telefono)
    .set("dni", dni)
    .set("correo", correo)
    .set("estado", estado)
    .set("idPais", idPais)
    .set("idModalidad", idModalidad);
  let headers = new HttpHeaders();
  headers.append('Accept', 'application/pdf');
  let requestOptions: any = { headers: headers, responseType: 'blob' };

  return this.http.post(baseUrlConsulta + "/reporteAlumnoPdf?nombres=" + nombres + "&apellidos=" + 
  apellidos +"&telefono=" + telefono +"&dni=" + dni + "&correo=" + correo +"&estado=" + estado + 
  "&idPais=" + idPais + "&idModalidad=" + idModalidad, '', requestOptions).pipe(map((response) => {
    return {
      filename: 'reporteAlumnoCl3.pdf',
      data: new Blob([response], { type: 'application/pdf' })
    };
  }));
}
}
