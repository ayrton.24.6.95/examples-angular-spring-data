import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { AppSettings } from '../app.settings';
import { Sala } from '../models/sala.model';

const baseUrlSala = AppSettings.API_ENDPOINT+ '/sala';
const baseUrlCrudSala = AppSettings.API_ENDPOINT+'/crudSala'
const baseConsulta = AppSettings.API_ENDPOINT+'/consultaSala'

@Injectable({
  providedIn: 'root'
})
export class SalaService {

  constructor(private http:HttpClient) { }

  registrar(data:Sala):Observable<any>{
    return this.http.post(baseUrlSala, data);
  }

  consultarPorNumero(filtro:string):Observable<Sala[]>{
    return this.http.get<Sala[]>(baseUrlCrudSala+"/listaSalaporNumeroLike/"+filtro);
  }

  inserta(obj:Sala):Observable<any>{
    return this.http.post(baseUrlCrudSala + "/registrarSala", obj);
  }

  actualiza(obj:Sala):Observable<any>{
    return this.http.put(baseUrlCrudSala + "/actualizaSala", obj);
  }

  elimina(idSala:number):Observable<any>{
    return this.http.delete(baseUrlCrudSala + "/eliminaSala/"+ idSala);
  }

  consulta(
    numero:string,
    recursos:string,
    idTipoSala:number,
    idSede:number,
    estado:number
  ):Observable<Sala[]>{
    const params = new HttpParams()
    .set("numero", numero)
    .set("recursos", recursos)
    .set("idTipoSala", idTipoSala)
    .set("idSede", idSede)
    .set("estado", estado);
    return this.http.get<Sala[]>(baseConsulta + "/consultaPorParametros",{params})
  }

  generateDocumentReport(
    numero:string,
    recursos:string,
    idTipoSala:number,
    idSede:number,
    estado:number
  ):Observable<any>{
    const params = new HttpParams()
    .set("numero", numero)
    .set("recursos", recursos)
    .set("idTipoSala", idTipoSala)
    .set("idSede", idSede)
    .set("estado", estado);
    let headers = new HttpHeaders();
    headers.append('Accept', 'application/pdf');
    let requestOptions: any = { headers: headers, responseType: 'blob' };
    return this.http.post(baseConsulta +"/reporteSalaPdf?&numero="+numero+"&recursos="+recursos+
      "&idTipoSala="+idTipoSala+"&idSede="+idSede+"&estado="+estado,'', requestOptions).pipe(map((response)=>{
        return {
          filename: 'reporteSala.pdf',
          data: new Blob([response], { type: 'application/pdf' })
        };
      }));
  }
}
