import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from '../app.settings';
import { Revista } from '../models/revista.model';
import { map } from "rxjs/operators";

const baseUrl = AppSettings.API_ENDPOINT + '/revista';
const baseUrlConsulta = AppSettings.API_ENDPOINT + "/consultaRevista";

@Injectable({
  providedIn: 'root'
})
export class RevistaService {

  constructor(private http: HttpClient) { }

  consultaPorNombre(filtro: string): Observable<Revista[]> {
    return this.http.get<Revista[]>(baseUrl + "/listaRevistaPorNombreLike/" + filtro);
  }

  inserta(obj: Revista): Observable<any> {
    return this.http.post(baseUrl + "/registraRevista", obj);
  }

  // para la PC2
  actualiza(obj: Revista): Observable<any> {
    return this.http.put(baseUrl + "/actualizaRevista", obj);
  }
  elimina(idRevista: number): Observable<any> {
    return this.http.delete(baseUrl + "/eliminaRevista/" + idRevista);
  }

  //consulta
  consulta(
    nombre: string,
    frecuencia: string,
    estado: number,
    pais: number,
    revista: number
  ): Observable<Revista[]> {
    const params = new HttpParams()
      .set("nombre", nombre)
      .set("frecuencia", frecuencia)
      .set("estado", estado)
      .set("idpais", pais)
      .set("tipoRevista", revista);

    return this.http.get<Revista[]>(baseUrlConsulta + "/consultaRevistaPorParametros", { params });
  }

  generateDocumentReport
    (
      nombre: string,
      frecuencia: string,
      estado: number,
      pais: number,
      revista: number ): Observable<any> {

    const params = new HttpParams()
      .set("nombre", nombre)
      .set("frecuencia", frecuencia)
      .set("estado", estado)
      .set("idpais", pais)
      .set("tipoRevista", revista);

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/pdf');
    let requestOptions: any = { headers: headers, responseType: 'blob' };

    return this.http.post(baseUrlConsulta + "/reporteRevistaPdf?nombre=" + nombre + "&frecuencia=" + frecuencia + "&estado=" + estado + "&idpais=" + pais + "&tipoRevista=" + revista, '', requestOptions).pipe(map((response) => {
      return {
        filename: 'reporteRevista20232.pdf',
        data: new Blob([response], { type: 'application/pdf' })
      };
    }));
  }



}