import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from '../app.settings';
import { Autor } from '../models/autor.model';
// para .pipe(map((response) =>
import { map } from "rxjs/operators";

//pc1 y pc2
const baseUrlAutor = AppSettings.API_ENDPOINT + '/autor';
//pc3
const baseUrlConsulta = AppSettings.API_ENDPOINT + "/consultaAutor";

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  constructor(private http: HttpClient) { }

  // para la PC1
  resgistarAutor(data: Autor): Observable<any> {
    return this.http.post(baseUrlAutor, data);
  }

  // para la PC2
  consultaPorNombre(filtro: string): Observable<Autor[]> {
    return this.http.get<Autor[]>(baseUrlAutor + "/listaAutorPorNombreLike/" + filtro);
  }

  // para la PC2
  inserta(obj: Autor): Observable<any> {
    return this.http.post(baseUrlAutor + "/registraAutor", obj);
  }

  // para la PC2
  actualiza(obj: Autor): Observable<any> {
    return this.http.put(baseUrlAutor + "/actualizaAutor", obj);
  }
  elimina(idAutor: number): Observable<any> {
    return this.http.delete(baseUrlAutor + "/eliminaAutor/" + idAutor);
  }

  //consulta
  consulta(
    nombres: string,
    apellidos: string,
    telefono: string,
    estado: number,
    idPais: number,
    idGrado: number
  ): Observable<Autor[]> {
    //llamr los parametros con los mismos del backend 
    const params = new HttpParams()
      .set("nombres", nombres)
      .set("apellidos", apellidos)
      .set("telefono", telefono)
      .set("estado", estado)
      .set("pais", idPais)
      .set("elGrado", idGrado);

    return this.http.get<Autor[]>(baseUrlConsulta + "/consultaAutorPorParametros", { params });
  } // fin de consulta

  generateDocumentReport(
    nombres: string,
    apellidos: string,
    telefono: string,
    estado: number,
    idPais: number,
    idGrado: number
  ): Observable<any> {

    const params = new HttpParams()
      .set("nombres", nombres)
      .set("apellidos", apellidos)
      .set("telefono", telefono)
      .set("estado", estado)
      .set("pais", idPais)
      .set("elGrado", idGrado);

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/pdf');
    let requestOptions: any = { headers: headers, responseType: 'blob' };

    return this.http.post(baseUrlConsulta + "/reporteAutorPdf?nombres=" + nombres + "&apellidos=" + apellidos + "&telefono=" + telefono + "&pais=" + idPais +"&elGrado=" + idGrado,'', requestOptions).pipe(map((response) => {
      return {                               
        filename: 'reporteAutor2023.pdf',
        data: new Blob([response], { type: 'application/pdf' })
      };
    }));
  }



} // fin de class AutorService
