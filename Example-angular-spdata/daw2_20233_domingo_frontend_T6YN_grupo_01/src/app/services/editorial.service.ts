import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { AppSettings } from '../app.settings';
import { Ejemplo } from '../models/ejemplo.model';
import { Editorial } from '../models/editorial.model';

const baseUrl = AppSettings.API_DOMAIN + '/url/editorial';

const baseUrlConsulta = AppSettings.API_DOMAIN + '/url/consultaEditorial';

@Injectable({
  providedIn: 'root',
})
export class EditorialService {
  constructor(private http: HttpClient) {}

  registrar(data: Ejemplo): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  listar(): Observable<Editorial[]> {
    return this.http.get<Editorial[]>(baseUrl);
  }

  actualizar(data: Editorial): Observable<Editorial> {
    return this.http.put<Editorial>(baseUrl, data);
  }

  eliminar(id: number): Observable<any> {
    const url = `${baseUrl}/${id}`;
    return this.http.delete(url);
  }

  busquedaPorParametro(
    razon: string,
    direccion: string,
    ruc: string,
    estado: number,
    idPais: number
  ): Observable<Editorial[]> {
    const params = new HttpParams()
      .set('razon', razon)
      .set('direccion', direccion)
      .set('ruc', ruc)
      .set('estado', estado)
      .set('idPais', idPais);
    return this.http.get<Editorial[]>(
      baseUrlConsulta + '/consultaEditorialPorParametros',
      { params }
    );
  }

  generateDocumentReport(
    razon: string,
    direccion: string,
    ruc: string,
    estado: number,
    idPais: number
  ): Observable<any> {
    const params = new HttpParams()
      .set('razon', razon)
      .set('direccion', direccion)
      .set('ruc', ruc)
      .set('estado', estado)
      .set('idPais', idPais);

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/pdf');
    let requestOptions: any = { headers: headers, responseType: 'blob' };

    return this.http
      .post(
        baseUrlConsulta +
          '/reporteEditorialPdf?razon=' +
          razon +
          '&direccion=' +
          direccion +
          '&ruc=' +
          ruc +
          '&estado=' +
          estado +
          '&idPais=' +
          idPais,
        '',
        requestOptions
      )
      .pipe(
        map((response) => {
          return {
            filename: 'Reporte.pdf',
            data: new Blob([response], { type: 'application/pdf' }),
          };
        })
      );
  }
}
