import { Injectable } from '@angular/core';
import { AppSettings } from '../app.settings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Libro } from '../models/libro.model';
import { map } from "rxjs/operators"
import { LibroHasAutor } from '../models/libroHasAutor.model';

const baseUrlLibro = AppSettings.API_ENDPOINT + '/libro';
const baseUrlCrudLibro = AppSettings.API_ENDPOINT + '/crudLibro';
const baseUrlConsultaLibro = AppSettings.API_ENDPOINT + '/consultaLibro';
const baseUrlLibroHasAutor = AppSettings.API_ENDPOINT + '/libro-has-autor';

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  constructor(private http: HttpClient) { }

  registrar(data: Libro): Observable<any> {
    return this.http.post(baseUrlLibro, data)
  }

  consultarPorTitulo(filtro: string): Observable<Libro[]> {
    return this.http.get<Libro[]>(baseUrlCrudLibro +
      "/listaLibroPorTituloLike/" + filtro);
  }

  //crud
  inserta(obj: Libro): Observable<any> {
    return this.http.post(baseUrlCrudLibro + "/registrarLibro", obj);
  }

  actualiza(obj: Libro): Observable<any> {
    return this.http.put(baseUrlCrudLibro + "/actualizaLibro", obj);
  }

  elimina(idLibro: number): Observable<any> {
    return this.http.delete(baseUrlCrudLibro + "/eliminaLibro/" + idLibro);
  }

  //consulta
  consulta(titulo: string,
    anio: number,
    serie: string,
    estado: number,
    categoriaLibro: number,
    tipoLibro: number,
  ): Observable<Libro[]> {

    const params = new HttpParams()
      .set("titulo", titulo)
      .set("anio", anio)
      .set("serie", serie)
      .set("estado", estado)
      .set("categoriaLibro", categoriaLibro)
      .set("tipoLibro", tipoLibro)


    return this.http.get<Libro[]>(baseUrlConsultaLibro + "/consultaLibroPorParametros", { params });
  }

  //pdf

  generateDocumentReport(titulo: string,
    anio: number,
    serie: string,
    estado: number,
    categoriaLibro: number,
    tipoLibro: number,
  ): Observable<any> {

    const params = new HttpParams()
      .set("titulo", titulo)
      .set("anio", anio)
      .set("serie", serie)
      .set("estado", estado)
      .set("categoriaLibro", categoriaLibro)
      .set("tipoLibro", tipoLibro);



    let headers = new HttpHeaders();
    headers.append('Accept', 'application/pdf');
    let requestOptions: any = { headers: headers, responseType: 'blob' };


    return this.http.post(baseUrlConsultaLibro +"/reporteLibroPdf?titulo="+titulo+"&anio="+anio+"&serie="+serie+"&estado="+estado+"&categoriaLibro="+categoriaLibro+"&tipoLibro="+tipoLibro,'',requestOptions).pipe(map((response)=>{
      return {
        filename: 'reporteLibro2023.pdf',
        data: new Blob([response], {type: 'application/pdf'})
      };
    }));
  }

  listarLibros(): Observable<Libro[]> {
    return this.http.get<Libro[]>(baseUrlLibro);
  }

  listarLibrosHasAutor(): Observable<any> {
    return this.http.get(baseUrlLibroHasAutor);
  }

  guardarLibroHasAutor(
    libroHasAutor: LibroHasAutor
  ): Observable<LibroHasAutor> {
    return this.http.post<LibroHasAutor>(baseUrlLibroHasAutor, libroHasAutor);
  }

  eliminarLibroHasAutor(libroHasAutor: LibroHasAutor): Observable<any> {
    const url = `${baseUrlLibroHasAutor}/${libroHasAutor.libroHasAutorPK.idLibro}/${libroHasAutor.libroHasAutorPK.idAutor}`;
    return this.http.delete(url);
  }

  consultarPorParametros(idLibro: number, idAutor: number): Observable<LibroHasAutor> {
    const url = `${baseUrlLibroHasAutor}/${idLibro}/${idAutor}`;
    return this.http.get<LibroHasAutor>(url);
  }



}
