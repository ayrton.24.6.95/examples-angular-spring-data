import { Injectable } from '@angular/core';
import { AppSettings } from '../app.settings';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Proveedor } from '../models/proveedor.model';
import { DataCatalogo } from '../models/dataCatalogo.model';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

const baseUrlProveedor = AppSettings.API_ENDPOINT + '/proveedor';
const baseUrlCrudProveedor = AppSettings.API_ENDPOINT + '/crudproveedor';
const baseUrlConsulta = AppSettings.API_ENDPOINT + '/consultaProveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  constructor(private http: HttpClient) { }

  registrarProveedor(data: Proveedor): Observable<any> {
    return this.http.post(baseUrlProveedor, data);
  }

  //filtro
  consultaPorNombre(filtro: string): Observable<Proveedor[]> {
    return this.http.get<Proveedor[]>(baseUrlCrudProveedor + "/listaProveedorPorNombreLike/" + filtro);
  }

  //CRUD
  inserta(obj: Proveedor): Observable<any> {
    return this.http.post(baseUrlCrudProveedor + "/registraProveedor", obj);
  }

  actualiza(obj: Proveedor): Observable<any> {
    return this.http.put(baseUrlCrudProveedor + "/actualizaProveedor", obj);
  }

  elimina(idProveedor: number): Observable<any> {
    return this.http.delete(baseUrlCrudProveedor + "/eliminaProveedor/" + idProveedor);
  }

  //consulta
  consultaParametro(razonsocial: string,
    ruc: string,
    estado: number,
    idPais: number,
    idDataCatalogo: number): Observable<Proveedor[]> {

    const params = new HttpParams()
      .set('razonsocial', razonsocial)
      .set('ruc', ruc)
      .set('estado', estado)
      .set('idPais', idPais)
      .set('idTipoProveedor', idDataCatalogo);
    return this.http.get<Proveedor[]>(
      baseUrlConsulta + '/consultaProveedorPorParametros',
      { params });
  }

  generarDocumentReport(razonsocial:string,
    ruc:string,
    estado:number,
    idPais:number,
    idDataCatalogo:number):Observable<any> {

      const params = new HttpParams()
      .set("razonsocial", razonsocial)
      .set("ruc", ruc)
      .set("estado", estado)
      .set("idTipoProveedor", idDataCatalogo);

      let headers = new HttpHeaders();
      headers.append('Accept', 'application/pdf');
      let requestOptions: any = {headers: headers, responseType: 'blob'};

      return this.http.post(baseUrlConsulta +"/reporteProveedorPdf?razonsocial="+razonsocial+"&ruc="+ruc+"&estado="+estado+"&idPais="+idPais+"&idDataCatalogo="+idDataCatalogo,'',requestOptions).pipe(map((response)=>{
          return {
            filename: 'reporteProveedor20232.pdf',
            data: new Blob([response], {type: 'application/pdf'})
          };
      }));
    }

}
