import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Alumno } from 'src/app/models/alumno.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { UtilService } from 'src/app/services/util.service';
import { CrudAlumnoAddComponent } from '../crud-alumno-add/crud-alumno-add.component';
import { CrudAlumnoUpdateComponent } from '../crud-alumno-update/crud-alumno-update.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-alumno',
  templateUrl: './crud-alumno.component.html',
  styleUrls: ['./crud-alumno.component.css']
})
export class CrudAlumnoComponent implements OnInit {

  objUsuario: Usuario = {};

      //Para la Grilla
      filtro: string = "";

      //Para el pais y modaliad
        pais : Pais[] = [];
        modalidad : DataCatalogo[] = [];
    
      //Grilla
      dataSource: any;

      @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
      //displayedColumns = ["idDocente", "nombre", "dni", "fecha", "hora", "ubigeo", "estado", "acciones"];
      displayedColumns = ["idAlumno", "nombres", "apellidos", "dni", "correo", "pais", "modalidad", "estado", "acciones"];

      
  constructor(private formBuilder: FormBuilder,
    private dialogService: MatDialog,
    private alumnoService: AlumnoService,
    private utilService:  UtilService,
    private tokenService:TokenService) {
      this.utilService.listaPais().subscribe(
        data=>this.pais = data      
      );
  
      this.utilService.listaModalidadAlumno().subscribe(
        mod=> this.modalidad = mod    
      ); 
      this.objUsuario.idUsuario = tokenService.getUserId();
     }

  ngOnInit(): void {
  }

  consultaAlumno() {
    console.log(">>> consultaAlumno>>> " + this.filtro);
    this.refreshTable();
  }

  openAddDialog() {//para registrar
    console.log(">>> openAddDialog  >>");
    const dialogRef = this.dialogService.open(CrudAlumnoAddComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1) {
        this.refreshTable();//refrescar la data de la grilla o lista
      }
    });
  }

  openUpdateDialog(obj: Alumno) {
    console.log(">>> openUpdateDialog  >>");
    const dialogRef = this.dialogService.open(CrudAlumnoUpdateComponent, { data: obj });
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  actualizaEstado(obj: Alumno) {//para que se pueda poner activo o inactivo con un boton
    obj.estado = obj.estado == 1 ? 0 : 1;
    this.alumnoService.actualiza(obj).subscribe();
  }

  elimina(obj:Alumno){
    Swal.fire({
      title: '¿Desea eliminar?',
      text: "Los cambios no se van a revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, elimina',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
          if (result.isConfirmed) {
              this.alumnoService.elimina(obj.idAlumno|| 0).subscribe(
                    x => {
                          this.refreshTable();
                          Swal.fire('Mensaje', x.mensaje, 'info');
                    }
              );
          }
    })   
  }

  private refreshTable() {
    this.alumnoService.consultaPorNombre(this.filtro == "" ? "todos" : this.filtro).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Alumno>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

}
