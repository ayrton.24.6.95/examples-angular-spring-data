import { Component,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Editorial } from 'src/app/models/editorial.model';
import { Pais } from 'src/app/models/pais.model';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-crud-editorial-update',
  templateUrl: './crud-editorial-update.component.html',
  styleUrls: ['./crud-editorial-update.component.css'],
})
export class CrudEditorialUpdateComponent {
  
  lstPais: Pais[] = [];
  editorial: Editorial= {};

  listEstado = [
    {
      id: 0,
      detalle: 'Inactivo',
    },
    {
      id: 1,
      detalle: 'Activo',
    },
  ];

  editorialForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CrudEditorialUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Editorial,
    private utilService: UtilService,
    private fb: FormBuilder
  ) {
    
    utilService.listaPais().subscribe((x) => {
      this.lstPais = x
      
    });
    this.editorial=data || {};

    if (this.editorial.pais) {
      this.editorial.pais.idPais = this.editorial.pais.idPais || -1;
    }

    this.editorialForm = this.fb.group({ 
      validaRazonSocial: [this.editorial.razonSocial, [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
      validaRUC: [this.editorial.ruc, [Validators.required, Validators.pattern('[0-9]{10}')] ] , 
      validaDireccion: [this.editorial.direccion, Validators.required] , 
      validaFechaCrecion:[this.editorial.fechaCreacion, Validators.required],
      validaPais:[this.editorial.pais?.idPais,Validators.required]
    });

    this.editorialForm.statusChanges.subscribe(status => {
      console.log("status Validacion:",status);
      
    });

  }

  onSave(): void {
    if (this.editorialForm.valid) {
      this.editorial.razonSocial=this.editorialForm.get("validaRazonSocial")?.value;
      this.editorial.direccion=this.editorialForm.get("validaDireccion")?.value;
      this.editorial.ruc=this.editorialForm.get("validaRUC")?.value;
      this.editorial.fechaCreacion=this.editorialForm.get("validaFechaCrecion")?.value;
      if (!this.editorial.pais) {
        this.editorial.pais = {};
      }
      const paisEncontrado = this.lstPais.find(pais => pais.idPais === (Number(this.editorialForm.get("validaPais")?.value)));
      this.editorial.pais= paisEncontrado;
      this.dialogRef.close({...this.editorial});
    }
  }

  onCancel(): void {
    this.editorial={}
    this.dialogRef.close({...this.editorial});
  }
}
