import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Revista } from 'src/app/models/revista.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { RevistaService } from 'src/app/services/revista.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-consulta-revista',
  templateUrl: './consulta-revista.component.html',
  styleUrls: ['./consulta-revista.component.css']
})
export class ConsultaRevistaComponent {
  //Grila
  dataSource: any;

  //Clase para la paginacion
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //Cabecera
  displayedColumns = ["idRevista", "nombre", "frecuencia", "fechaCreacion", "fechaRegistro", "hora", "estado", "pais","tipoRevista"];

  // CREAR PARAMETROS
  nombreRev: string = "";
  frecuencia: string = "";  
  estado: boolean = true;
  selPais: number = -1;
  selRevista: number = -1;

  lstPais: Pais[] = [];
  lstRevista: DataCatalogo[] = [];

  objUsuario: Usuario = {};

  constructor(private revistaService: RevistaService,
      private utilService: UtilService,
      private tokenService: TokenService) {
    this.utilService.listaPais().subscribe(
      x => this.lstPais = x
    )
    this.utilService.listaTipoLibroRevista().subscribe(
      y => this.lstRevista = y
    )
    this.objUsuario.idUsuario = tokenService.getUserId();
  }

  consulta(){
    console.log(">> nombre >> " + this.nombreRev) ;
    console.log(">> frecuencia >> " + this.frecuencia) ;
    console.log(">> estado >> " + this.estado) ;
    console.log(">> selPais >> " + this.selPais) ;
    console.log(">> selRevista >> " + this.selRevista) ;

    this.revistaService.consulta(this.nombreRev, this.frecuencia, this.estado?1:0, this.selPais, this.selRevista).subscribe(
           x => {
                 this.dataSource = new MatTableDataSource<Revista>(x);
                 this.dataSource.paginator = this.paginator;
           }
    );
  }

  exportarPDF() {

    this.revistaService.generateDocumentReport(this.nombreRev, this.frecuencia, this.estado?1:0, this.selPais, this.selRevista).subscribe(
          response => {
            console.log(response);
            var url = window.URL.createObjectURL(response.data);
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.setAttribute('target', 'blank');
            a.href = url;
            a.download = response.filename;
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove();
        }); 
  }




  
} // class ConsultaRevistaComponent
