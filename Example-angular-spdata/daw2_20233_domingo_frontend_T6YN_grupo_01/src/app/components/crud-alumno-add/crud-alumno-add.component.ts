import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Alumno } from 'src/app/models/alumno.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-alumno-add',
  templateUrl: './crud-alumno-add.component.html',
  styleUrls: ['./crud-alumno-add.component.css']
})
export class CrudAlumnoAddComponent {

  pais : Pais[] = []
  modalidad : DataCatalogo[] = []

  //declarar las validaciones
  formsRegistra = this.formBuilder.group({
    validaNombres: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaApellidos: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaTelefono: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    validaDni: ['', [Validators.required, Validators.pattern('[0-9]{8}')]],
    validaCorreo: ['', [Validators.required, Validators.email]],
    //validaNacimiento: ['', [Validators.required, this.fechaValidaValidator]],
    validaPais: ['', Validators.min(1)],
    validaModalidad: ['', Validators.min(1)],
  })

  /*fechaValidaValidator(control) {
    const inputDate = new Date(control.value);
    const currentDate = new Date();

    if (isNaN(inputDate.getTime()) || inputDate < currentDate) {
      return { fechaInvalida: true };
    }

    return null;
  }*/

  alumno: Alumno = {
    idAlumno:0,
    nombres:"",
    apellidos:"",
    telefono:"",
    dni:"",
    correo:"",
    fechaNacimiento:new Date(),
    estado: 1,
    pais: {
      idPais: -1
    },
    modalidad: {
      idDataCatalogo: -1
    },  
};

objUsuario: Usuario = {};

constructor(private formBuilder: FormBuilder,
  private alumnoService: AlumnoService,
  private utilService:  UtilService,
  private tokenService:TokenService) {
    this.utilService.listaPais().subscribe(
      data=>this.pais = data      
    );
    this.utilService.listaModalidadAlumno().subscribe(
      mod=> this.modalidad = mod    
    ); 
    this.objUsuario.idUsuario = tokenService.getUserId();
   }

   registra(){
    this.alumno.usuarioActualiza = this.objUsuario;
    this.alumno.usuarioRegistro = this.objUsuario;
    this.alumnoService.inserta(this.alumno).subscribe(
      x=>{
        Swal.fire({
          icon: 'info',
          title: 'Resultado del Registro',
          text: x.mensaje,
        })
      },
    );
}


}
