import { Component, OnInit, ViewChild } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Proveedor } from 'src/app/models/proveedor.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-consulta-proveedor',
  templateUrl: './consulta-proveedor.component.html',
  styleUrls: ['./consulta-proveedor.component.css']
})
export class ConsultaProveedorComponent implements OnInit {
  objUsuario: Usuario = {};
  lstPais: Pais[] = [];
  lstTipoProveedor: DataCatalogo[] = [];

  //parámetros de consulta
  razonsocial: string = "";
  ruc: string = "";
  estado: boolean = true;
  selPais: number = -1;
  selTipoProveedor: number = -1;

  //grilla
  dataSource: any;

  //clase para la paginación
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //cabecera
  displayedColumns = ["idProveedor", "razonsocial", "ruc", "direccion", "celular", "contacto", "estado", "fechaRegistro", "pais", "tipoProveedor"];

  constructor(private utilService: UtilService,
    private dateAdapter: DateAdapter<Date>,
    private tokenService: TokenService,
    private proveedorService: ProveedorService) {
    this.utilService.listaPais().subscribe(
      x => this.lstPais = x
    );
    this.utilService.listaTipoProveedor().subscribe(
      a => this.lstTipoProveedor = a
    );
  }

  consultaParametro() {
    console.log(">> razonsocial >> " + this.razonsocial);
    console.log(">> ruc >> " + this.ruc);
    console.log(">> estado >> " + this.estado);
    console.log(">> selPais >> " + this.selPais);
    console.log(" >> selTipoProveedor >> " + this.selTipoProveedor);

    this.proveedorService.consultaParametro(
      this.razonsocial, this.ruc, this.estado ? 1 : 0, this.selPais, this.selTipoProveedor).subscribe(x => {
        this.dataSource = new MatTableDataSource<Proveedor>(x);
        this.dataSource.paginator = this.paginator;
      });
  }

  exportarPDF() {

    this.proveedorService.generarDocumentReport(this.razonsocial, this.ruc, this.estado ? 1 : 0, this.selPais, this.selTipoProveedor).subscribe(
          response => {
            console.log(response);
            var url = window.URL.createObjectURL(response.data);
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.setAttribute('target', 'blank');
            a.href = url;
            a.download = response.filename;
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove();
          });
  }

  ngOnInit(): void {
  }

}
