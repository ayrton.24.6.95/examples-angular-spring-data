import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Revista } from 'src/app/models/revista.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { RevistaService } from 'src/app/services/revista.service';
import { UtilService } from 'src/app/services/util.service';
import { CrudRevistaAddComponent } from '../crud-revista-add/crud-revista-add.component';
import { CrudRevistaUpdateComponent } from '../crud-revista-update/crud-revista-update.component';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-crud-revista',
  templateUrl: './crud-revista.component.html',
  styleUrls: ['./crud-revista.component.css']
})
export class CrudRevistaComponent {
  
  //Grilla
  dataSource: any;
  filtro: string = "";
  
  lstPais: Pais[] = [];
  lstRevista: DataCatalogo[] = [];

  objUsuario: Usuario = {};

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  displayedColumns = ["idRevista","nombre","frecuencia","fechaCreacion","fechaRegistro","estado","pais","tipoRevista", "acciones"];  

  constructor(    
    private dialogService: MatDialog,
    private revistaService: RevistaService,
    private utilService: UtilService,
    private tokenService: TokenService
  ) {
    this.utilService.listaPais().subscribe(
      x => this.lstPais = x
    )
    this.utilService.listaTipoLibroRevista().subscribe(
      y => this.lstRevista = y
    )
    this.objUsuario.idUsuario = tokenService.getUserId();

  }
 
  consultaRevista() {
    console.log(">>> consultaRevista >>> " + this.filtro);
    this.refreshTable();
  }

  openAddDialog() {
    console.log(">>> openAddDialog  >>");
    const dialogRef = this.dialogService.open(CrudRevistaAddComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  openUpdateDialog(obj: Revista) {
    console.log(">>> openUpdateDialog  >>");
    const dialogRef = this.dialogService.open(CrudRevistaUpdateComponent, { data: obj });
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  actualizaEstado(obj: Revista) {
    obj.estado = obj.estado == 1 ? 0 : 1;
    this.revistaService.actualiza(obj).subscribe();
  }

  elimina(obj: Revista) {    
    Swal.fire({
      title: '¿Desea eliminar?',
      text: "Los cambios no se van a revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, elimina',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.isConfirmed) {        
        this.revistaService.elimina(obj.idRevista || 0).subscribe(
          x => {
            this.refreshTable();
            Swal.fire('Mensaje', x.mensaje, 'info');
          }
        );       
      }
    })
  }

  private refreshTable() {
    this.revistaService.consultaPorNombre(this.filtro == "" ? "todos" : this.filtro).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Revista>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }


}

