import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Autor } from 'src/app/models/autor.model';
import { Libro } from 'src/app/models/libro.model';
import { LibroHasAutor } from 'src/app/models/libroHasAutor.model';
import { AutorService } from 'src/app/services/autor.service';
import { LibroService } from 'src/app/services/libro.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-transaccion-asignacion-libro',
  templateUrl: './transaccion-asignacion-libro.component.html',
  styleUrls: ['./transaccion-asignacion-libro.component.css'],
})
export class TransaccionAsignacionLibroComponent {
  lstLibro: Libro[] = [];
  lstAutor: Autor[] = [];
  lstlibroHasAutor: LibroHasAutor[] = [];
  lstlibroHasAutorFiltered: LibroHasAutor[] = [];

  idAutorSeleccionado: number | null | undefined = null;

  libroHasAutor: LibroHasAutor = new LibroHasAutor(); // Inicializa con el constructor de la clase
  form: FormGroup; // Define el formulario reactivo

  //Grila
  dataSource: any;

  //Clase para la paginacion
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //Cabecera
  displayedColumns = [
    'idLibro',
    'titulo',
    'idAutor',
    'nombreAutor',
    'apellidoAutor',
    'eliminar',
  ];

  constructor(
    private libroService: LibroService,
    private autorService: AutorService,
    private fb: FormBuilder // Inyecta FormBuilder
  ) {
    libroService.listarLibros().subscribe((x) => {
      this.lstLibro = x;
    });
    autorService.consulta('', '', '', 1, -1, -1).subscribe((x) => {
      this.lstAutor = x;
    });

    // Define el formulario reactivo con las validaciones
    this.form = this.fb.group({
      libro: ['-1', [Validators.required, Validators.min(1)]],
      autor: ['-1', [Validators.required, Validators.min(1)]],
    });
  }

  cargarListaLibroHasAutor(): void {
    this.libroService.listarLibrosHasAutor().subscribe((x) => {
      this.lstlibroHasAutor = x;
      this.dataSource = new MatTableDataSource<LibroHasAutor>(
        this.lstlibroHasAutor
      );
      this.dataSource.paginator = this.paginator;
      //console.log('cargarListaLibroHasAutor => ', this.lstlibroHasAutor);
    });
  }

  consultarPorIdLibroIdAutor(idLibro: number, idAutor: number): void {
    this.libroService.consultarPorParametros(idLibro, idAutor).subscribe({
      next: (x) => {
        console.log('consultarLibroHasAutor ==>>', x);
      },
      error: (error) => {},
    });
  }

  onAutorSelectionChange(selectedAutorId: number | undefined): void {
    this.idAutorSeleccionado = selectedAutorId;

    if (
      this.idAutorSeleccionado !== null &&
      this.idAutorSeleccionado !== undefined &&
      selectedAutorId !== -1
    ) {
      this.libroService.listarLibrosHasAutor().subscribe((x) => {
        this.lstlibroHasAutor = x;

        // Filtrar la lista por idAutor
        this.lstlibroHasAutorFiltered = this.lstlibroHasAutor.filter(
          (lh) => lh.autor?.idAutor === this.idAutorSeleccionado
        );

        // Actualizar la fuente de datos de la tabla con la lista filtrada
        this.dataSource = new MatTableDataSource<LibroHasAutor>(
          this.lstlibroHasAutorFiltered
        );
        this.dataSource.paginator = this.paginator;
      });
    } else {
      this.dataSource = new MatTableDataSource<LibroHasAutor>([]);

      this.dataSource.paginator = this.paginator;
    }
  }

  registrar(): void {
    if (this.form.valid) {
      const autorFormControl = this.form.get('autor');
      const libroFormControl = this.form.get('libro');

      if (autorFormControl && libroFormControl) {
        const idAutor = autorFormControl.value;
        const idLibro = libroFormControl.value;

        // Validar si ya existe el LibroHasAutor en la lista
        const yaRegistrado = this.lstlibroHasAutor.some(
          (lh) => lh.autor?.idAutor === idAutor && lh.libro?.idLibro === idLibro
        );

        if (yaRegistrado) {
          // Mostrar mensaje de que ya está registrado
          Swal.fire({
            icon: 'warning',
            title: 'Registro Duplicado',
            text: 'El libro ya tiene registrado al autor seleccionado.',
          });
        } else {
          // No está registrado, puedes continuar con el registro
          // Buscar el autor en la lista
          const autorSeleccionado = this.lstAutor.find(
            (a) => a.idAutor === idAutor
          );

          // Buscar el libro en la lista
          const libroSeleccionado = this.lstLibro.find(
            (l) => l.idLibro === idLibro
          );

          // Completar el objeto libroHasAutor
          if (autorSeleccionado && libroSeleccionado) {
            this.libroHasAutor.autor = autorSeleccionado;
            this.libroHasAutor.libro = libroSeleccionado;

            // Continuar con el registro
            this.libroHasAutor.libroHasAutorPK.idAutor = idAutor;
            this.libroHasAutor.libroHasAutorPK.idLibro = idLibro;
          }

          console.log('libroHasAutor Cargados ==>', this.libroHasAutor);
          this.libroService.guardarLibroHasAutor(this.libroHasAutor).subscribe(
            (respuesta) => {
              console.log('guardarLibroHasAutor ==> ', respuesta);
              // Mostrar mensaje de éxito
              Swal.fire({
                icon: 'success',
                title: 'Registro Exitoso',
                text: `El autor ${respuesta.autor?.nombres} ${respuesta.autor?.apellidos} se ha asignado al libro ${respuesta.libro?.titulo}.`,
              });
              // Limpiar el formulario o realizar otras acciones después del exito
              this.form.reset();
              this.onAutorSelectionChange(
                this.libroHasAutor.libroHasAutorPK.idAutor
              );
            },
            (error) => {
              // Mostrar mensaje de error genérico en el caso en que falle el registro de la asignacion del autor al libro
              console.log(error);
              Swal.fire({
                icon: 'error',
                title: 'Error en el registro',
                text: 'Hubo un error al intentar registrar el libro y autor. Por favor, inténtelo nuevamente.',
              });
            }
          );
        }
      }
    } else {
      // Mostrar un mensaje de error si el formulario no es válido
      Swal.fire({
        icon: 'error',
        title: 'Error de validación',
        text: 'Por favor, complete el formulario correctamente.',
      });
    }
  }

  eliminarLibroHasAutor(libroHasAutor: LibroHasAutor): void {
    // Mostrar mensaje de confirmación
    Swal.fire({
      title: '¿Estás seguro?',
      text: `¿Quieres eliminar la asignación del autor ${libroHasAutor.autor?.nombres} ${libroHasAutor.autor?.apellidos} al libro ${libroHasAutor.libro?.titulo}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        // Confirmado, proceder con la eliminación
        this.libroService.eliminarLibroHasAutor(libroHasAutor).subscribe({
          next: () => {
            // Éxito
            Swal.fire({
              icon: 'success',
              title: 'Eliminación Exitosa',
              text: `La asignación del autor ${libroHasAutor.autor?.nombres} ${libroHasAutor.autor?.apellidos} al libro ${libroHasAutor.libro?.titulo} ha sido eliminada.`,
            });
            // Volver a cargar la lista después de la eliminación
            this.onAutorSelectionChange(libroHasAutor.autor?.idAutor);
          },
          error: (error) => {
            // Error
            console.log(error);
            Swal.fire({
              icon: 'error',
              title: 'Error en la eliminación',
              text: 'Hubo un error al intentar eliminar la asignación del autor al libro. Por favor, inténtelo nuevamente.',
            });
          },
        });
      }
    });
  }
  
}
