import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Proveedor } from 'src/app/models/proveedor.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-proveedor',
  templateUrl: './agregar-proveedor.component.html',
  styleUrls: ['./agregar-proveedor.component.css']
})
export class AgregarProveedorComponent implements OnInit {
  lstPais: Pais[] = [];
  lstTipoProveedor: DataCatalogo[] = [];

  //declarando las validaciones
  formsRegistra = this.formBuilder.group({
    validaRazonSocial: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ0-9 ]{3,30}')]],
    validaRuc: ['', [Validators.required, Validators.pattern('^(10|20)[0-9]{9}$')]],
    validaDireccion: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ0-9 ]{3,30}')]],
    validaCelular: ['', [Validators.required, Validators.pattern('^9[0-9]{8}$')]],
    validaContacto: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaPais: ['', Validators.min(1)],
    validaTipoProveedor: ['', Validators.min(1)],
  });

  proveedor: Proveedor = {
    "razonsocial": "",
    "ruc": "",
    "direccion": "",
    "celular": "",
    "contacto": "",
    "pais": {
      "idPais": -1,
    },
    "tipoProveedor": {
      "idDataCatalogo": -1,
    },
  };

  objUsuario: Usuario = {};

  constructor(private formBuilder: FormBuilder, private proveedorService: ProveedorService, private utilService: UtilService, private tokenService: TokenService) {
    utilService.listaPais().subscribe(data => this.lstPais = data)
    utilService.listaTipoProveedor().subscribe(x => this.lstTipoProveedor = x)
    this.objUsuario.idUsuario = tokenService.getUserId();
  }

  registra() {
    this.proveedor.usuarioActualiza = this.objUsuario;
    this.proveedor.usuarioRegistro = this.objUsuario;
    this.proveedorService.registrarProveedor(this.proveedor).subscribe(
      x => {
        Swal.fire({
          icon: 'info',
          title: 'Resultado del Registro Proveedor',
          text: x.mensaje,
        })
      },
    );
  }

  ngOnInit(): void {
  }

}
