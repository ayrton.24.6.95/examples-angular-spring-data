import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Libro } from 'src/app/models/libro.model';
import { LibroService } from 'src/app/services/libro.service';
import { UtilService } from 'src/app/services/util.service';
import { CrudLibroUpdateComponent } from '../crud-libro-update/crud-libro-update.component';
import { CrudLibroAddComponent } from '../crud-libro-add/crud-libro-add.component';
import Swal from 'sweetalert2';
import { TokenService } from 'src/app/security/token.service';
import { Usuario } from 'src/app/models/usuario.model';

@Component({
  selector: 'app-crud-libro',
  templateUrl: './crud-libro.component.html',
  styleUrls: ['./crud-libro.component.css']
})
export class CrudLibroComponent implements OnInit {

    //Para los util
    CategoriaLibro: DataCatalogo [] = [];
    TipoLibro: DataCatalogo[] =[];
    

    objUsuario: Usuario = {

    };

  //Grilla:
  filtro: string = "";
  dataSource: any;
  @ViewChild(MatPaginator, {static:true}) paginator!: MatPaginator;
  displayedColumns = ["idLibro","titulo","anio","serie","fechaRegistro","categoriaLibro","tipoLibro","estado","acciones"];


  constructor(private formBuilder: FormBuilder,
    private dialogService: MatDialog,
    private libroService: LibroService,
    private utilService: UtilService,
    private tokenService:TokenService) { 
      utilService.listaCategoriaDeLibro().subscribe(
        x => this.CategoriaLibro =x
       )
       utilService.listaTipoLibroRevista().subscribe(
        x=> this.TipoLibro =x
       )
    }

    openAddDialog(){
      console.log(">>>>openDialog >>>");
      const dialogRef = this.dialogService.open(CrudLibroAddComponent);
      dialogRef.afterClosed().subscribe(
        result => {console.log(">>result >>" + result);
        if (result === 1){
           this.refreshTable();
        }
      });
    }

    openUpdateDialog(obj:Libro){
       const dialogRef = this.dialogService.open(CrudLibroUpdateComponent,{data:obj});

       dialogRef.afterClosed().subscribe(x=>{
        if(x === 1){
          this.refreshTable();
        }
       });
    }

  ngOnInit(): void {
  }

  consultaLibro() {
    console.log(">>>consultaLibro>>>>" + this.filtro);
    this.refreshTable();
  }

  actualizaEstado(obj: Libro){
    obj.estado = obj.estado == 1 ? 0 : 1;
    this.libroService.actualiza(obj).subscribe();
  }

  elimina(obj:Libro){
    Swal.fire({
      title:'¿Desea Eliminar?',
      text: "Los cambios no se van a revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor:'#3085d6',
      cancelButtonColor:'#d33',
      confirmButtonText: 'Sí, elimina',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
         if (result.isConfirmed){
          this.libroService.elimina(obj.idLibro || 0).subscribe(
             x=> {
              this.refreshTable();
              Swal.fire('Mensaje', x.mensaje,'info');
             }
          );
         }
    })
  }

  private refreshTable(){
    
    this.libroService.consultarPorTitulo(this.filtro == "" ? "todos" : this.filtro).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Libro>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

}
  

