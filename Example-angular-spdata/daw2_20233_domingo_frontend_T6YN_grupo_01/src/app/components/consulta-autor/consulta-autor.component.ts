import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Autor } from 'src/app/models/autor.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AutorService } from 'src/app/services/autor.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-consulta-autor',
  templateUrl: './consulta-autor.component.html',
  styleUrls: ['./consulta-autor.component.css']
})
export class ConsultaAutorComponent implements OnInit {

   //Grila
   dataSource:any;

   //Clase para la paginacion
   @ViewChild (MatPaginator, { static: true }) paginator!: MatPaginator;

   //Cabecera
   displayedColumns = ["idAutor","nombres","apellidos","fechaNacimiento","telefono","fechaRegistro","hora","estado","pais","grado"]; //

  //parametros de la consulta
  nombres:string  = "";
  apellidos:string  = "";
  telefono:string  = "";
  estado:boolean  = true;
  selPais:number = -1;
  selGrado:number = -1;

  //combos: import Pais DataCatalogo
  lstPais: Pais[] = [];
  lstGrado: DataCatalogo[] = [];

  objUsuario: Usuario = {};

  constructor(
    private autorService: AutorService,
    private utilService: UtilService,
    private tokenService: TokenService
  ) {
    this.utilService.listaPais().subscribe(
      x => this.lstPais = x
    )
    this.utilService.listaGradoAutor().subscribe(
      a => this.lstGrado = a
    )
    this.objUsuario.idUsuario = tokenService.getUserId();
  } // fin de constructor

  consulta(){
    console.log(">> nombres >> " + this.nombres) ;
    console.log(">> apellidos >> " + this.apellidos) ;
    console.log(">> telefono >> " + this.telefono) ;
    console.log(">> estado >> " + this.estado) ;
    console.log(">> selPais >> " + this.selPais) ;
    console.log(">> selGrado >> " + this.selGrado) ;
   
    this.autorService.consulta(this.nombres, this.apellidos, this.telefono, this.estado?1:0, this.selPais, this.selGrado).subscribe(
           x => {
                 this.dataSource = new MatTableDataSource<Autor>(x);
                 this.dataSource.paginator = this.paginator;
           }
    );
  } // fin de consulta 

  exportarPDF() {
    this.autorService.generateDocumentReport(this.nombres, this.apellidos, this.telefono, this.estado?1:0, this.selPais, this.selGrado).subscribe(
          response => {
            console.log(response);
            var url = window.URL.createObjectURL(response.data);
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.setAttribute('target', 'blank');
            a.href = url;
            a.download = response.filename;
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove();
        }); 
  }




  ngOnInit(): void {
  }

}
