import { Component, Inject } from '@angular/core';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Usuario } from 'src/app/models/usuario.model';
import { Sala } from 'src/app/models/sala.model';
import { FormBuilder, Validators } from '@angular/forms';
import { SalaService } from 'src/app/services/sala.service';
import { TokenService } from 'src/app/security/token.service';
import { UtilService } from 'src/app/services/util.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-sala-update',
  templateUrl: './crud-sala-update.component.html',
  styleUrls: ['./crud-sala-update.component.css']
})
export class CrudSalaUpdateComponent {
  TipoSala: DataCatalogo[] = [];
  Sede: DataCatalogo[] = []
  objUsuario: Usuario = {};

  sala: Sala={
    idSala:0,
    numero: "",
    piso: 0,
    numAlumnos: 0,
    recursos: "",
    tipoSala:{
      idDataCatalogo:-1
    },
    sede:{
      idDataCatalogo:-1
    }
  }

  formsActualiza = this.formBuilder.group({
    validaAula:['',[Validators.required, Validators.pattern('^[A-Z]\\d{3}$')]],
    validaPiso:['',[Validators.required, Validators.pattern('^[1-3]$')]],
    validaCantidadAlumno:['',[Validators.required, Validators.pattern('^(1[0-9]|2[0-9]|50)$')]],
    validaRecurso:['',[Validators.required, Validators.pattern('^[a-zA-Z0-9 ]{5,50}$')]],
    validaTipoSala:['',Validators.min(1)],
    validaSede:['',Validators.min(1)]
  });

  constructor(private formBuilder: FormBuilder,
    private salaService: SalaService,
    private utilService: UtilService,
    private tokenService:TokenService,
    @Inject(MAT_DIALOG_DATA)public data:any){
      this.sala = data;
      utilService.listaTipoSala().subscribe(
        x => this.TipoSala= x
      )
      utilService.listaSede().subscribe(
        x => this.Sede= x
      )
      this.objUsuario.idUsuario = tokenService.getUserId();
  }

  cargarTipoSala(){
    console.log(this.sala.tipoSala?.descripcion);
    this.utilService.listaTipoSala().subscribe(
      x => this.TipoSala = x
    );
  }

  cargaSede(){
    console.log(this.sala.sede?.descripcion);
    this.utilService.listaSede().subscribe(
      x => this.Sede = x
    );
  }

  ngOnInit(): void {}

  actualiza(){
    this.sala.usuarioRegistro = this.objUsuario;
    this.sala.usuarioActualiza = this.objUsuario;
    this.salaService.actualiza(this.sala).subscribe(
      x => {
        Swal.fire('Mensaje', x.mensaje, 'info');
      }
    );
  }
}
