import { Component } from '@angular/core';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Usuario } from 'src/app/models/usuario.model';
import { SalaService } from 'src/app/services/sala.service';
import { FormBuilder, Validators } from '@angular/forms';
import { TokenService } from 'src/app/security/token.service';
import { UtilService } from 'src/app/services/util.service';
import { Sala } from 'src/app/models/sala.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-sala-add',
  templateUrl: './crud-sala-add.component.html',
  styleUrls: ['./crud-sala-add.component.css']
})
export class CrudSalaAddComponent {
  TipoSala: DataCatalogo[] = [];
  Sede: DataCatalogo[] = []
  objUsuario: Usuario = {};

  formRegistra = this.formBuilder.group({
    validaAula:['',[Validators.required, Validators.pattern('^[A-Z]\\d{3}$')]],
    validaPiso:['',[Validators.required, Validators.pattern('^[1-3]$')]],
    validaCantidadAlumno:['',[Validators.required, Validators.pattern('^(1[0-9]|2[0-9]|50)$')]],
    validaRecurso:['',[Validators.required, Validators.pattern('^[a-zA-Z0-9 ]{5,50}$')]],
    validaTipoSala:['',Validators.min(1)],
    validaSede:['',Validators.min(1)]
  });

  sala: Sala={
    numero: "",
    piso: 0,
    numAlumnos: 0,
    recursos: "",
    estado: 1,
    tipoSala:{
      idDataCatalogo:-1
    },
    sede:{
      idDataCatalogo:-1
    }
  }

  constructor(private formBuilder: FormBuilder,
    private salaService: SalaService,
    private utilService: UtilService,
    private tokenService:TokenService){
      utilService.listaTipoSala().subscribe(
        x => this.TipoSala= x
      )
      utilService.listaSede().subscribe(
        x => this.Sede= x
      )
      this.objUsuario.idUsuario = tokenService.getUserId();
  }

  ngOnInit(): void {
  }

  registrarSala(){
    this.sala.usuarioRegistro = this.objUsuario;
    this.sala.usuarioActualiza = this.objUsuario;
    this.salaService.inserta(this.sala).subscribe(
      x=>{
        Swal.fire('Mensaje', x.mensaje, 'info');
        this.sala = {
          idSala:0,
          numero: "",
          piso: 0,
          numAlumnos: 0,
          recursos: "",
          tipoSala:{
            idDataCatalogo:-1
          },
          sede:{
            idDataCatalogo:-1
          }
        };
      }
    );
  }
}

