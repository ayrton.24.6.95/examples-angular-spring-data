import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Autor } from 'src/app/models/autor.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AutorService } from 'src/app/services/autor.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-agregar-autor',
  templateUrl: './agregar-autor.component.html',
  styleUrls: ['./agregar-autor.component.css']
})
export class AgregarAutorComponent implements OnInit {

  //combos
  lstPais: Pais[] = [];
  lstGrado: DataCatalogo[] = [];

  // declaraciones de las validaciones
  formsRegistra = this.formBuilder.group({
  validaNombre: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}') ] ],
  validaApellido: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}') ] ],
  validaFecha: ['', Validators.required],
  validaTelefono: ['', [Validators.required, Validators.pattern('[0-9]{9}')]],
  validaPais: ['', Validators.min(1)] ,
  validaGrado: ['', Validators.min(1)] ,
  
});

  //JSON para registrar o actualizar
  autor: Autor = {
    nombres: "",
    apellidos: "",
    fechaNacimiento: null,
    telefono:"",
    estado :1,
    pais: {
      idPais: -1
    },
    grado: {
      idDataCatalogo: -1
    }
  }

  objUsuario: Usuario = {};

  constructor(
    private formBuilder: FormBuilder,
    private autorService: AutorService, 
    private utilService: UtilService, 
    private tokenService: TokenService
    ) {
    this.utilService.listaPais().subscribe(
      x => this.lstPais = x
    )
    this.utilService.listaGradoAutor().subscribe(
      a => this.lstGrado = a
    )
    this.objUsuario.idUsuario = tokenService.getUserId();
  } // fin constructor

  insertaAutor() {
    this.autor.usuarioActualiza = this.objUsuario;
    this.autor.usuarioRegistro = this.objUsuario;
    this.autorService.resgistarAutor(this.autor).subscribe(
      x => {
        Swal.fire({
          icon: 'info',
          title: 'Resultado del Registro',
          text: x.mensaje,
        })
        // limpiar los campos despues de registrar
        this.autor = {
          nombres: "",
          apellidos: "",
          fechaNacimiento: null,
          telefono:"",
          estado :1,
          pais: {
            idPais: -1
          },
          grado: {
            idDataCatalogo: -1
          }
        }

      },

    );
  }// fin registrarAutor


ngOnInit(): void {
}

} // fin de class AgregarAutorComponent
