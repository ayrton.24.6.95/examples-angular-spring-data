import { DatePipe, formatDate } from '@angular/common';
import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { Editorial } from 'src/app/models/editorial.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { EditorialService } from 'src/app/services/editorial.service';
import { UtilService } from 'src/app/services/util.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-editorial',
  templateUrl: './agregar-editorial.component.html',
  styleUrls: ['./agregar-editorial.component.css'],
})
export class AgregarEditorialComponent {
  lstPais: Pais[] = [];
  editorial: Editorial = {
    razonSocial: '',
    direccion: '',
    ruc: '',
    estado: 1,
    fechaRegistro: '',
    fechaCreacion: '',
    pais: {
      idPais: 173,
    },
  };

  listEstado = [
    {
      id: 0,
      detalle: 'Inactivo',
    },
    {
      id: 1,
      detalle: 'Activo',
    },
  ];

  objUsuario: Usuario = {};

  form: FormGroup; // Define el formulario reactivo

  constructor(
    private dateAdapter: DateAdapter<Date>,
    private utilService: UtilService,
    private tokenService: TokenService,
    private editorialService: EditorialService,
    private fb: FormBuilder // Inyecta FormBuilder
  ) {
    utilService.listaPais().subscribe((x) => (this.lstPais = x));
    this.objUsuario.idUsuario = tokenService.getUserId();

    // Define el formulario reactivo con las validaciones
    this.form = this.fb.group({
      razonSocial: [
        '',
        [Validators.required, Validators.pattern('[A-Za-z ]*')],
      ],
      direccion: ['', [Validators.required]],
      ruc: ['', [Validators.required, Validators.pattern('^[0-9]{1,11}$')]],
      fechaCreacion: ['', [Validators.required]],
      pais: ['-1', [Validators.required, Validators.min(1)]],
    });
        
  }

  registra_antiguo() {
    this.editorial.usuarioActualiza = this.objUsuario;
    this.editorial.usuarioRegistro = this.objUsuario;

    const datepipe: DatePipe = new DatePipe('en-US');
    let formattedDate = datepipe.transform(
      this.editorial.fechaCreacion!,
      'YYYY-MM-dd HH:mm:ss'
    );

    this.editorial.fechaCreacion = formattedDate!;

    console.log(this.editorial);
    this.editorialService.registrar(this.editorial).subscribe((x) => {
      Swal.fire({
        icon: 'info',
        title: 'Resultado del Registro',
        text: x.mensaje,
      });
    });
  }

  limpiarFormulario(){
    this.form.get('razonSocial')?.setValue('');
    this.form.get('direccion')?.setValue('');
    this.form.get('ruc')?.setValue('');
    this.form.get('fechaCreacion')?.setValue('');
    this.form.get('pais')?.setValue('-1');
  }

  registra() {
    // Validar el formulario antes de enviar los datos
    if (this.form.valid) {
      this.editorial.usuarioActualiza = this.objUsuario;
      this.editorial.usuarioRegistro = this.objUsuario;

      // Asignar los valores del formulario al objeto editorial
      this.editorial.razonSocial = this.form.get('razonSocial')?.value;
      this.editorial.direccion = this.form.get('direccion')?.value;
      this.editorial.ruc = this.form.get('ruc')?.value;
      this.editorial.fechaCreacion = this.form.get('fechaCreacion')?.value;
      if (this.form.get('pais')) {
        this.editorial.pais = { idPais: this.form.get('pais')?.value } ?? {};
      }

      const datepipe: DatePipe = new DatePipe('en-US');
      let formattedDate = datepipe.transform(
        this.editorial.fechaCreacion!,
        'YYYY-MM-dd HH:mm:ss'
      );
      this.editorial.fechaCreacion = formattedDate!;
      console.log("Editorial a registrar:",this.editorial);

      // Realizar la llamada al servicio de registro aquí
      this.editorialService.registrar(this.editorial).subscribe((x) => {
        Swal.fire({
          icon: 'info',
          title: 'Resultado del Registro',
          text: x.mensaje,
        });
        this.limpiarFormulario();
      });
    } else {
      // Mostrar un mensaje de error si el formulario no es válido
      Swal.fire({
        icon: 'error',
        title: 'Error de validación',
        text: 'Por favor, complete el formulario correctamente.',
      });
    }
  }
}
