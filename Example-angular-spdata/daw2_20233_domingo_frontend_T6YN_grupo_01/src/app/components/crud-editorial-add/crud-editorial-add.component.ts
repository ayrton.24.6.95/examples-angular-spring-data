import { Component } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { Editorial } from 'src/app/models/editorial.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { EditorialService } from 'src/app/services/editorial.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-crud-editorial-add',
  templateUrl: './crud-editorial-add.component.html',
  styleUrls: ['./crud-editorial-add.component.css'],
})
export class CrudEditorialAddComponent {
  lstPais: Pais[] = [];
  editorial: Editorial = {
    razonSocial: '',
    direccion: '',
    ruc: '',
    estado: 1,
    fechaRegistro: '',
    fechaCreacion: '',
    pais: {
      idPais: 173,
    },
  };

  listEstado = [
    {
      id: 0,
      detalle: 'Inactivo',
    },
    {
      id: 1,
      detalle: 'Activo',
    },
  ];

  objUsuario: Usuario = {};

  constructor(
    private dateAdapter: DateAdapter<Date>,
    private utilService: UtilService,
    private tokenService: TokenService,
    private editorialService: EditorialService
  ) {
    utilService.listaPais().subscribe((x) => (this.lstPais = x));
    this.objUsuario.idUsuario = tokenService.getUserId();
  }

  guardar(editorial:Editorial){

  }

}
