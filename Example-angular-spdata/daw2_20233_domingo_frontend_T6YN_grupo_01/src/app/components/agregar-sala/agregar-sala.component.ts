import { Component, OnInit} from '@angular/core';
import { SalaService } from 'src/app/services/sala.service';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { UtilService } from 'src/app/services/util.service';
import { Sala } from 'src/app/models/sala.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-sala',
  templateUrl: './agregar-sala.component.html',
  styleUrls: ['./agregar-sala.component.css']
})
export class AgregarSalaComponent implements OnInit {

  lstSala: DataCatalogo[] = []
  lstSede: DataCatalogo[] = []
  sala: Sala={
    numero: "",
    piso: 0,
    numAlumnos: 0,
    recursos: "",
    tipoSala:{
      idDataCatalogo:-1
    },
    sede:{
      idDataCatalogo:-1
    }
  }

  objUsuario: Usuario = {} ;
  constructor(private salaService:SalaService , private utilService: UtilService, private tokenService: TokenService) { 
    utilService.listaTipoSala().subscribe(
      x => this.lstSala=x
    )
    utilService.listaSede().subscribe(
      y => this.lstSede=y
    )
    this.objUsuario.idUsuario = tokenService.getUserId();
  }




  registra() {
      this.sala.usuarioActualiza = this.objUsuario;
      this.sala.usuarioRegistro = this.objUsuario;
      this.salaService.registrar(this.sala).subscribe(
        x => {
          Swal.fire({
            icon: 'info',
            title: 'Resultado del Registro',
            text: x.mensaje,
          });
        },
      );
  }
  
  ngOnInit(): void {
  }

}
