import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Revista } from 'src/app/models/revista.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { RevistaService } from 'src/app/services/revista.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-revista-add',
  templateUrl: './crud-revista-add.component.html',
  styleUrls: ['./crud-revista-add.component.css']
})
export class CrudRevistaAddComponent {

  lstPais: Pais[] = [];
  lstRevista: DataCatalogo[] = []; 

  //declaracion de las validaciones
  formsRegistra = this.formBuilder.group({
    validaNombre: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaFrecuencia: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaFechaCreacion:  ['', Validators.required],       
    validaPais: ['', Validators.min(1)],
    validaTipoRevista: ['', Validators.min(1)],
  });
 
    //json
    revista: Revista = {
      nombre: "",
      frecuencia: "",
      fechaCreacion: null,
      estado: 1,
      pais: {
        idPais: -1
      },
      tipoRevista: {
        idDataCatalogo: -1
      }
    }

    objUsuario: Usuario = {};

    constructor(private formBuilder: FormBuilder,
      private revistaService: RevistaService,
      private utilService: UtilService,
      private tokenService: TokenService
    ) {
      //Adicionar combos pais y tipo de revistas
      utilService.listaPais().subscribe(
        x => this.lstPais = x
      )
      utilService.listaTipoLibroRevista().subscribe(
        y => this.lstRevista = y
       )
  
      this.objUsuario.idUsuario = tokenService.getUserId();
    }

    registra(){
      this.revista.usuarioActualiza = this.objUsuario;
      this.revista.usuarioRegistro = this.objUsuario;
      this.revistaService.inserta(this.revista).subscribe(
        x=>{
          Swal.fire({
            icon: 'info',
            title: 'Resultado del Registro',
            text: x.mensaje,
          })
          this.revista = {
            nombre: "",
            frecuencia: "",
            fechaCreacion: null,
            estado: 1,
            pais: {
              idPais: -1
            },
            tipoRevista: {
              idDataCatalogo: -1
            }
          };
        },
      );
  }

}
