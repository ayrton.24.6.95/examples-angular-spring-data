import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { LibroService } from 'src/app/services/libro.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-libro',
  templateUrl: './agregar-libro.component.html',
  styleUrls: ['./agregar-libro.component.css']
})
export class AgregarLibroComponent implements OnInit{
  lstCategoria: DataCatalogo [] = [];
  lstTipo: DataCatalogo[] =[];
  
  objUsuario: Usuario = {};

  formRegistra = this.formBuilder.group({
    validaTitulo:['',[Validators.required, Validators.pattern('^[a-zA-Zá-úÁ-ÚñÑ0-9 ]{3,30}$')]],
    validaAnio:['',[Validators.required, Validators.pattern('[0-9]{4}')]],
    validaSerie:['',[Validators.required, Validators.pattern('[a-zA-Z0-9]{12}$')]],
    validaCategoria:['',Validators.min(1)],
    validaTipo:['',Validators.min(1)]


  });
 
  constructor( private formBuilder: FormBuilder,
    private utilService: UtilService,
    private libroService: LibroService , 
    private tokenService:TokenService) {
         utilService.listaCategoriaDeLibro().subscribe(
          x => this.lstCategoria=x
         )
         utilService.listaTipoLibroRevista().subscribe(
          x=> this.lstTipo=x
         )

         this.objUsuario.idUsuario = tokenService.getUserId();

}

//Json
objLibro: Libro ={
  titulo:"",
   anio: 0,
   serie:"",
   estado:1,
   categoriaLibro:{
      idDataCatalogo:-1
   },
   tipoLibro:{
    idDataCatalogo:-1
   }
}

ngOnInit(): void {
    
}

registrar(){
  this.objLibro.usuarioRegistro = this.objUsuario;
  this.objLibro.usuarioActualiza = this.objUsuario;
  this.libroService.registrar(this.objLibro).subscribe(
     x=>{
       Swal.fire({
         icon: 'info',
         title: 'Resultado del registro',
         text: x.mensaje,
         })
     },
  );

 }


 
  
     
   
}
 

 



