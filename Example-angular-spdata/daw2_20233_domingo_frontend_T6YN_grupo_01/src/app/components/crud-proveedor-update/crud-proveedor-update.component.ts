import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Proveedor } from 'src/app/models/proveedor.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-proveedor-update',
  templateUrl: './crud-proveedor-update.component.html',
  styleUrls: ['./crud-proveedor-update.component.css']
})
export class CrudProveedorUpdateComponent {

  pais: Pais[] = [];
  tipoProveedor: DataCatalogo[] = [];

  objUsuario: Usuario = {};

   //declarando las validaciones
   formsActualiza = this.formBuilder.group({
    validaRazonSocial: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ0-9 ]{3,30}')]],
    validaRuc: ['', [Validators.required, Validators.pattern('^(10|20)[0-9]{9}$')]],
    validaDireccion: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ0-9 ]{3,30}')]],
    validaCelular: ['', [Validators.required, Validators.pattern('^9[0-9]{8}$')]],
    validaContacto: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaPais: ['', Validators.min(1)],
    validaTipoProveedor: ['', Validators.min(1)],
  });

  //JSON para registro y update
  proveedor: Proveedor = {
    "idProveedor": 0,
		"razonsocial": "",
		"ruc": "",
		"direccion": "",
		"celular": "",
		"contacto": "",
		"pais": {
			"idPais": -1,
		},
		"tipoProveedor": {
			"idDataCatalogo": -1,
		},
  };


  constructor(private formBuilder: FormBuilder,
    private dialogService: MatDialog,
    private proveedorService: ProveedorService,
    private utilService: UtilService,
    private tokenService: TokenService,
    @Inject(MAT_DIALOG_DATA) public data:any) {
      this.proveedor = data;
      this.utilService.listaPais().subscribe(x => this.pais = x);
      this.utilService.listaTipoProveedor().subscribe(y => this.tipoProveedor = y);
      this.objUsuario.idUsuario = tokenService.getUserId();
    }

    cargaPais(){
      console.log(">>> pais >>> " + this.proveedor.pais?.nombre);
      this.utilService.listaPais().subscribe(
        x => this.pais = x
      );
    }

    cargaTipoProveedor(){
      console.log(">>> Tipo proveedor >>> " + this.proveedor.tipoProveedor?.descripcion);
      this.utilService.listaTipoProveedor().subscribe(
        y => this.tipoProveedor = y
      );
    }

    actualiza(){
      this.proveedor.usuarioActualiza = this.objUsuario;
      this.proveedorService.actualiza(this.proveedor).subscribe(
        x =>  Swal.fire('Mensaje', x.mensaje, 'info')
      )
    }

    ngOnInit(): void {
    }

}
