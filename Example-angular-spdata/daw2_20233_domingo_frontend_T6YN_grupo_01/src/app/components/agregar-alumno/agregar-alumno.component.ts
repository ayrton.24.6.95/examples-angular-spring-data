import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Alumno } from 'src/app/models/alumno.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-alumno',
  templateUrl: './agregar-alumno.component.html',
  styleUrls: ['./agregar-alumno.component.css']
})
export class AgregarAlumnoComponent implements OnInit {

  lstPais: Pais[] = []
  lstModalidad: DataCatalogo[] = []

  //declarar las validaciones
  formsRegistra = this.formBuilder.group({
    validaNombres: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaApellidos: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaTelefono: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    validaDni: ['', [Validators.required, Validators.pattern('[0-9]{8}')]],
    validaCorreo: ['', [Validators.required, Validators.email]],
    validaNacimiento: ['', Validators.required],
    validaPais: ['', Validators.min(1)],
    validaModalidad: ['', Validators.min(1)],
  })


  objAlumno: Alumno = {
    idAlumno:0,
    nombres: "",
    apellidos: "",
    telefono: "",
    dni: "",
    correo: "",
    estado: 1,
    fechaNacimiento: new Date(),
    pais: {
      idPais: -1
    },
    modalidad: {
      idDataCatalogo: -1
    },
  };

  objUsuario: Usuario = {};

  constructor(private formBuilder: FormBuilder,
              private utilService: UtilService, 
              private alumnoService: AlumnoService, 
              private tokenService: TokenService) {
    this.utilService.listaPais().subscribe(
      data => this.lstPais = data
    );

    this.utilService.listaModalidadAlumno().subscribe(
      mod => this.lstModalidad = mod
    );

    this.objUsuario.idUsuario = tokenService.getUserId();
  }

  inserta() {
    this.objAlumno.usuarioActualiza = this.objUsuario;
    this.objAlumno.usuarioRegistro = this.objUsuario;
    this.alumnoService.registraAlumno(this.objAlumno).subscribe(

      x => Swal.fire({ icon: 'info', title: 'Resultado del Registro - Rocio', text: x.mensaje }),
    );
  }

  ngOnInit(): void {
  }

}