import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Autor } from 'src/app/models/autor.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AutorService } from 'src/app/services/autor.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-autor-update',
  templateUrl: './crud-autor-update.component.html',
  styleUrls: ['./crud-autor-update.component.css']
})
export class CrudAutorUpdateComponent {
    //combos
    lstPais: Pais[] = [];
    lstGrado: DataCatalogo[] = [];
 
  // declaraciones de las validaciones
  formsActualiza = this.formBuilder.group({
    validaNombre: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaApellido: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
    validaTelefono: ['', [Validators.required, Validators.pattern('[0-9]{9}')]],
    validaFecha: ['', Validators.required],
    validaPais: ['', Validators.min(1)],
    validaGrado: ['', Validators.min(1)],

  });

      //JSON para registrar o actualizar
      autor: Autor = {
        idAutor :0,
        nombres: "",
        apellidos: "",
        fechaNacimiento: null,
        telefono:"",
        estado:1,
        pais: {
          idPais: -1
        },
        grado: {
          idDataCatalogo: -1
        }
      };   
  
    objUsuario: Usuario = {};
  
    constructor(
              private formBuilder: FormBuilder,
              private autorService: AutorService, 
              private utilService: UtilService, 
              private tokenService: TokenService,
              @Inject(MAT_DIALOG_DATA) public data: any
              ) {
                this.autor = data;

      this.utilService.listaPais().subscribe(
        x => this.lstPais = x
      )
      this.utilService.listaGradoAutor().subscribe(
        a => this.lstGrado = a
      )
      this.objUsuario.idUsuario = tokenService.getUserId();
  
    } // fin constructor

    actualizaPC2() {

      this.autor.usuarioActualiza = this.objUsuario;
      
      this.autorService.actualiza(this.autor).subscribe(
        x =>  Swal.fire('Mensaje', x.mensaje, 'info')
    );
  
    } // fin del actualiza()
  
   

} // fin de export class CrudAutorAddComponent
