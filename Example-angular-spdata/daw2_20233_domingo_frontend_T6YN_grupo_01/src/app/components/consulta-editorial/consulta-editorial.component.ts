import { Component, ViewChild } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Editorial } from 'src/app/models/editorial.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { EditorialService } from 'src/app/services/editorial.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-consulta-editorial',
  templateUrl: './consulta-editorial.component.html',
  styleUrls: ['./consulta-editorial.component.css'],
})
export class ConsultaEditorialComponent {
  objUsuario: Usuario = {};
  lstPais: Pais[] = [];

  razon: string = '';
  direccion: string = '';
  ruc: string = '';
  estado: boolean = true;
  selPais: number = -1;

  //Grila
  dataSource: any;

  //Clase para la paginacion
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //Cabecera
  displayedColumns = [
    'idEditorial',
    'razonSocial',
    'direccion',
    'ruc',
    'fechaCreacion',
    'fechaRegistro',
    'fechaActualizacion',
    'estado',
    'pais',
  ];

  constructor(
    private utilService: UtilService,
    private dateAdapter: DateAdapter<Date>,
    private tokenService: TokenService,
    private editorialService: EditorialService
  ) {
    utilService.listaPais().subscribe((x) => (this.lstPais = x));
    this.objUsuario.idUsuario = tokenService.getUserId();
  }

  consulta() {
    console.log('>> razon >> ' + this.razon);
    console.log('>> direccion >> ' + this.direccion);
    console.log('>> ruc >> ' + this.ruc);
    console.log('>> estado >> ' + this.estado);
    console.log('>> selPais >> ' + this.selPais);

    this.editorialService
      .busquedaPorParametro(
        this.razon,
        this.direccion,
        this.ruc,
        this.estado ? 1 : 0,
        this.selPais
      )
      .subscribe((x) => {
        this.dataSource = new MatTableDataSource<Editorial>(x);
        this.dataSource.paginator = this.paginator;
      });
  }

  exportarPDF() {
    this.editorialService
      .generateDocumentReport(
        this.razon,
        this.direccion,
        this.ruc,
        this.estado ? 1 : 0,
        this.selPais
      )
      .subscribe((response) => {
        console.log(response);
        var url = window.URL.createObjectURL(response.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.setAttribute('target', 'blank');
        a.href = url;
        a.download = response.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      });
  }

  cargaPais(){
    this.utilService.listaPais().subscribe((x) => (this.lstPais = x));
    this.selPais = -1;
  }
}
