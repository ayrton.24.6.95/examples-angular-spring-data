import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/services/libro.service';
import { UtilService } from 'src/app/services/util.service';



@Component({
  selector: 'app-consulta-libro',
  templateUrl: './consulta-libro.component.html',
  styleUrls: ['./consulta-libro.component.css']
})
export class ConsultaLibroComponent implements OnInit {

  //Para los util
  CategoriaLibro: DataCatalogo[] = [];
  TipoLibro: DataCatalogo[] = [];
  objUsuario: Usuario = {};

  //Grilla:
  dataSource: any;

  //Clase para paginacion 
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //cabecera
  displayedColumns = ["idLibro", "titulo", "anio", "serie", "estado", "fechaRegistro", "categoriaLibro", "tipoLibro"];

  //parametros de consulta
  titulo: string = "";
  anio: number = 0;
  serie: string = "";
  estado: boolean = true;
  categoriaLibro: number = -1;
  tipoLibro: number = -1;


  constructor(private utilService: UtilService, private libroService: LibroService) {
    utilService.listaCategoriaDeLibro().subscribe(
      x => this.CategoriaLibro = x
    )
    utilService.listaTipoLibroRevista().subscribe(
      x => this.TipoLibro = x
    )
  }


  cargaCategoriaLibro() {
    //console.log(this.libro.categoriaLibro?.descripcion);
    this.utilService.listaCategoriaDeLibro().subscribe(
      x => this.CategoriaLibro = x
    );

  }

  cargaTipoLibro() {
    //console.log(this.libro.categoriaLibro?.descripcion);
    this.utilService.listaTipoLibroRevista().subscribe(
      x => this.TipoLibro = x
    );

  }

  consulta() {
    //console.log(">>titulo>>"+this.titulo);
    console.log(">>titulo>>" + this.categoriaLibro);
    this.libroService.consulta(this.titulo, this.anio, this.serie, this.estado ? 1 : 0, this.categoriaLibro, this.tipoLibro).subscribe(
      x => {
        console.log(x);
        this.dataSource = new MatTableDataSource<Libro>(x);
        this.dataSource.paginator = this.paginator
      }
    );
  }

  exportarPDF() {
    this.libroService.generateDocumentReport(this.titulo, this.anio, this.serie, this.estado ? 1 : 0, this.categoriaLibro, this.tipoLibro).subscribe(response => {
      console.log(response);
      var url = window.URL.createObjectURL(response.data);
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.setAttribute('target', 'blank');
      a.href = url;
      a.download = response.filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();

    });
  }

  ngOnInit(): void {
  }

}


