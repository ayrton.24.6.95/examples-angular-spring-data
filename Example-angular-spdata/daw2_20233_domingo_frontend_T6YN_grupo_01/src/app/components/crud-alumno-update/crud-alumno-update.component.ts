import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Alumno } from 'src/app/models/alumno.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-alumno-update',
  templateUrl: './crud-alumno-update.component.html',
  styleUrls: ['./crud-alumno-update.component.css']
})
export class CrudAlumnoUpdateComponent {

  pais : Pais[] = []
  modalidad : DataCatalogo[] = []

    //declarar las validaciones
    formsActualiza = this.formBuilder.group({
      validaNombres: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
      validaApellidos: ['', [Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
      validaTelefono: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      validaDni: ['', [Validators.required, Validators.pattern('[0-9]{8}')]],
      validaCorreo: ['', [Validators.required, Validators.email]],
      validaNacimiento: ['', Validators.required],
      validaPais: ['', Validators.min(1)],
      validaModalidad: ['', Validators.min(1)],
    })

  alumno: Alumno = {
    idAlumno:0,
    nombres:"",
    apellidos:"",
    telefono:"",
    dni:"",
    correo:"",
    fechaNacimiento:new Date(),
    estado: 1,
    pais: {
      idPais: -1
    },
    modalidad: {
      idDataCatalogo: -1
    },  
};

objUsuario: Usuario = {};

constructor(private formBuilder: FormBuilder,
  private alumnoService: AlumnoService,
  private utilService:  UtilService,
  private tokenService:TokenService,
  @Inject(MAT_DIALOG_DATA) public data: any) {

    this.alumno = data;
    console.log(">>> idalumno >> " + this.alumno.idAlumno);
    console.log(">>> nombres >>  " + this.alumno.nombres);
    console.log(">>> apellidos >>  " + this.alumno.apellidos);
    console.log(">>> telefono >>  " + this.alumno.telefono);
    console.log(">>> dni >>  " + this.alumno.dni);
    console.log(">>> correo >>  " + this.alumno.correo);
    console.log(">>> fechaNaciemiento >>  " + this.alumno.fechaNacimiento);
    console.log(">>> estado >>  " + this.alumno.estado);
    console.log(">>> idPais >>  " + this.alumno.pais?.idPais);
    console.log(">>> modalidad >>  " + this.alumno.modalidad?.idDataCatalogo);

    this.utilService.listaPais().subscribe(
      data=>this.pais = data      
    );
    this.utilService.listaModalidadAlumno().subscribe(
      mod=> this.modalidad = mod    
    ); 
    this.objUsuario.idUsuario = tokenService.getUserId();
   }

   actualiza() {
    this.alumnoService.actualiza(this.alumno).subscribe(
      x =>  Swal.fire('Mensaje', x.mensaje, 'info')
  );
  }

}
