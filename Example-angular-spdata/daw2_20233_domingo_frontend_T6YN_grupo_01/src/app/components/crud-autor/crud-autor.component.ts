import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Autor } from 'src/app/models/autor.model';
import { AutorService } from 'src/app/services/autor.service';
import { CrudAutorAddComponent } from '../crud-autor-add/crud-autor-add.component';
import { UtilService } from 'src/app/services/util.service';
import { TokenService } from 'src/app/security/token.service';
import { Pais } from 'src/app/models/pais.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Usuario } from 'src/app/models/usuario.model';
import { CrudAutorUpdateComponent } from '../crud-autor-update/crud-autor-update.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-autor',
  templateUrl: './crud-autor.component.html',
  styleUrls: ['./crud-autor.component.css']
})
export class CrudAutorComponent implements OnInit {
  //Para la Grilla
  filtro: string = "";

  //combos
  lstPais: Pais[] = [];
  lstGrado: DataCatalogo[] = [];
  
  objUsuario: Usuario = {};

  //Grilla
  dataSource: any;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  displayedColumns = ["idAutor", "nombres", "apellidos", "fechaNacimiento", "telefono", "fechaRegistro", "pais", "grado", "estado", "acciones"];

  constructor(    
    private dialogService: MatDialog,
    private autorService: AutorService,
    private utilService: UtilService,
    private tokenService: TokenService
  ) {
    this.utilService.listaPais().subscribe(
      x => this.lstPais = x
    )
    this.utilService.listaGradoAutor().subscribe(
      a => this.lstGrado = a
    )
    this.objUsuario.idUsuario = tokenService.getUserId();

  } // fin de constructor

  // PARA ventana de REGISTRAR
  openAddDialog() {
    console.log(">>> openAddDialog  >>");
    const dialogRef = this.dialogService.open(CrudAutorAddComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  // PARA ventana de ACTUALIZAR
  openUpdateDialog(obj: Autor) {
    console.log(">>> openUpdateDialog  >>");

    const dialogRef = this.dialogService.open(CrudAutorUpdateComponent, { data: obj });
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1) {
        this.refreshTable();
      }
    });
  }
  // FIN DE ACTUALIZAR


  // PARA EL FILTRAR
  consultaAutor() {
    console.log(">>> consultaAutor >>> " + this.filtro);
    this.refreshTable();
  }

  // para el boton de cambio de estado
  actualizaEstado(obj: Autor) {
    obj.estado = obj.estado == 1 ? 0 : 1;
    this.autorService.actualiza(obj).subscribe();
  }
  // fin para el boton de cambio estado

  // Para accion de eliminar
  eliminaPC2(obj: Autor) {
    // para el mensaje de ALERTA (preguta de confimacion)
    Swal.fire({
      title: '¿Desea eliminar?',
      text: "Los cambios no se van a revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, elimina',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        //codigo para solo elimianr sin alerta
        this.autorService.elimina(obj.idAutor || 0).subscribe(
          x => {
            this.refreshTable();
            Swal.fire('Mensaje', x.mensaje, 'info');
          }
        );
        //fin del codigo para solo elimianr sin alerta
      }
    })
  }
  // fin de eliminar  



  private refreshTable() {
    this.autorService.consultaPorNombre(this.filtro == "" ? "todos" : this.filtro).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Autor>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }


  ngOnInit(): void {
  }

} // fin de export class CrudAutorComponent 
