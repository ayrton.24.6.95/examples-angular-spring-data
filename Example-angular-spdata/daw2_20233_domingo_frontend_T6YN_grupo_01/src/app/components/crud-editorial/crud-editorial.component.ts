import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Editorial } from 'src/app/models/editorial.model';
import { Pais } from 'src/app/models/pais.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { EditorialService } from 'src/app/services/editorial.service';
import { UtilService } from 'src/app/services/util.service';
import { CrudEditorialUpdateComponent } from '../crud-editorial-update/crud-editorial-update.component';

import { Location } from '@angular/common';


import Swal from 'sweetalert2';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-crud-editorial',
  templateUrl: './crud-editorial.component.html',
  styleUrls: ['./crud-editorial.component.css'],
})
export class CrudEditorialComponent {



  objUsuario: Usuario = {};
  lstPais: Pais[] = [];
  //editorials: Editorial[] = [];
  editorials: MatTableDataSource<Editorial>= new MatTableDataSource<Editorial>();;
  displayedColumns: string[] = [
    'razonSocial',
    'direccion',
    'ruc',
    'edit',
    'delete',
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private utilService: UtilService,
    private tokenService: TokenService,
    private editorialService: EditorialService,
    private router: Router,
    private dialog: MatDialog,
    private location: Location
  ) {
    utilService.listaPais().subscribe((x) => (this.lstPais = x));
    this.objUsuario.idUsuario = tokenService.getUserId();
  }

  ngOnInit(): void {
    this.cargarLista();
  }

  cargarLista(){
    this.editorialService.listar().subscribe((data: Editorial[]) => {
      this.editorials = new MatTableDataSource(data);
      this.editorials.paginator = this.paginator;
    });
  }

  editarEditorial(editorial: Editorial): void {
    console.log('Editar editorial', editorial);
    const dialogRef = this.dialog.open(CrudEditorialUpdateComponent, {
      data: editorial,
      width: '100%',
      height: '90%',
    });

    dialogRef.afterClosed().subscribe((result: Editorial) => {
      console.log("Resultado del diaglos:",result);
      console.log("validacion del objeto",this.validarEsObjetoVacio(result));
            
      if (result !== undefined && !this.validarEsObjetoVacio(result)) {
        editorial = result;
        this.editorialService.actualizar(editorial).subscribe((x) => {
          Swal.fire({
            icon: 'info',
            title: 'Resultado del Registro',
            text: 'Se actualizo el registro',
          });
          this.cargarLista();
        });
      }else{
        this.cargarLista();
      }
    });
  }

  eliminarEditorial(editorial: Editorial): void {
    console.log('Eliminar editorial', editorial);

    Swal.fire({
      title: '¿Estás seguro?',
      text: `Esta acción eliminará la editorial "${editorial.razonSocial}". ¿Deseas continuar?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        // El usuario confirmó la eliminación
        if (editorial.idEditorial !== undefined) {
          this.editorialService
            .eliminar(editorial.idEditorial)
            .subscribe(() => {
              Swal.fire(
                'Eliminado',
                'El registro de la editorial ha sido eliminado',
                'success'
              );
              // Recarga la lista de editoriales u otras acciones necesarias
              this.cargarLista();
            });
        } else {
          // Manejar el caso en el que el ID de la editorial es undefined
          console.error('ID de la editorial es undefined');
        }
      }
    });
  }

  nuevaEditorial(): void {
    this.router.navigate(['/verRegistroEditorial']);
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.editorials.filter = filterValue.trim().toLowerCase();
  }

  validarEsObjetoVacio(obj: object): boolean {
    return Object.keys(obj).length === 0;
  }

}
