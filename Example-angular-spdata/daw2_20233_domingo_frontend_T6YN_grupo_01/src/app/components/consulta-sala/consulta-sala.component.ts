import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Sala } from 'src/app/models/sala.model';
import { SalaService } from 'src/app/services/sala.service';
import { UtilService } from 'src/app/services/util.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-consulta-sala',
  templateUrl: './consulta-sala.component.html',
  styleUrls: ['./consulta-sala.component.css']
})
export class ConsultaSalaComponent implements OnInit {
  dataSource: any;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  displayedColumns = ["idSala", "numero", "recursos", "tipoSala", "sede", "estado"];

  numero: string="";
  recursos: string="";
  idTipoSala: number=-1;
  idSede: number=-1;
  estado: boolean = true;
  
  TipoSala: DataCatalogo[] = [];
  Sede: DataCatalogo[] = [];

  constructor(private utilService: UtilService,
              private salaService: SalaService) {
                utilService.listaTipoSala().subscribe(
                  x => this.TipoSala =x
                )
                utilService.listaSede().subscribe(
                  x => this.Sede =x
                )
  }

  consulta(){
    console.log(">> numero >>"+this.numero);
    console.log(">> recursos >>"+this.recursos);
    console.log(">> TipoSala >>"+this.TipoSala);
    console.log(">> Sede >>"+this.Sede);
    console.log(">> estado >>"+this.estado);

    this.salaService.consulta(this.numero, this.recursos, this.idTipoSala, this.idSede, this.estado ? 1 : 0).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Sala>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  exportarPDF(){
    this.salaService.generateDocumentReport(this.numero, this.recursos, this.idTipoSala, this.idSede, this.estado ? 1 : 0).subscribe(
      response => {
        console.log(response);
        var url = window.URL.createObjectURL(response.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.setAttribute('target', 'blank');
        a.href = url;
        a.download = response.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      }
    );

  }

  ngOnInit(): void {
  }

}
