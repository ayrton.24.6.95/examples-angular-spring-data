import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { TokenService } from 'src/app/security/token.service';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { UtilService } from 'src/app/services/util.service';
import { CrudProveedorAddComponent } from '../crud-proveedor-add/crud-proveedor-add.component';
import { MatDialog } from '@angular/material/dialog';
import { Pais } from 'src/app/models/pais.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Usuario } from 'src/app/models/usuario.model';
import { Proveedor } from 'src/app/models/proveedor.model';
import { CrudProveedorUpdateComponent } from '../crud-proveedor-update/crud-proveedor-update.component';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-proveedor',
  templateUrl: './crud-proveedor.component.html',
  styleUrls: ['./crud-proveedor.component.css']
})
export class CrudProveedorComponent implements OnInit {

  filtro: string = "";

  pais: Pais[] = [];
  tipoProveedor: DataCatalogo[] = [];

  objUsuario: Usuario = {};

   dataSource: any;

  @ViewChild(MatPaginator, { static:true}) paginator!: MatPaginator;
  displayedColumns = ["idProveedor", "razonsocial","ruc","direccion","celular","contacto","fechaRegistro", "pais" , "tipo", "estado", "acciones"];

  constructor(private formBuilder: FormBuilder,
              private dialogService: MatDialog,
              private proveedorService: ProveedorService,
              private utilService: UtilService,
              private tokenService: TokenService) {
                this.utilService.listaPais().subscribe(x => this.pais = x);
                this.utilService.listaTipoProveedor().subscribe(y => this.tipoProveedor = y);
                this.objUsuario.idUsuario = tokenService.getUserId();
               }

  openAddDialog(){
    console.log(">>> openAddDialog >>");
    const dialogRef = this.dialogService.open(CrudProveedorAddComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(">>> result >> " + result);
      if (result === 1){
        this.refreshTable();
      }
    });
  }

  openUpdateDialog(obj: Proveedor){
    console.log(">>> openUpdateDialog >>");
    const dialogRef = this.dialogService.open(CrudProveedorUpdateComponent, {data:obj});
    dialogRef.afterClosed().subscribe(result =>{
      console.log(">>> result >> " + result);
      if (result == 1){
        this.refreshTable();
      }
    });
  }

  //método para el filtrado
  consultaProveedor(){
    console.log(">>> consultaProveedor >>> " + this.filtro);
    this.refreshTable();
  }

  actualizaEstado(obj: Proveedor){
    obj.estado = obj.estado == 1 ? 0 : 1;
    this.proveedorService.actualiza(obj).subscribe();
  }

  elimina(obj: Proveedor){
    Swal.fire({
      title: '¿Desea eliminar el registro?',
      text: "Los cambios no se revertirán",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, elimina',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
          if (result.isConfirmed) {
              this.proveedorService.elimina(obj.idProveedor || 0).subscribe(
                    x => {
                          this.refreshTable();
                          Swal.fire('Mensaje', x.mensaje, 'info');
                    }
              );
          }
    })   
}

  private refreshTable(){
    this.proveedorService.consultaPorNombre(this.filtro==""?"todos":this.filtro).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Proveedor>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  ngOnInit(): void {
  }

}