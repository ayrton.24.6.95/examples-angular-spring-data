import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Usuario } from 'src/app/models/usuario.model';
import { MatDialog } from '@angular/material/dialog';
import { SalaService } from 'src/app/services/sala.service';
import { UtilService } from 'src/app/services/util.service';
import { TokenService } from 'src/app/security/token.service';
import { Sala } from 'src/app/models/sala.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CrudSalaUpdateComponent } from '../crud-sala-update/crud-sala-update.component';
import { CrudSalaAddComponent } from '../crud-sala-add/crud-sala-add.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-sala',
  templateUrl: './crud-sala.component.html',
  styleUrls: ['./crud-sala.component.css']
})
export class CrudSalaComponent implements OnInit {

  TipoSala: DataCatalogo[] = [];
  Sede: DataCatalogo[] = [];

  objUsuario: Usuario = {} ;
  
  filtro: string = "";
  dataSource: any;
  @ViewChild(MatPaginator, {static:true}) paginator!: MatPaginator;
  displayedColumns = ["idSala","numero","recursos","fechaRegistro","fechaActualizacion","estado","tipoSala","sede","acciones"];


  constructor(private formBuilder: FormBuilder,
              private salaService:SalaService , 
              private utilService: UtilService, 
              private tokenService: TokenService,
              private dialogService: MatDialog) {
                utilService.listaTipoSala().subscribe(
                  x => this.TipoSala =x
                )
                utilService.listaSede().subscribe(
                  x => this.Sede =x
                )
              }

  openAddDialog(){
    console.log(">>>>openDialog >>>");
    const dialogRef = this.dialogService.open(CrudSalaAddComponent);
    dialogRef.afterClosed().subscribe(
      result => {console.log(">>result >>" + result);
      if (result === 1){
         this.refreshTable();
      }
    });
  }

  openUpdateDialog(obj:Sala){
    const dialogRef = this.dialogService.open(CrudSalaUpdateComponent,{data:obj});

    dialogRef.afterClosed().subscribe(x=>{
     if(x === 1){
       this.refreshTable();
     }
    });
  }

  ngOnInit(): void {
  }

  consultaSala(){
    console.log(">>>consultaSala>>>>" + this.filtro);
    this.refreshTable();
  }

  actualizaEstado(obj: Sala){
    obj.estado = obj.estado == 1 ? 0 : 1;
    this.salaService.actualiza(obj).subscribe();
  }

  elimina(obj:Sala){
    Swal.fire({
      title:'¿Desea Eliminar?',
      text: "Los cambios no se van a revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, elimina',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.isConfirmed){
        this.salaService.elimina(obj.idSala || 0).subscribe(
          x=>{
            this.refreshTable();
            Swal.fire('Mensaje', x.mensaje,'info');
          }
        );
      }
    })
  }

  private refreshTable(){
    this.salaService.consultarPorNumero(this.filtro == "" ? "todos" : this.filtro).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Sala>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }
}
