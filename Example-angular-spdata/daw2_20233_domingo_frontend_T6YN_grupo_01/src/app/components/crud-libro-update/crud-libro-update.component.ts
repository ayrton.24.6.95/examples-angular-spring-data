import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { LibroService } from 'src/app/services/libro.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-libro-update',
  templateUrl: './crud-libro-update.component.html',
  styleUrls: ['./crud-libro-update.component.css']
})
export class CrudLibroUpdateComponent {
//Para los util
CategoriaLibro: DataCatalogo [] = [];
TipoLibro: DataCatalogo[] =[];


objUsuario: Usuario = {};

//Json
 libro: Libro ={
   idLibro:0,
   titulo:"",
    anio: 0,
    serie:"",
    categoriaLibro:{
       idDataCatalogo:-1
    },
    tipoLibro:{
     idDataCatalogo:-1
    }
 }

 //validaciones
 formsActualiza = this.formBuilder.group({
  validaTitulo:['',[Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ ]{3,30}')]],
  validaAnio:['',[Validators.required, Validators.pattern('[0-9]{4}')]],
  validaSerie:['',[Validators.required, Validators.pattern('[a-zA-Z0-9]{12}$')]],
  validaCategoria:['',Validators.min(1)],
  validaTipo:['',Validators.min(1)]


});


constructor(private formBuilder: FormBuilder,
            private libroService: LibroService,
            private utilService: UtilService,
            private tokenService:TokenService,
            @Inject(MAT_DIALOG_DATA)public data:any
          ) { 
              this.libro=data;
              this.utilService.listaCategoriaDeLibro().subscribe(
                x => this.CategoriaLibro =x
               );
               this.utilService.listaTipoLibroRevista().subscribe(
                x => this.TipoLibro=x
               );

               this.objUsuario.idUsuario = tokenService.getUserId();
              
}

cargaCategoriaLibro(){
              console.log(this.libro.categoriaLibro?.descripcion);
              this.utilService.listaCategoriaDeLibro().subscribe(
                x => this.CategoriaLibro = x
              );
              
}

cargaTipoLibro(){
              console.log(this.libro.categoriaLibro?.descripcion);
              this.utilService.listaTipoLibroRevista().subscribe(
               x => this.TipoLibro = x
               );
                
}


ngOnInit(): void {
}

actualiza(){
  this.libro.usuarioRegistro = this.objUsuario;
  this.libro.usuarioActualiza = this.objUsuario;
 this.libroService.actualiza(this.libro).subscribe(
  x=>{ Swal.fire('Mensaje', x.mensaje, 'info'); 
   
   }
 );
}
}
