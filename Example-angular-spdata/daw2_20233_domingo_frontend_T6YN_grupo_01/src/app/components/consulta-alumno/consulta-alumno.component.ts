import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Alumno } from 'src/app/models/alumno.model';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Pais } from 'src/app/models/pais.model';
import { AlumnoService } from 'src/app/services/alumno.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-consulta-alumno',
  templateUrl: './consulta-alumno.component.html',
  styleUrls: ['./consulta-alumno.component.css']
})
export class ConsultaAlumnoComponent implements OnInit {

  //Grila
  dataSource: any;

  //Clase para la paginacion
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  //Cabecera
  displayedColumns = ["idAlumno", "nombres", "apellidos", "telefono", "dni", "correo", "estado", "pais", "modalidad"];

  //parametros de la consulta
  nombres: string = "";
  apellidos: string = "";
  telefono: string = "";
  dni: string = "";
  correo: string = "";
  estado: boolean = true;
  selPais: number = -1;
  selModalidad: number = -1;

  //Para los combos
  pais: Pais[] = [];
  modalidad: DataCatalogo[] = [];


  constructor(private utilService: UtilService,
    private alumnoService: AlumnoService) {
    this.utilService.listaPais().subscribe(
      data => this.pais = data
    );

    this.utilService.listaModalidadAlumno().subscribe(
      mod => this.modalidad = mod
    );
  }


  consulta() {
    console.log(">> nombre >> " + this.nombres);
    console.log(">> apellidos >> " + this.apellidos);
    console.log(">> telefono >> " + this.telefono);
    console.log(">> dni >> " + this.dni);
    console.log(">> correo >> " + this.correo);
    console.log(">> estado >> " + this.estado);
    console.log(">> selPais>> " + this.selPais);
    console.log(">> selModalidad>> " + this.selModalidad);

    this.alumnoService.consulta(this.nombres, this.apellidos, this.telefono, this.dni, this.correo, this.estado ? 1 : 0, this.selPais, this.selModalidad).subscribe(
      x => {
        this.dataSource = new MatTableDataSource<Alumno>(x);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  exportarPDF() {

    this.alumnoService.generateDocumentReport(this.nombres, this.apellidos, this.telefono, this.dni, this.correo, this.estado ? 1 : 0, this.selPais, this.selModalidad).subscribe(
          response => {
            console.log(response);
            var url = window.URL.createObjectURL(response.data);
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.setAttribute('target', 'blank');
            a.href = url;
            a.download = response.filename;
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove();
        }); 
  }


  ngOnInit(): void {
  }

}
