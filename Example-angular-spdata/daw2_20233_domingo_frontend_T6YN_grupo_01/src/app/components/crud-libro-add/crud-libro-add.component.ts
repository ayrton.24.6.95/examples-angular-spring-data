import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataCatalogo } from 'src/app/models/dataCatalogo.model';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuario.model';
import { TokenService } from 'src/app/security/token.service';
import { LibroService } from 'src/app/services/libro.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crud-libro-add',
  templateUrl: './crud-libro-add.component.html',
  styleUrls: ['./crud-libro-add.component.css']
})
export class CrudLibroAddComponent {
 //Para los util
 CategoriaLibro: DataCatalogo [] = [];
 TipoLibro: DataCatalogo[] =[];
 

 objUsuario: Usuario = {};
 

  //validaciones
  formRegistra = this.formBuilder.group({
    validaTitulo:['',[Validators.required, Validators.pattern('[a-zA-Zá-úÁ-ÚñÑ\d ]{3,30}')]],
    validaAnio:['',[Validators.required, Validators.pattern('[0-9]{4}')]],
    validaSerie:['',[Validators.required, Validators.pattern('[a-zA-Z0-9]{12}$')]],
    validaCategoria:['',Validators.min(1)],
    validaTipo:['',Validators.min(1)]


  });

 //Json
  libro: Libro ={
    titulo:"",
     anio: 0,
     serie:"",
     estado:1,
     categoriaLibro:{
        idDataCatalogo:-1
     },
     tipoLibro:{
      idDataCatalogo:-1
     }
  }

 
constructor(private formBuilder: FormBuilder,
 private libroService: LibroService,
 private utilService: UtilService,
 private tokenService:TokenService) { 
  this.utilService.listaCategoriaDeLibro().subscribe(
    x => this.CategoriaLibro=x
   );
   this.utilService.listaTipoLibroRevista().subscribe(
    x => this.TipoLibro=x
   );
  

   this.objUsuario.idUsuario = tokenService.getUserId();

 }



ngOnInit(): void {
}

registrarLibro(){
  this.libro.usuarioRegistro = this.objUsuario;
  this.libro.usuarioActualiza = this.objUsuario;
  this.libroService.inserta(this.libro).subscribe(
   x=>{
     Swal.fire('Mensaje', x.mensaje, 'info'); 
     this.libro = {
      idLibro:0,
      titulo:"",
      anio: 0,
      serie:"",
      categoriaLibro:{
         idDataCatalogo:-1
      },
      tipoLibro:{
       idDataCatalogo:-1
      }
         
     };

    }
  );
}

}
